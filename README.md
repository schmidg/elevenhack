# Eleven Hack

Eleven Hack has been written to be able to share rigs with other users on my Linux platform.
I wrote it in Java so it could be used by Mac and Windows user.
It started with a simple tfx file parser, to be able to reproduce by hand the parameters of any rig on the 11R.
Then I hacked the usb protocol and wrote a complete rig management software able to upload rigs on the device.
The software has been written in 2013, It is mostly interesting as an implementation of the tfx parser and usb protocol.
I had a few requests to open source it, so here it is.
It was also my first Java Software, feel free, to contribute and make it better. The 11R is a wonderful device and deserve some love :)

ElevenHack Website:

https://sites.google.com/site/elevenhack/
