/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.tfx.TfxError;

public class EffectFactory
{
   public static Effect  createEffect( int type, int effectId ) throws TfxError
   {
      switch( effectId ){
         case Effect.AMP_CAB: return new EffectAmpCab(type, effectId);
         default: return new Effect( type, effectId );
      }
   }
}
