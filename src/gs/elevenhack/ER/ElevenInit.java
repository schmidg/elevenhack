/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.utils.rigloader.EProgressMonitor;

public class ElevenInit implements ElevenListener
{
	private ElevenRack m_rack;
	private EProgressMonitor m_pMon;
	
	public ElevenInit() { Term.println("creating ElevenInit" );}
	public ElevenInit( EProgressMonitor pmon ) 
	{
		Term.println("creating ElevenInit With param.");
	   m_pMon = pmon;
	}

	public void attachRack( ElevenRack er )
	{
       m_rack = er;
	}
	
	//-----------------------------------------------------------
	@Override
	public void detachRack()
	{
	   m_rack = null;
	}
	    	
	//-----------------------------------------------------------
	public void startInit() throws ERError
	{
		Term.println( "ElevenRackInit staring...");
		if( m_pMon != null ) {
		   m_pMon.setMinimum( 0 );
		   m_pMon.setMaximum( 10 ); // Temporary.
		   m_pMon.setProgress( 0 );
		   m_pMon.setNote( "Initializing Eleven Rack..." );
		}
		
		m_rack.getTransmitter().requestMainVolume();
		m_rack.getTransmitter().requestNbEffects();
	}

	//-----------------------------------------------------------
	@Override
    public void onEffectCount(int n) 
    {
	   if( m_pMon != null ) m_pMon.setMaximum( n );
       try {
          m_rack.getTransmitter().requestDescEffect( 0 );
      } catch (ERError e) {
          e.printStackTrace();
      }
    }

	//-----------------------------------------------------------
	@Override
    public void onEffectDescription(int id) throws ERError
    {
       if( m_pMon != null ) m_pMon.setProgress( id );
       Term.println("received effect desc " + id );
	   
		if( id == m_rack.getNbEffects() - 1 ) {

		   if( m_pMon != null ) {
		      m_pMon.setMaximum( 96 );
		      m_pMon.setProgress( 0 );
		      m_pMon.setNote( "Initializing Rig List..." );
		   }
		   m_rack.getTransmitter().requestRigName(0,0);
		}
		else {
	       try {
	          m_rack.getTransmitter().requestDescEffect( id + 1 );
	      } catch (ERError e) {
	          e.printStackTrace();
	      }
		}
    }

	//-----------------------------------------------------------
	@Override
    public void onUpdateRigList( int bank, int id )throws ERError
    {
       if( m_pMon != null ) m_pMon.setProgress( id );

		if( id == ElevenRack.MAX_RIG_BANK - 1 ) {
			if( bank == 0 ) m_rack.getTransmitter().requestRigName(1,0);
			else {
				m_rack.getTransmitter().requestCurrentRigNumber();
				m_rack.notifyInitDeviceFinished();
			}
		}
		else m_rack.getTransmitter().requestRigName(bank,id + 1);
    }
	
	//-----------------------------------------------------------
	@Override
    public void onRigDescChanged() throws ERError
    {
		Term.println("Rig update.");
    }

	//-----------------------------------------------------------
	@Override
    public void onRigSelected(int bank, int num) throws ERError
    {
		m_rack.getTransmitter().requestRigDesc();	    
    }

   @Override
   public void onStartInitDevice() throws ERError
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void onDeviceInitialized() throws ERError
   {
   }
   @Override public void onTunerSwitched( boolean b ) throws ERError{}
   @Override public void onMainVolumeSet( int v ) throws ERError {} 
	//-----------------------------------------------------------
	

}
