/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.SelectOption;

public interface RigRenderer
{
   public void rigName( String n );
   public void rigVersion( int v );
   public void newEffectName( int effectId, int effectClass, String name );
   public void knobParam( String name, int Value );
   public void selectorParam( String Name, SelectOption[] options, int m_val );
   public void switchParam( String Name, int m_val );
   public void commitEffect();
   public void renderUnset( String m_deco );
}
