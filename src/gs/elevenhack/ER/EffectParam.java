/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.tfx.TfxError;

import java.text.DecimalFormat;

public abstract class EffectParam
{
   final static int MININT            = -2147483648;
   final static int MAXINT            = +2147483647;
   final static int STEP0_1           = MAXINT-MININT / 100;

   EffectParam( String key, String deco ) 
   {
      m_key = key;
      m_deco = deco;
   }
   
   public String getKey() { return m_key; }
   public String getDecoration() { return m_deco; }
   
   public void render( RigRenderer r) throws TfxError
   {
      if( m_isSet ) doRender( r );
      else r.renderUnset( m_deco );
   }

   public boolean isSet() { return m_isSet; }
   public void setValue( int v ) throws TfxError
   {
      doSetValue( v);
      m_isSet = true;
   }
   
   public int getValue()
   {
      return doGetValue();
   }
   //-------------------------------------------------------------------- TOOLS
   static double valToDouble(int val, int min, int max )
   {
      double range = max - min;
      return (double) val / range;
   }
   
   static String toDecString(double v)
   {
      return m_decForm.format( v );
   }
   
   static String toDecString( int val, int min, int max )
   {
      return toDecString( valToDouble( val, min, max ));
   }
   
   
   //---------------------------------------------------------------------
   protected abstract void doRender( RigRenderer r );
   protected abstract void doSetValue( int v ) throws TfxError;
   protected abstract int doGetValue();
   private String m_key;
   private String m_deco;
   private boolean m_isSet = false;
   protected static final DecimalFormat m_decForm = new DecimalFormat("0.00");
}
