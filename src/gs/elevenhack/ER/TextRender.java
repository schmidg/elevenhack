/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.SelectOption;

public class TextRender implements RigRenderer
{
   StringBuilder sb = new StringBuilder();
   int stage = 0;
   
   //-----------------------------------------------------------
   @Override
   public
   void rigName( String n )
   {
      sb.append( "Rig name: " + n + "\n");
   }   

   //-----------------------------------------------------------
   @Override
   public
   void rigVersion( int v )
   {
      sb.append( "Extension :" + (v < 4 ? "No" : "Yes") + "\n");
   }

   //-----------------------------------------------------------
   @Override
   public
   void newEffectName( int type, int effectClass, String name )
   {
      sb.append("Stage: " + stage + ": " + Effect.getClassShortName( type ) + " " + name  +"\n");
      ++stage;
   }
   
   //-----------------------------------------------------------
   @Override
   public
   void knobParam( String name, int value )
   {
      sb.append( "\t" + name + ": " + value + "\n"  );
   }
   
   //-----------------------------------------------------------
   @Override
   public
   void selectorParam( String name, SelectOption[] options, int val )
   {
      sb.append( "\t" + name + ": " + val/*options[val].val*/ + "\n" );
   }

   //-----------------------------------------------------------
   @Override
   public
   void switchParam( String name, int value )
   {
      sb.append( "\t" + name + ": " );
      switch(value) {
         case -1: sb.append( "not set" ); break;
         case 0 : sb.append( "Off" );     break;
         default: sb.append( "On" );
      }
      sb.append( "\n" );
   }

   //-----------------------------------------------------------
   @Override
   public void renderUnset( String name )
   {
      sb.append( "\t" + name + ": " + "not_set\n" );
   }
   
   //-----------------------------------------------------------
   @Override
   public
   void commitEffect(){}
   
   public String getText() { return sb.toString(); }
}
