/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.ERError;
import gs.elevenhack.MessageBuilder;
import gs.elevenhack.Term;
import gs.elevenhack.midi.ElevenReceiver;
import gs.elevenhack.midi.ElevenTransmitter;
import gs.elevenhack.midi.MidiCom;
import gs.elevenhack.midi.SysEx;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;
import gs.elevenhack.utils.rigloader.EProgressMonitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.TreeMap;

import javax.swing.ProgressMonitor;

public class ElevenRack
{

   public static final int                   MAX_RIG_BANK   = 104;

   private int                               m_nbEffects;
   private ElevenEffect[]                    m_effects;
   private String[]                          m_effectTypes  = { "00 Amp/Cab",
         "01 FX Loop", "02 Vol    ", "03 Wah    ", "04 Mod    ", "05 Reverb ",
         "06 Delay  ", "07 Disto  ", "08 FX1    ", "09 FX2    ", "0A        ",
         "0B Input  ", "0C\t", "0D\t", "0E\t", "0F\t"      };
   private static String[]                   m_rigLocToName = { "A1", "A2",
         "A3", "A4", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "C4", "D1",
         "D2", "D3", "D4", "E1", "E2", "E3", "E4", "F1", "F2", "F3", "F4",
         "G1", "G2", "G3", "G4", "H1", "H2", "H3", "H4", "I1", "I2", "I3",
         "I4", "J1", "J2", "J3", "J4", "K1", "K2", "K3", "K4", "L1", "L2",
         "L3", "L4", "M1", "M2", "M3", "M4", "N1", "N2", "N3", "N4", "O1",
         "O2", "O3", "O4", "P1", "P2", "P3", "P4", "Q1", "Q2", "Q3", "Q4",
         "R1", "R2", "R3", "R4", "S1", "S2", "S3", "S4", "T1", "T2", "T3",
         "T4", "U1", "U2", "U3", "U4", "V1", "V2", "V3", "V4", "W1", "W2",
         "W3", "W4", "X1", "X2", "X3", "X4", "Y1", "Y2", "Y3", "Y4", "Z1",
         "Z2", "Z3", "Z4"                                  };
   private static TreeMap< String, Integer > m_rigNameToLoc = new TreeMap< String, Integer >() {
                                                               {
                                                                  for ( int i = 0; i < 104; ++i )
                                                                     put( m_rigLocToName[i],
                                                                          i );
                                                               }
                                                            };

   private MidiCom                           m_com;
   private String[][]                        m_rigBanks;
   private LinkedList< ElevenListener >      m_listeners;
   private int                               m_currRigId;
   private int                               m_currBankId;
   private ElevenInit                        m_rackInit;

   // -------------------------------------------------------------------
   public ElevenRack()
   {
      m_nbEffects = 0;
      m_effects = null;
      m_rigBanks = new String[ 2 ][ ];
      m_rigBanks[0] = new String[ MAX_RIG_BANK ];
      m_rigBanks[1] = new String[ MAX_RIG_BANK ];

      mInit();
      m_listeners = new LinkedList< ElevenListener >();
   }

   // --------------------------------------------------------------------
   private void mInit()
   {
      for ( int i = 0; i < MAX_RIG_BANK; i++ ) {
         m_rigBanks[0][i] = new String( "" );
         m_rigBanks[0][i] = new String( "" );
      }
   }

   // -------------------------------------------------------------------
   public void open( int inPort, int outPort ) throws ERError
   {
      m_com = new MidiCom( this, inPort, outPort );
   }

   // -------------------------------------------------------------------
   public void close()
   {
      if( m_com != null ) try {
         m_com.close();
      }
      catch( ERError e ) {
         // TODO Auto-generated catch block
         Term.println( e.getMessage() );
      }
      mInit();
   }

   // -------------------------------------------------------------------
   static public String LocToName( int loc )
   {
      return m_rigLocToName[loc];
   }

   // -------------------------------------------------------------------
   static public int NameToLoc( String name )
   {
      if( m_rigNameToLoc.containsKey( name ) ) {
         return m_rigNameToLoc.get( name ).intValue();
      }
      return -1;
   }

   // -------------------------------------------------------------------
   public void startInitDevice( EProgressMonitor pMon ) throws ERError
   {
      if( m_rackInit == null ) {
         m_rackInit = new ElevenInit( pMon );
         addListener( m_rackInit );
      }
      m_rackInit.startInit();
   }

   // -------------------------------------------------------------------
   public void notifyStartInitDevice() throws ERError
   {
      for ( ElevenListener el : m_listeners )
         el.onStartInitDevice();
   }

   // -------------------------------------------------------------------
   public void notifyInitDeviceFinished() throws ERError
   {
      removeListener( m_rackInit );
      m_rackInit = null;
      for ( ElevenListener el : m_listeners )
         el.onDeviceInitialized();
   }

   // -------------------------------------------------------------------
   public ElevenTransmitter getTransmitter() throws ERError
   {
      if( m_com == null || !m_com.isConnected() ) {
         throw new ERError(
                            "Cannot perform operation, Eleven Rack is not connected." );
      }
      return m_com.getTransmitter();
   }

   // -------------------------------------------------------------------
   public ElevenReceiver getReceiver()
   {
      return m_com.getReceiver();
   }

   // -------------------------------------------------------------------
   public void addListener( ElevenListener l )
   {
      if( m_listeners.contains( l )) {
         Term.println( "LISTENER ALREADY REGISTERED LOGIC ERROR!!! (gui, do somthing!!!)\n" );
      }
      l.attachRack( this );
      m_listeners.add( l );
   }

   public void removeListener( ElevenListener l )
   {
      Term.println( "Nb of liteners: " + m_listeners.size() );
      m_listeners.remove( l );
      l.detachRack();
      Term.println( "Removing listener, new size: " + m_listeners.size());
   }

   // -------------------------------------------------------------------
   public void callbackSetNbEffects( int n ) throws ERError
   {
      m_nbEffects = n;
      m_effects = new ElevenEffect[ n ];
      for ( ElevenListener el : m_listeners )
         el.onEffectCount( n );
   }

   // -------------------------------------------------------------------
   public void callBackSetEffect( int n, String strId, String name )
                                                                    throws ERError
   {
      m_effects[n] = new ElevenEffect( n, strId, name );
      for ( ElevenListener el : m_listeners )
         el.onEffectDescription( n );
   }

   // -------------------------------------------------------------------
   public int getNbEffects()
   {
      return m_nbEffects;
   }

   public ElevenEffect getEffect( int i )
   {
      return m_effects[i];
   }

   // -------------------------------------------------------------------
   public int getNbRigsInBank( int n )
   {
      return m_rigBanks[n].length;
   }

   // -------------------------------------------------------------------
   public void callbackSetRigName( int bank, int num, String name )
                                                                   throws ERError
   {
      m_rigBanks[bank][num] = name;
      for ( ElevenListener el : m_listeners )
         el.onUpdateRigList( bank, num );
   }

   // -------------------------------------------------------------------
   public String getRigName( int bank, int n )
   {
      return m_rigBanks[bank][n];
   }

   // -------------------------------------------------------------------
   public void callbackCurrentRigUpdated( int[] params ) throws ERError
   {
      /*
       * for (int i = 0; i < RigDescriptor.NBRIGITEMS; i++) {
       * m_rigDesc.setRigItem( i, params[i * 3], params[i * 3 + 1], params[i * 3
       * + 2] ); } for (ElevenListener el : m_listeners) el.onRigDescChanged();
       */
   }

   // -------------------------------------------------------------------
   public void callbackRigItemUpdated( int pos, int eid, int uid, int cid )
   {
      // m_rigDesc.setRigItem( pos, eid, uid, cid );
   }

   // -------------------------------------------------------------------
   public ElevenEffect getRigEffect( int pos )
   {
      // return m_effects[m_rigDesc.getItemId( pos )];
      return null;
   }

   // -------------------------------------------------------------------
   public int getRigEffectUid( int pos )
   {
      // return m_rigDesc.getItemUid( pos );
      return -1;
   }

   // -------------------------------------------------------------------
   public String getRigEffectType( int pos )
   {
      // return m_effectTypes[m_rigDesc.getItemType( pos )];
      return null;
   }

   // -------------------------------------------------------------------
   public void callbackSwitchRig( int bank, int num ) throws ERError
   {
      m_currBankId = bank;
      m_currRigId = num;
      for ( ElevenListener el : m_listeners )
         el.onRigSelected( bank, num );
   }

   // -------------------------------------------------------------------
   public void callbackTunerSwitch( boolean v ) throws ERError
   {
      for ( ElevenListener el : m_listeners )
         el.onTunerSwitched( v );
   }

   // -------------------------------------------------------------------
   public void callbackMainVolumeSet( int val ) throws ERError
   {
      for ( ElevenListener el : m_listeners )
         el.onMainVolumeSet( val );
   }

   // -------------------------------------------------------------------
   public int getCurrentRigNum()
   {
      return m_currRigId;
   }

   // -------------------------------------------------------------------
   public int getCurrentRigBank()
   {
      return m_currBankId;
   }

   // -------------------------------------------------------------------
   public void loadRigFile( String fname, boolean saveOnLoad ) throws ERError,
                                                              IOException,
                                                              TfxError
   {
      FileInputStream in = new FileInputStream( new File( fname ) );
      loadRigStream( in, saveOnLoad );
   }
   
   // -------------------------------------------------------------------
   public void loadRigFile( File f, boolean saveOnLoad ) throws ERError,
                                                              IOException,
                                                              TfxError
   {
      FileInputStream in = new FileInputStream( f );
      loadRigStream( in, saveOnLoad );
   }

   // -------------------------------------------------------------------
   public void loadRigFile( String fname, int numRig, boolean saveOnLoad ) throws ERError,
                                                              IOException,
                                                              TfxError
   {
      FileInputStream in = new FileInputStream( new File( fname ) );
      loadRigStream( in, numRig, saveOnLoad );
   }
   
   // -------------------------------------------------------------------
   public void loadRigFile( File f, int numRig, boolean saveOnLoad ) throws ERError,
                                                              IOException,
                                                              TfxError
   {
      FileInputStream in = new FileInputStream( f );
      loadRigStream( in, numRig, saveOnLoad );
   }
   // ---------------------------------------------------------
   public void loadRigStream( InputStream in, boolean saveOnLoad ) throws ERError,
   TfxError, IOException
   {
      loadRigStream( in, m_currRigId, saveOnLoad );
   }
   
   // ---------------------------------------------------------
   public void loadRigStream( InputStream in, int numRig, boolean saveOnLoad ) throws ERError,
   TfxError, IOException
   {
      MessageBuilder mb = new MessageBuilder();
      mb.append( new byte[ ] { SysEx.START, SysEx.VENDOR_ID, SysEx.DEVICE_ID,
            SysEx.MODEL_ID, 0, 1 } );


      in.skip( 56 );
      int size = in.available();
      byte[] body = new byte[ size ];
      in.read( body );
      in.close();

      byte[] bulk7 = SysEx.encodeTo7bits( body, 0, body.length );

      mb.append( bulk7 );
      mb.append( SysEx.END );

      byte[] msg = mb.get();
      
      getTransmitter().selectCurrentRig( 0, numRig );
      getTransmitter().sendSysex( msg );
      if( saveOnLoad ) {
         getTransmitter().saveRig( numRig );
      }
   }

   // ---------------------------------------------------------
   public void midiConsole() throws ERError
   {
      ElevenDumper dumper = new ElevenDumper();
      addListener( dumper );

      try {
         // open up standard input
         BufferedReader br = new BufferedReader(
                                                 new InputStreamReader(
                                                                        System.in ) );
         String command = " ";

         // read the username from the command-line; need to use try/catch
         // with the
         // readLine() method
         Term.println( "cmd:" );

         while( command.length() != 0 ) {
            command = br.readLine();
            if( command == null ) break;
            command = command.trim();

            if( command.startsWith( "send" ) ) {
               command = command.substring( 4 ).trim();

               byte[] msg = SysEx.stringToSysEx( command );
               for ( int i = 0; i < 5; ++i )
                  if( msg != null ) m_com.sendSysex( msg );
            }
            if( command.startsWith( "sendn" ) ) {
               command = command.substring( 5 ).trim();

               byte[] msg = SysEx.stringToSysEx( command );
               for ( int i = 0; i < 5; ++i )
                  if( msg != null ) m_com.sendSysex( msg );
            }
            if( command.startsWith( "bulk" ) ) {
               m_com.sendSysex( SysEx.buildQuery1( SysEx.CMD_GET_BULK_TFX ) );
            }
            if( command.startsWith( "bulkn" ) ) {
               for ( int i = 0; i < 10; ++i ) {
                  m_com.sendSysex( SysEx.buildQuery1( SysEx.CMD_GET_BULK_TFX ) );
               }
            }

            else {
               Term.println( "commands: send, effects, listrigs" );
            }
         }
      }
      catch( IOException ioe ) {
         Term.println( "IO error trying to read from stdin!" );
      }
   }

   // ------------------------------------------------------------
   public void selectNextItem() throws ERError
   {
      int nrig = getCurrentRigNum();
      if( nrig < MAX_RIG_BANK - 1 ) ++nrig;
      getTransmitter().selectCurrentRig( 0, nrig );
   }

   // ------------------------------------------------------------
   public void selectPrevItem() throws ERError
   {
      int nrig = getCurrentRigNum();
      if( nrig > 0 ) --nrig;
      getTransmitter().selectCurrentRig( 0, nrig );
   }

   //-------------------------------------------------------------
   public void selectItem( int nrig ) throws ERError
   {
      if( nrig < 0 ) nrig = 0;
      if( nrig > MAX_RIG_BANK ) nrig = MAX_RIG_BANK;
      getTransmitter().selectCurrentRig( 0, nrig );
   }

   // -------------------------------------------------------------
   public void renameRig( String name ) throws ERError
   {
      getTransmitter().setRigName( name );
      getTransmitter().saveRig( m_currRigId );
   }

   // -------------------------------------------------------------
   public void activateTuner( boolean v ) throws ERError
   {
      getTransmitter().setTunerOn( v );
   }

   public void callbackSetCurrentRigName( String rigName ) throws ERError
   {
      callbackSetRigName( m_currBankId, m_currRigId, rigName );
   }

   public boolean isConnected()
   {
      return m_com != null && m_com.isConnected();
   }

   public int getInPort()
   {
      return( m_com != null ? m_com.getInPort() : 0 );
   }

   public int getOutPort()
   {
      return( m_com != null ? m_com.getOutPort() : 0 );
   }

}
