/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.tfx.TfxError;

public class ParamKnob extends EffectParam
{

   public ParamKnob( String key, String deco, int min, int max, int step )
   {
      super( key, deco );
      m_min = min;
      m_max = max;
      m_step = step;
   }

   public ParamKnob( String key, String deco )
   {
      this( key, deco, MININT, MAXINT, STEP0_1);
   }

   @Override
   protected void doSetValue( int v ) throws TfxError
   {
      if( v < m_min || v > m_max) throw new TfxError ( "invalid value for Knob " + getDecoration() + ": " + v );
      m_val = v;
   }
   
   @Override
   protected void doRender( RigRenderer r )
   {
      r.knobParam( getDecoration(), m_val );
   }
   
   @Override
   protected int doGetValue() { return m_val; }
   
   private int m_val;
   private int m_min;
   private int m_max;
   private int m_step;
}
