/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.ERError;
import gs.elevenhack.tfx.TfxError;

import java.util.ArrayList;
import java.util.Iterator;

public class Rig
{
   private ArrayList< Effect > m_effects = new ArrayList< Effect >();
   private Effect m_rigParams;
   private String m_name;
   private byte[] m_signature;
   private int m_version;
   
   public Rig() throws TfxError
   {
      m_rigParams = EffectFactory.createEffect(Effect.EF_TYPE_RIG_PARAM, Effect.RIG_PARAMS );
   }
   public void setName( String name ) { m_name = name; }
   public String getName() { return m_name; }
   
   public void AddEffect( Effect e ) { m_effects.add( e ); }
   public Effect getRigParams() { return m_rigParams; }
   
   public Iterator<Effect> getEffectIterator() { return m_effects.iterator(); }
   public void setSignature( byte[] s ) { m_signature = s; }
   
   public void render( RigRenderer r )
   {
      r.rigName( m_name );
      r.rigVersion(  m_version );
      m_rigParams.render( r );
      for( Effect ef : m_effects ) ef.render(  r  );
   }
   public Effect getEffect( int n ) throws ERError
   {
	   if( n < 0 || n >= m_effects.size() ) throw new ERError("rig has no effect " + n);
	   return m_effects.get( n );
   }
   
   public void setExpansionPackVersion( int v ) { m_version = v; }
   public int getExpansionPackVersion() { return m_version; }
   public byte[] getSignature() { return m_signature; }

}