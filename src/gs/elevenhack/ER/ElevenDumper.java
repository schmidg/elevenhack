/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.midi.SysEx;

public class ElevenDumper implements ElevenListener
{
	private ElevenRack m_rack;
	
	public ElevenDumper()
	{
	}

	@Override
	public void attachRack( ElevenRack er )
	{
	   // TODO Auto-generated method stub
	   m_rack = er; 
	}

	@Override
	public void detachRack()
	{
	   m_rack = null;
	}
	
	@Override
    public void onEffectCount(int n) throws ERError
    {
	    Term.println("Receivec effect count: " + n );
    }

	@Override
    public void onEffectDescription(int id)throws ERError
    {
		ElevenEffect ef = m_rack.getEffect( id );
		Term.println("Effect nb " + ef.efid + "[ " + SysEx.intToHex( ef.efid)+  " ] : " 
		+ ef.strId + " - " + ef.name );
    }

	@Override
    public void onUpdateRigList(int bank, int num)throws ERError
    {
	    // TODO Auto-generated method stub
	    
    }

	@Override
    public void onRigDescChanged() throws ERError
    {
		//
    }

	@Override
    public void onRigSelected(int bank, int num) throws ERError
    {
    }

   @Override
   public void onStartInitDevice() throws ERError
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void onDeviceInitialized() throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   @Override public void onTunerSwitched( boolean b ) throws ERError {}
   @Override public void onMainVolumeSet( int v ) throws ERError {} 

}
