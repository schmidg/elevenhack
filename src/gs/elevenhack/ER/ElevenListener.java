/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.ERError;

public interface ElevenListener
{
// @formatter:off
   public void attachRack( ElevenRack er );

   public void detachRack();

   public void onEffectCount( int n ) throws ERError;

   public void onEffectDescription( int id ) throws ERError;

   public void onUpdateRigList( int bank, int id ) throws ERError;

   public void onRigDescChanged() throws ERError;

   public void onRigSelected( int bank, int num ) throws ERError;

   public void onStartInitDevice() throws ERError;

   public void onDeviceInitialized() throws ERError;

   public void onTunerSwitched( boolean b ) throws ERError;

   public void onMainVolumeSet( int val ) throws ERError;

// @formatter:on
}
