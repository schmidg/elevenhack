/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import java.util.TreeMap;

import gs.elevenhack.SelectOption;
import gs.elevenhack.tfx.TfxError;

public class EffectAmpCab extends Effect
{
   // @formatter:off
   @SuppressWarnings( "serial" )
   final static TreeMap< Integer, String > m_ampMap = 
   new TreeMap< Integer, String >()
   { { 
      put( 0,  "59 Tweed Lux" );
      put( 1,  "59 Tweed Bass" );
      put( 2,  "64 Black Panel Lux Vib" );
      put( 3,  "64 Black Panel Lux Norm" );
      put( 4,  "66 AC Hi Boost" );
      put( 5,  "67 Black Duo" );
      put( 6,  "69 Plexiglas 100W" );
      put( 7,  "82 Lead 800 100W" );
      put( 8,  "85 M-2 Lead" );
      put( 9,  "89 SL-100 Drive" );
      put( 10, "89 SL-100 Crunch" );
      put( 11, "89 SL-100 Clean" );
      put( 12, "92 Treadplate Modern" );
      put( 13, "92 Treadplate Vintage" );
      put( 14, "DC Modern Overdrive" );
      put( 15, "DC Vintage Crunch" );
      put( -2147483648,  "59 Tweed Lux" );
      put( -1861152495,  "59 Tweed Bass" );
      put( -1574821342,  "64 Black Panel Lux Vib" );
      put( -1288490189,  "64 Black Panel Lux Norm" );
      put( -1002159036,  "66 AC Hi Boost" );
      put( -715827883,   "67 Black Duo" );
      put( -429496730,   "69 Plexiglas 100W" );
      put( -143165577,   "82 Lead 800 100W" );
      put( 143165576,    "85 M-2 Lead" );
      put( 429496729,    "89 SL-100 Drive" );
      put( 715827882,    "89 SL-100 Crunch" );
      put( 1002159035,   "89 SL-100 Clean" );
      put( 1095980628,   "69 Blue Line Bass" );
      put( 1112957526,   "64 Black Vib" );
      put( 1112764530,   "65 Black SR" );
      put( 1129136945,   "DC Modern Clean" );
      put( 1129726769,   "DC Vintage Clean" );
      put( 1128428337,   "DC Bass" );
      put( 1128940365,   "DC Modern 800" );
      put( 1145261362,   "DC Modern SOD" );
      put( 1145263666,   "DC Vintage OD" );
      put( 1247032373,   "65 J45" );
      put( 1288490188,   "92 Treadplate Modern" );
      put( 1296315184,   "93 MS 30" );
      put( 1345663095,   "68 Plexiglas 50w" );
      put( 1349277298,   "67 Plexiglas Vari" );
      put( 1447258221,   "65 Black Mini" );
      put( 1481917250,   "97 RB-01b Blue" );
      put( 1481917255,   "97 RB-01b Green" );
      put( 1481917266,   "97 RB-01b Red" );
      put( 1574821341,   "92 Treadplate Vintage" );
      put( 1861152494,   "DC Modern Overdrive" );
      put( 2147483647,   "DC Vintage Crunch" ); 
   }};
   // @formatter:on   

   // ------------------------------------- Amp Types.
   public EffectAmpCab( int code, int type ) throws TfxError
   {
      super( code, type );
      // @formatter:off
      mAddSelector( "sld6", "Amp type", new SelectOption[ ] 
            {
             new SelectOption( 0,  "59 Tweed Lux" ),
             new SelectOption( 1,  "59 Tweed Bass" ),
             new SelectOption( 2,  "64 Black Panel Lux Vib" ),
             new SelectOption( 3,  "64 Black Panel Lux Norm" ),
             new SelectOption( 4,  "66 AC Hi Boost" ),
             new SelectOption( 5,  "67 Black Duo" ),
             new SelectOption( 6,  "69 Plexiglas 100W" ),
             new SelectOption( 7,  "82 Lead 800 100W" ),
             new SelectOption( 8,  "85 M-2 Lead" ),
             new SelectOption( 9,  "89 SL-100 Drive" ),
             new SelectOption( 10, "89 SL-100 Crunch" ),
             new SelectOption( 11, "89 SL-100 Clean" ),
             new SelectOption( 12, "92 Treadplate Modern" ),
             new SelectOption( 13, "92 Treadplate Vintage" ),
             new SelectOption( 14, "DC Modern Overdrive" ),
             new SelectOption( 15, "DC Vintage Crunch" ),
             new SelectOption( -2147483648,  "59 Tweed Lux" ),
             new SelectOption( -1861152495,  "59 Tweed Bass" ),
             new SelectOption( -1574821342,  "64 Black Panel Lux Vib" ),
             new SelectOption( -1288490189,  "64 Black Panel Lux Norm" ),
             new SelectOption( -1002159036,  "66 AC Hi Boost" ),
             new SelectOption( -715827883,   "67 Black Duo" ),
             new SelectOption( -429496730,   "69 Plexiglas 100W" ),
             new SelectOption( -143165577,   "82 Lead 800 100W" ),
             new SelectOption( 143165576,    "85 M-2 Lead" ),
             new SelectOption( 429496729,    "89 SL-100 Drive" ),
             new SelectOption( 715827882,    "89 SL-100 Crunch" ),
             new SelectOption( 1002159035,   "89 SL-100 Clean" ),
             new SelectOption( 1095980628,   "69 Blue Line Bass" ),
             new SelectOption( 1112957526,   "64 Black Vib" ),
             new SelectOption( 1112764530,   "65 Black SR" ),
             new SelectOption( 1129136945,   "DC Modern Clean" ),
             new SelectOption( 1129726769,   "DC Vintage Clean" ),
             new SelectOption( 1128428337,   "DC Bass" ),
             new SelectOption( 1128940365,   "DC Modern 800" ),
             new SelectOption( 1145261362,   "DC Modern SOD" ),
             new SelectOption( 1145263666,   "DC Vintage OD" ),
             new SelectOption( 1247032373,   "65 J45" ),
             new SelectOption( 1288490188,   "92 Treadplate Modern" ),
             new SelectOption( 1296315184,   "93 MS 30" ),
             new SelectOption( 1345663095,   "68 Plexiglas 50w" ),
             new SelectOption( 1349277298,   "67 Plexiglas Vari" ),
             new SelectOption( 1447258221,   "65 Black Mini" ),
             new SelectOption( 1481917250,   "97 RB-01b Blue" ),
             new SelectOption( 1481917255,   "97 RB-01b Green" ),
             new SelectOption( 1481917266,   "97 RB-01b Red" ),
             new SelectOption( 1574821341,   "92 Treadplate Vintage" ),
             new SelectOption( 1861152494,   "DC Modern Overdrive" ),
             new SelectOption( 2147483647,   "DC Vintage Crunch" )});
   }
   // @formatter:on

   @Override
   public String getName()
   {
      return m_ampMap.get( getValue("sld6") );
   }
   
   public static TreeMap< Integer, String > getAmpMap() { return m_ampMap; }
}
