/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.tfx.TfxError;

public class ParamSwitch extends EffectParam
{
   public ParamSwitch( String key, String deco )
   {
      super( key, deco );
   }
   
   public ParamSwitch( String key, String deco,
                       int valTrue, int valFalse )
   {
      super( key, deco );
   }

   @Override
   public void doRender( RigRenderer r )
   {
      r.switchParam( getKey(), m_val );
   }
   
   @Override
   protected void doSetValue( int v ) throws TfxError
   {
      m_val = v;
   }
   
   @Override
   protected int doGetValue() { return m_val; }

   private int m_val = 0;

}
