/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.SelectOption;
import gs.elevenhack.tfx.TfxError;

public class ParamSelector extends EffectParam
{
   
   public ParamSelector( String key, String deco, SelectOption[] opts )
   {
      super( key, deco );
      m_opts = opts;
   }

   @Override
   protected void doSetValue( int v)
   {
      boolean found = false;
      for( SelectOption o : m_opts ) {
         if( v == o.val ) {
            m_val = o.val;
            break;
         }
      }
      if( !found )  new TfxError ( "invalid value for Selector " + getDecoration() + ": " + v );
   }

   @Override
   protected void doRender( RigRenderer r )
   {
      r.selectorParam( getDecoration(),m_opts, m_val );
   }
   
   @Override
   protected int doGetValue() { return m_val; }

   private SelectOption[] m_opts;
   private int m_val;
}
