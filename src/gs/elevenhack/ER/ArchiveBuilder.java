/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import java.util.Arrays;

import javax.swing.ProgressMonitor;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.midi.ReceiverCallback;
import gs.elevenhack.midi.SysEx;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

public class ArchiveBuilder extends ReceiverCallback
{
   private ElevenRack    m_rack;
   private static byte[] expectedSeq = { SysEx.RESPOND, SysEx.CMD_GET_BULK_TFX };
   private String m_errorString;
   private RigArchive m_arch;
   private ProgressMonitor m_pMon;
   private int m_bank;
   private String m_fname;
   private int m_currRigNum = 0;
   
   // -------------------------------------------------------------------------
   public ArchiveBuilder( ElevenRack rack, int bank, ProgressMonitor p )
   {
      super( expectedSeq );
      m_bank = bank;
      m_rack = rack;
      m_pMon = p;
   }

   // -------------------------------------------------------------------------
   public void buildArchive( String fname ) throws ERError
   {
      m_fname = fname;
      if( m_pMon != null) {
         m_pMon.setNote( "Creating Bank Achive " + fname + "..." );
         m_pMon.setMinimum( 0 );
         m_pMon.setMaximum( 103 );
         m_pMon.setProgress( 0 );
      }
      m_arch = new RigArchive( fname );
      m_rack.getReceiver().setCallback( this );
      m_currRigNum = 0;
      m_rack.getTransmitter().selectCurrentRig( m_bank, m_currRigNum );
      m_rack.getTransmitter().requestBulkRig();

      if( m_errorString != null ) throw new ERError( m_errorString );
   }

   // -------------------------------------------------------------------------
   @Override
   public void onExpectedSeq( byte[] msg )
   {
      int bulkoffset = 6;

      byte[] tmp = Arrays.copyOfRange( msg, bulkoffset, msg.length );
      byte[] bulk = SysEx.extractFrom7bits( tmp, 0, tmp.length );
 
      try {
         Rig rig = new Rig();

         TfxParser p = new TfxParser();
         p.parseMsg( rig, bulk );
         
         String name = rig.getName();
         if( m_pMon != null) {
            m_pMon.setProgress( m_currRigNum );
            m_pMon.setNote( "Please, don't change presets on your device manually.\nSaving Rig: " + name );
         }

         name = m_fname + "-EHBank/" + ElevenRack.LocToName( m_currRigNum ) + "-" + name + ".tfx";
         m_arch.addRigToArchive( name, bulk );
         if( m_currRigNum < 103) {
            m_rack.getReceiver().setCallback( this );
            ++ m_currRigNum;
            m_rack.getTransmitter().selectCurrentRig( m_bank, m_currRigNum );
            m_rack.getTransmitter().requestBulkRig();
         }
         else {
            m_arch.closeArchive();
            m_rack.getReceiver().clearCallback();
         }
      }
      catch( ERError e ) {
         Term.println( e.getMessage() );
         m_rack.getReceiver().clearCallback();
      }
      catch( TfxError e ) {
         Term.println( e.getMessage() );
         m_rack.getReceiver().clearCallback();
      }
      
   }
}
