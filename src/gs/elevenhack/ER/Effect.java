/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.ER;

import gs.elevenhack.SelectOption;
import gs.elevenhack.tfx.TfxError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

public class Effect
{
   // ------------------------------------- Effect Code
   final static int                  RIG_PARAMS        = -1;

   final static int                  VOLUME_PEDAL_1    = 38;
   final static int                  VOLUME_PEDAL_2    = 72;

   final static int                  DISTO_TRI_KNOB    = 29;
   final static int                  DISTO_BLACK_OP    = 30;
   final static int                  DISTO_GREEN_JRC   = 31;
   final static int                  DISTO_WHITE_BOOST = 87;
   final static int                  DISTO_DC          = 91;

   final static int                  WAH_SHINE         = 36;
   final static int                  WAH_BLACK         = 55;

   final static int                  FX_LOOP_1         = 16;
   final static int                  FX_LOOP_2         = 17;
   final static int                  FX_LOOP_3         = 56;
   final static int                  FX_LOOP_4         = 57;
   final static int                  FX_LOOP_5         = 73;
   final static int                  FX_LOOP_6         = 14;
   final static int                  FX_LOOP_7         = 15;
   
   final static int                  COMP_GRAY         = 32;
   final static int                  COMP_DYN_1        = 85;
   final static int                  COMP_DYN_2        = 86;

   final static int                  EQ_GRAPHIC_1      = 33;
   final static int                  EQ_GRAPHIC_2      = 50;
   final static int                  EQ_PARA_1         = 78;
   final static int                  EQ_PARA_2         = 79;

   final static int                  ORANGE_PHASER_1   = 34;
   final static int                  ORANGE_PHASER_2   = 71;

   final static int                  VIBE_PHASER_1     = 35;
   final static int                  VIBE_PHASER_2     = 46;

   final static int                  REV_SPRING_1      = 37;
   final static int                  REV_SPRING_2      = 47;

   final static int                  REV_STEREO_1      = 51;
   final static int                  REV_STEREO_2      = 52;
   final static int                  REV_STEREO_3      = 53;

   final static int                  DELAY_BBD_1       = 27;
   final static int                  DELAY_BBD_2       = 48;
   final static int                  DELAY_TAPE_1      = 28;
   final static int                  DELAY_TAPE_2      = 49;
   final static int                  DELAY_DYN_1       = 80;
   final static int                  DELAY_DYN_2       = 81;
   final static int                  DELAY_DYN_3       = 82;

   final static int                  CHORUS_VIB_1      = 11;
   final static int                  CHORUS_VIB_2      = 39;
   final static int                  CHORUS_VIB_3      = 40;
   final static int                  CHORUS_MULTI_1    = 88;
   final static int                  CHORUS_MULTI_2    = 89;
   final static int                  CHORUS_MULTI_3    = 90;

   final static int                  FLANGER_1         = 69;
   final static int                  FLANGER_2         = 70;

   final static int                  ROTO_SPEAKER_1    = 75;
   final static int                  ROTO_SPEAKER_2    = 76;
   final static int                  ROTO_SPEAKER_3    = 77;

   final static int                  AMP_CAB           = 12;

   // ------------------------------------ Effect Type

   public final static int                  EF_TYPE_AMPCAB    = 0;
   public final static int                  EF_TYPE_FXLOOP    = 1;
   public final static int                  EF_TYPE_VOL       = 2;
   public final static int                  EF_TYPE_WAH       = 3;
   public final static int                  EF_TYPE_MOD       = 4;
   public final static int                  EF_TYPE_REVERB    = 5;
   public final static int                  EF_TYPE_DELAY     = 6;
   public final static int                  EF_TYPE_DISTO     = 7;
   public final static int                  EF_TYPE_FX1       = 8;
   public final static int                  EF_TYPE_FX2       = 9;
   public final static int                  EF_TYPE_INPUT     = 10;
   public final static int                  EF_TYPE_0B        = 11;
   public final static int                  EF_TYPE_0C        = 12;
   public final static int                  EF_TYPE_0D        = 13;
   public final static int                  EF_TYPE_0E        = 14;
   public final static int                  EF_TYPE_0F        = 15;
   public final static int                  EF_TYPE_RIG_PARAM = 16;

   
   final static int                  MININT            = 0x80000000;
   final static int                  MAXINT            = 0x7FFFFFFF;

   final static String[]             m_effectTypeNames = { "00 Amp/Cab",
         "01 FX Loop", "02 Vol    ", "03 Wah    ", "04 Mod    ", "05 Reverb ",
         "06 Delay  ", "07 Disto  ", "08 FX1    ", "09 FX2    ", "0A        ",
         "0B Input  ", "0C\t", "0D\t", "0E\t", "0F\t", "10 Globals" };

   final static String[]             m_effectTypeShort = { "AMP", "Loop",
         "VOL", "WAH", "MOD", "REV", "DLY", "Dist", "FX 1", "FX 2", "0A", "In",
         "0C", "0D", "0E", "0F", "GLOB"               };
   static TreeMap< Integer, String >       m_effectNames;

   // ---------------------------------------------------------------------
   public Effect( int effectClass, int effectId )
   {
      m_effectId = effectId;
      m_effectClass = effectClass;
      m_params = new TreeMap< String, EffectParam >();
      mBuildEffect( effectClass, effectId );
   }

   // ---------------------------------------------------------------------
   public int getId()
   {
      return m_effectId;
   }

   // ---------------------------------------------------------------------
   public void setValue( String key, int val ) throws TfxError
   {
      boolean found = false;

      for ( EffectParam p : m_params.values() ) {
         if( p.getKey().equals( key ) ) {
            p.setValue( val );
            found = true;
            break;
         }
      }
      if( !found ) {
         mAddKnob( key, "UNKNOWN " + key );
         for ( EffectParam p : m_params.values() ) {
            if( p.getKey().equals( key ) ) {
               p.setValue( val );
               found = true;
               break;
            }
         }
      }
   }
   
   //----------------------------------------------------------------------
   protected int getValue( String key )
   {
      return m_params.get( key ).getValue();
   }

   // ---------------------------------------------------------------------
   public void render( RigRenderer render )
   {

      render.newEffectName( m_effectId, m_effectClass, getName() );

      for ( EffectParam ep : m_params.values() ) {
         try {
            ep.render( render );
         }
         catch( TfxError e ) {
            e.printStackTrace();
         }
      }
   }

   // ----------------------------------------------------------------------
   public String getName()
   {
      return m_name;
   }
   
   //-----------------------------------------------------------------------
   public boolean isKnown()
   {
      return !m_unknown;
   }
   
   //-----------------------------------------------------------------------
   public ArrayList< Integer > getSiblings() { return m_siblings; }
   
   // ----------------------------------------------------------------------
   public static String getClassName( int n )
   {
      return m_effectTypeNames[n];
   }

   // ----------------------------------------------------------------------
   public static String getClassShortName( int n )
   {
      return m_effectTypeShort[n];
   }

   // ---------------------------------------------------------------------
   private void mBuildEffect( int effectClass, int effectId )
   {
      mAddSwitch( "bypa", "Bypass    " );
      switch ( effectId ) {
         case RIG_PARAMS:
            mCreateRigParams();

            break;

         case VOLUME_PEDAL_1:
         case VOLUME_PEDAL_2:
            m_siblings = new ArrayList< Integer >(
                                                   Arrays.asList( VOLUME_PEDAL_1,
                                                                  VOLUME_PEDAL_2 ) );
            mCreateVolumePedal();
            break;

         case DISTO_TRI_KNOB:
         case DISTO_BLACK_OP:
         case DISTO_GREEN_JRC:
         case DISTO_WHITE_BOOST:
         case DISTO_DC:
            mCreateDisto();
            break;

         case FX_LOOP_1:
         case FX_LOOP_2:
         case FX_LOOP_3:
         case FX_LOOP_4:
         case FX_LOOP_5:
         case FX_LOOP_6:
         case FX_LOOP_7:
            m_siblings = new ArrayList< Integer >( Arrays.asList( FX_LOOP_1,
                                                                  FX_LOOP_2,
                                                                  FX_LOOP_3,
                                                                  FX_LOOP_4,
                                                                  FX_LOOP_5,
                                                                  FX_LOOP_6,
                                                                  FX_LOOP_7 ));
            mCreateFxLoop();
            break;

         case WAH_SHINE:
         case WAH_BLACK:
            mCreateWah();
            break;

         case CHORUS_VIB_1:
         case CHORUS_VIB_2:
         case CHORUS_VIB_3:
            m_siblings = new ArrayList< Integer >( Arrays.asList( CHORUS_VIB_1,
                                                                  CHORUS_VIB_2,
                                                                  CHORUS_VIB_3 ) );
            mCreateChorusVibe();
            break;

         case EQ_GRAPHIC_1:
         case EQ_GRAPHIC_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( EQ_GRAPHIC_1,
                                                                  EQ_GRAPHIC_2 ) );
            mCreateGraphicEq();
            break;
            
         case EQ_PARA_1:
         case EQ_PARA_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( EQ_PARA_1,
                                                                  EQ_PARA_2 ) );
            mCreateParaEq();
            break;

         case ORANGE_PHASER_1:
         case ORANGE_PHASER_2:
            m_siblings = new ArrayList< Integer >(
                                                   Arrays.asList( ORANGE_PHASER_1,
                                                                  ORANGE_PHASER_2 ) );
            mCreateOrangPhaser();
            break;

         case COMP_GRAY:
            mCreateCompGray();
            break;

         case COMP_DYN_1:
         case COMP_DYN_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( COMP_DYN_1,
                                                                  COMP_DYN_2 ) );
            mCreateCompDyn();
            break;

         case VIBE_PHASER_1:
         case VIBE_PHASER_2:
            m_siblings = new ArrayList< Integer >(
                                                   Arrays.asList( VIBE_PHASER_1,
                                                                  VIBE_PHASER_2 ) );
            mCreateVibePhaser();
            break;

         case REV_SPRING_1:
         case REV_SPRING_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( REV_SPRING_1,
                                                                  REV_SPRING_2 ) );
            mCreateRevSpring();
            break;

         case REV_STEREO_1:
         case REV_STEREO_2:
         case REV_STEREO_3:
            m_siblings = new ArrayList< Integer >( Arrays.asList( REV_STEREO_1,
                                                                  REV_STEREO_2,
                                                                  REV_STEREO_3 ) );

            mCreateRevStereo();
            break;

         case DELAY_BBD_1:
         case DELAY_BBD_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( DELAY_BBD_1,
                                                                  DELAY_BBD_2 ) );
            mCreateDelayBBD();
            break;

         case DELAY_TAPE_1:
         case DELAY_TAPE_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( DELAY_TAPE_1,
                                                                  DELAY_TAPE_2 ) );

            mCreateDelayTape();
            break;

         case DELAY_DYN_1:
         case DELAY_DYN_2:
         case DELAY_DYN_3:
            m_siblings = new ArrayList< Integer >( Arrays.asList( DELAY_DYN_1,
                                                                  DELAY_DYN_2,
                                                                  DELAY_DYN_3 ) );

            mCreateDelayDyn();
            break;

         case CHORUS_MULTI_1:
         case CHORUS_MULTI_2:
         case CHORUS_MULTI_3:
            m_siblings = new ArrayList< Integer >(
                                                   Arrays.asList( CHORUS_MULTI_1,
                                                                  CHORUS_MULTI_2,
                                                                  CHORUS_MULTI_3 ) );
            mCreateChorusMulti();
            break;

         case FLANGER_1:
         case FLANGER_2:
            m_siblings = new ArrayList< Integer >( Arrays.asList( FLANGER_1,
                                                                  FLANGER_2 ) );
            mCreateFlanger();
            break;

         case ROTO_SPEAKER_1:
         case ROTO_SPEAKER_2:
         case ROTO_SPEAKER_3:
            m_siblings = new ArrayList< Integer >(
                                                   Arrays.asList( ROTO_SPEAKER_1,
                                                                  ROTO_SPEAKER_2,
                                                                  ROTO_SPEAKER_3 ) );

            mCreateRotoSpeaker();
            break;

         case AMP_CAB: // Taken care by inherited EffectAmpCab
            break;

         default:
            m_name = "Unknown code " + effectId;
            m_unknown = true;
            break;
      }
   }

   private void mCreateRevSpring()
   {
      m_name = "Spring_Reverb";
   }

   private void mCreateRevStereo()
   {
      m_name = "Stereo Reverb";
   }

   private void mCreateDelayBBD()
   {
      m_name = "BBDDelay";
   }

   private void mCreateDelayTape()
   {
      m_name = "Tape Delay";
   }

   private void mCreateDelayDyn()
   {
      m_name = "Dyn Delay";
   }

   private void mCreateChorusMulti()
   {
      m_name = "Multi Chorus";
   }

   private void mCreateFlanger()
   {
      m_name = "Flanger";
   }

   private void mCreateRotoSpeaker()
   {
      m_name = "Roto Speaker";
   }

   private void mCreateVibePhaser()
   {
      m_name = "Vibe Phaser";
   }

   // ----------------------------------------------------------------
   private void mCreateRigParams()
   {
      m_name = "Rig Params";
      mAddKnob( "RVol", "Volume" );
      mAddSwitch( "RMno", "Mono/Stereo" );
      mAddKnob( "Tmpo", "Tempo", 120000, 6000000, 1 );
      mAddKnob( "FXc1", "Fxc1" );
      mAddKnob( "FXc2", "Fxc2" );
      mAddKnob( "FXc3", "Fxc3" );
      mAddKnob( "FXc4", "Fxc4" );
      mAddKnob( "GlSF", "GlSF" );
      mAddKnob( "Msyc", "Msyc" );
      mAddKnob( "RslL", "RslL" );
      mAddKnob( "Vol1", "Vol1" );
      mAddKnob( "Vol2", "Vol2" );

      mAddSelector( "WorB", "Input Selector", new SelectOption[ ] {
            new SelectOption( 60, "Guitar" ), new SelectOption( 61, "Mic" ),
            new SelectOption( 20, "Line L" ), new SelectOption( 21, "Line R" ),
            new SelectOption( 63, "Line L + R" ),
            new SelectOption( 22, "Digital L" ),
            new SelectOption( 23, "Digital R" ),
            new SelectOption( 64, "Digital L + R" ),
            new SelectOption( 18, "Re-Amp" ) } );

      mAddSelector( "WstB", "WstB constant",
                    new SelectOption[ ] { new SelectOption( 11, "11" ) } );

      mAddSelector( "PIGI", "True Z Selector", new SelectOption[ ] {
            new SelectOption( 125, "Auto" ), new SelectOption( 0, "1 MOhm" ),
            new SelectOption( 1, "1 MOhm + Cap" ),
            new SelectOption( 2, "230 kOmh" ),
            new SelectOption( 3, "230 kOmh + Cap" ),
            new SelectOption( 4, "90  kOmh" ),
            new SelectOption( 5, "90  kOmh + Cap" ),
            new SelectOption( 6, "70  kOmh" ),
            new SelectOption( 7, "70  kOmh + Cap" ),
            new SelectOption( 8, "32  kOmh" ),
            new SelectOption( 9, "32  kOmh + Cap" ),
            new SelectOption( 10, "22  KOmh" ),
            new SelectOption( 11, "22  KOmh + Cap" ) } );

      mAddSelector( "ExpT", "Exp.Pedal Selector", new SelectOption[ ] {
            new SelectOption( 0, "None" ),
            new SelectOption( 1, "Multiple FX" ),
            new SelectOption( 2, "Rig Volume" ),
            new SelectOption( 3, "Volume" ), new SelectOption( 4, "Wah" ) } );
   }

   // -------------------------------------------------------------------
   private void mCreateVolumePedal()
   {
      m_name = "Volume Pedal";
      mAddKnob( "Vol ", "Position" );
      mAddKnob( "Min ", "Min Volume" );
      mAddSwitch( "Tapr", "Linear/Log" );
   }

   // -------------------------------------------------------------------
   private void mCreateDisto()
   {
      switch ( m_effectId ) {
         case DISTO_TRI_KNOB:
            m_name = "Tri Knob Disto";
            mAddKnob( "Driv", "Sustain" );
            mAddKnob( "Tone", "Tone" );
            mAddKnob( "Levl", "Level" );
            break;

         case DISTO_BLACK_OP:
            m_name = "Black Op Disto";
            mAddKnob( "Driv", "Distortion" );
            mAddKnob( "Tone", "Cut" );
            mAddKnob( "Levl", "Volume" );
            break;

         case DISTO_GREEN_JRC:
            m_name = "Green JRC Disto";
            mAddKnob( "Driv", "Overdrive " );
            mAddKnob( "Tone", "Tone      " );
            mAddKnob( "Levl", "Level     " );
            break;

         case DISTO_WHITE_BOOST:
            m_name = "White Boost Disto";
            mAddKnob( "Driv", "Gain   " );
            mAddKnob( "Treb", "Treble " );
            mAddKnob( "Bass", "Bass   " );
            mAddKnob( "Levl", "Volume " );
            break;

         case DISTO_DC:
            m_name = "DC_Disto";
            mAddKnob( "Driv", "Distortion " );
            mAddKnob( "Treb", "Treble     " );
            mAddKnob( "Bass", "Bass       " );
            mAddKnob( "Levl", "Volume     " );
            break;
      }

   }

   // ---------------------------------------------------------------------
   private void mCreateWah()
   {
      switch ( m_effectId ) {
         case WAH_SHINE:
            m_name = "Sine Wah";
            mAddKnob( "Filt", "Position " );
            mAddKnob( "VxCr", "VxCr" );
            break;

         case WAH_BLACK:
            m_name = "Black Wah";
            mAddKnob( "Filt", "Position " );
            mAddKnob( "VxCr", "VxCr" );
            break;
      }
   }

   // ----------------------------------------------------------------------
   private void mCreateChorusVibe()
   {
      m_name = "Chorus/Vibrato";
      mAddSwitch( "Mode", "Chorus/Vibrato " );
      mAddKnob( "ChIn", "Chorus         " );
      mAddKnob( "VbDp", "Vibrato Depth  " );
      mAddKnob( "VbRt", "Vibrato Rate   " );
      mAddSelector( "Sync", "Sync", new SelectOption[ ] {
            new SelectOption( 0, "None" ), new SelectOption( 1, "Whole Note" ),
            new SelectOption( 2, "Whole dot" ),
            new SelectOption( 3, "Half Note" ),
            new SelectOption( 4, "Half dot" ),
            new SelectOption( 5, "Quarter Note" ),
            new SelectOption( 6, "Quarter dot" ),
            new SelectOption( 7, "Eighth Note" ),
            new SelectOption( 8, "Eighth dot" ),
            new SelectOption( 9, "Sixteenth Note" ),
            new SelectOption( 10, "Sixteenth dot" ),
            new SelectOption( 11, "Thirty-second Note" ),
            new SelectOption( 13, "Thirty-second dot" ) } );
   }

   // ---------------------------------------------------------------------
   private void mCreateFxLoop()
   {
      m_name = "Fx Loop";
   }

   private void mCreateOrangPhaser()
   {
      m_name = "Orange Phaser";
      mAddKnob( "Sped", "Rate" );
      mAddSwitch( "Sync", "Sync" );
   }

   private void mCreateGraphicEq()
   {
      m_name = "Graphic EQ";
      mAddKnob( "LwSh", "100  Hz  (-12 to +12)" );
      mAddKnob( "LMGn", "370  Hz  (-18 to +18)" );
      mAddKnob( "MGn ", "800  Hz  (-18 to +18)" );
      mAddKnob( "HMGn", "2    KHz (-18 to +18)" );
      mAddKnob( "HiSh", "3.25 KHz (-12 to +12)" );
      mAddKnob( "Out ", "Output   (-20 to +6 )" );
   }

   private void mCreateCompGray()
   {
      m_name = "Gray Compressor";
   }

   private void mCreateCompDyn()
   {
      m_name = "Dyn Compressor";
   }

   private void mCreateParaEq()
   {
      m_name = "Para EQ";
   }

   // ---------------------------------------------------------------------
   private void mAddSwitch( String name, String deco )
   {
      m_params.put( name, new ParamSwitch( name, deco ) );
   }

   // ---------------------------------------------------------------------
   private void mAddKnob( String name, String deco )
   {
      m_params.put( name, new ParamKnob( name, deco ) );
   }

   // ---------------------------------------------------------------------
   private void mAddKnob( String name, String deco, int min, int max, int step )
   {
      m_params.put( name, new ParamKnob( name, deco, min, max, step ) );
   }

   // ---------------------------------------------------------------------
   protected void mAddSelector( String name, String deco, SelectOption[] opts )
   {
      m_params.put( name, new ParamSelector( name, deco, opts ) );
   }

   // ---------------------------------------------------------------------
   private TreeMap< String, EffectParam > m_params;
   private int                      m_effectId;
   private int                      m_effectClass;
   private String                   m_name;
   private ArrayList< Integer >     m_siblings = new ArrayList< Integer >();
   private boolean                  m_unknown  = false;
}
