/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack;

import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.midi.MidiCom;

public class Main
{
	enum Command {
		CONSOLE,
		SENDTFX
	};
	
	public static void main(String[] args)
	{
		Term.activated = true;
		Command cmdArg;
		
		if (args.length == 0) {
			//MainFrame.run( new MainFrame( null ), 250, 900 );
		}
		else {
			int i = 0, j;
			String arg;
			char flag;
			boolean vflag = false;
			boolean stdoutput = false;
			String fileParam = null;
			cmdArg = null;
		
			int numDevIn = -1;
			int numDevOut = -1;

			while (i < args.length && args[i].startsWith( "-" )) {
				arg = args[i++];
				Term.println( "param: [" + arg + "]" );
				// use this type of check for "wordy" arguments
				if (arg.equals( "-verbose" )) {
					Term.println( "verbose mode on" );
					vflag = true;
				}

				else if (arg.equals( "-midilist" )) {
					listMidiDevices();
					return;
				}

				else if (arg.equals( "-console" )) {
					if( cmdArg !=  null ) {
						System.err.println( "args must have only one main command." );
						System.exit( -1 );
					}
					cmdArg = Command.CONSOLE;
				}
				else if (arg.equals( "-sendtfx" )) {
					if( cmdArg !=  null ) {
						System.err.println( "args must have only one main command." );
						System.exit( -1 );
					}
					cmdArg = Command.SENDTFX;
				}

				else if (arg.equals( "-i" )) {
					if (i < args.length) numDevIn = Integer
					        .parseInt( args[i++] );
					else System.err.println( "-i requires a device number" );
					if (vflag)
					    Term.println( "using input device " + numDevIn );
				}
				else if (arg.equals( "-o" )) {
					if (i < args.length) numDevOut = Integer
					        .parseInt( args[i++] );
					else System.err.println( "-o requires a device number" );
					if (vflag)
					    Term.println( "using input device " + numDevOut );
				}

				// use this type of check for a series of flag arguments
				else {
					for (j = 1; j < arg.length(); j++) {
						flag = arg.charAt( j );
						switch (flag) {
						case 't':
							stdoutput = true;
							if (vflag)
							    Term.println( "Stdout output selected" );
							break;

						default:
							System.err.println( "ParseCmdLine: illegal option "
							        + flag );
							break;
						}
					}
				}
				if( args.length == i+1 ) {
					fileParam = args[i];
				}
			}
			if (cmdArg == Command.CONSOLE) {
				if (numDevIn == -1 || numDevOut == -1) {
					System.err
					        .println( "Error, must provide -i and -o parameters for console mode." );
					System.exit( -1 );
				}
				Term.println( "Listening to midi sysex..." );
				try {
					ElevenRack er = new ElevenRack();
					er.open( numDevIn, numDevOut );
					er.midiConsole();
				}
				catch (ERError e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if( cmdArg == Command.SENDTFX ) {
				if (fileParam == null) {
					System.err.println ("missing file parameter.");
					System.exit( -1 );
				}
				Term.println("Send sysex tom be implemented.");
			}
			else if (!stdoutput) {
				if (fileParam == null) {
					System.err.println ("missing file parameter.");
					System.exit( -1 );
				}
				//MainFrame.run( new MainFrame( fileParam ), 250, 900 );
			}
			else {
			   System.err.println ("missing command parameter.");
			   System.exit( -1 );
			}
		}
	}

	// -----------------------------------------------------------------
	static void listMidiDevices()
	{

		try {
			Term.println( "Midi devices: \n" + MidiCom.listMidiDevices() );
		} catch (ERError e) {
			// TODO Auto-generated catch block
			Term.println(e.getMessage());
		}
	}
}
