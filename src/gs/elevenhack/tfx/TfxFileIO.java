/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.tfx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class TfxFileIO
{
   //-----------------------------------------------------
   static public void writeTfx( String fname, byte[] msg ) throws TfxError, IOException
   {
      FileOutputStream out = new FileOutputStream( new File( fname ));
      writeTfx( out, msg );
      out.close();
   }
   
   //-----------------------------------------------------
   static public void writeTfx( OutputStream out, byte[] msg ) throws TfxError, IOException
   {
      TfxParser p = new TfxParser();

      byte[] header = { 0, 0, 0, 0, 0, 0, 0, 0, 
                        'D', 'i', 'g', 'i', 'E', 'l', 'v', 'R',
                        'E', 'L', 'V', 'h', 'R', 'i', 'g', ' ',
                        0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0,
                        };
      int msgLen = msg.length - (msg.length % 4);       
      int fileSize = header.length + msgLen;

      header[0] = (byte)(fileSize >>> 24 & 0xFF);
      header[1] = (byte)(fileSize >>> 16 & 0xFF);
      header[2] = (byte)(fileSize >>> 8 & 0xFF);
      header[3] = (byte)(fileSize & 0xFF);
      
      out.write( header );
      
      out.write( msg, 0, msgLen );
   }   
}

