/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.tfx;

import gs.elevenhack.ParseUtils;
import gs.elevenhack.Term;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class Section
{
	private TreeMap<String, Integer > m_keyVals;
	private char m_sectionId;
	
	public Section(TfxParser p) throws TfxError
	{
		m_keyVals = new TreeMap<String, Integer >();
		byte[] headq = p.readQuadlet();
		if ( headq == null ) {
			return; // Return Empty Section.
		}

		int byteSize = (0xFF & headq[0]) + (0xFF & headq[1]) * 256;
		if (byteSize % 8 != 0) {
			throw new TfxError( "Incorect Block size : " + byteSize
			        + " in quadlet " + ParseUtils.quadToString( headq ) );
		}

		StringBuilder sb = new StringBuilder();
		sb.append( (char) headq[2] );
		sb.append( (char) headq[3] );
		m_sectionId = (char) headq[3];

		int nbVals = byteSize / 8;
		for (int i = 0; i < nbVals; i++) {
			byte[] q = p.readQuadlet();
			if (q == null) {
				throw new TfxError( "Error while parsing key name in section "
				        + m_sectionId );
			}
			String key = ParseUtils.quadToKey( q );

			q = p.readQuadlet();
			if (q == null) {
				throw new TfxError( "Error while parsing value in section "
				        + m_sectionId );
			}
			//Term.println( "Setting: " + key + " -> " + ParseUtils.quadToInt( q ) );
			m_keyVals.put(key, ParseUtils.quadToInt( q ) );
		}
	}
	
	public Set< Entry< String, Integer >> getEntries() { return m_keyVals.entrySet(); }
	public Integer get(String key) { return m_keyVals.get( key ); }
	public char getSectionId() { return m_sectionId; }
	public boolean empty()
    {
	    return m_keyVals.isEmpty();
    }
}
