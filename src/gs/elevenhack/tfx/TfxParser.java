/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.tfx;

import gs.elevenhack.ERError;
import gs.elevenhack.ParseUtils;
import gs.elevenhack.ER.Effect;
import gs.elevenhack.ER.EffectFactory;
import gs.elevenhack.ER.Rig;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;

public class TfxParser
{
   private InputStream           m_in;
   private String                m_rigName;
   private byte[]                m_headerCode;
   private byte[]                m_version;

   //-------------------------------------------------------------
   public TfxParser()
   {
   }

   // ------------------------------------------------------------
   void parseFileHeader(Rig rig)
   {
      ParseUtils.quadToString( readQuadlet() );
      ParseUtils.quadToString( readQuadlet() );

      readString( 16 );

      readBytes( 32 );

      m_version = readQuadlet();
      m_headerCode = readQuadlet();
      rig.setSignature( m_headerCode );
      
      StringBuilder nameBuilder = new StringBuilder();
      for ( int i = 0; i < 7; i++ )
         nameBuilder.append( ParseUtils.quadToKey( readQuadlet() ) );
      m_rigName = nameBuilder.toString().trim();
      rig.setName( m_rigName );
   }

   // ------------------------------------------------------------
   void parseMsgHeader(Rig rig)
   {
      m_version = readQuadlet();
      m_headerCode = readQuadlet();
      rig.setSignature( m_headerCode );

      StringBuilder nameBuilder = new StringBuilder();
      for ( int i = 0; i < 7; i++ )
         nameBuilder.append( ParseUtils.quadToKey( readQuadlet() ) );
      m_rigName = nameBuilder.toString();
      m_rigName = m_rigName.substring(0,m_rigName.indexOf( (char)(0) ));
      rig.setExpansionPackVersion( m_version[0] );
      rig.setName(  m_rigName );
   }

   // ------------------------------------------------------------
   public void parseBody( Rig rig ) throws TfxError
   {
      Section toc = parseSection();
      if( toc == null) {
            throw new TfxError("Error while parsing section 1 of the file");
      }
      rig.getRigParams();
      
      // Set global rig variable.
      for( Entry<String, Integer> entry : toc.getEntries() ) {
         if( ! entry.getKey().startsWith( "W", 0 ) || 
               entry.getKey().equals( "WorB" ) ||
               entry.getKey().equals( "WstB" ) ) {
            rig.getRigParams().setValue( entry.getKey(), entry.getValue() );
         }
      }
      
      // Set Rig Structure.

      for( char c = 'C'; c <= 'L'; ++ c) {
         byte[] efId = { (byte)c, 'r', 'o', 'W' };
         byte[] efCat = { (byte)c, 't', 's', 'W' };

         int effectId = toc.get( ParseUtils.quadToKey( efId ));
         int category = toc.get( ParseUtils.quadToKey( efCat ));
         rig.AddEffect( EffectFactory.createEffect( category, effectId ) );
      }

      int nbSections = 0;
      for( Section s = parseSection(); s != null; s = parseSection() ) {

         char secId = s.getSectionId();
         int numSec = secId - 'C';
        
         Effect ef;
		try {
			ef = rig.getEffect( numSec );
		} catch (ERError e) {
			throw new TfxError( e.getMessage() );
		}
         
         for( Entry<String, Integer> entry : s.getEntries() ) {
            ef.setValue( entry.getKey(), entry.getValue() );
         }
         ++ nbSections;
      }
      if( nbSections < 10 ) throw new TfxError("missing section, found only " + nbSections );
   }

   // ------------------------------------------------------------
   public void parseFile( Rig rig, String fname ) throws TfxError, IOException
   {
      m_in = new FileInputStream( new File( fname ));
      parseFileHeader( rig );
      parseBody( rig );
      m_in.close();
   }
   
   // ------------------------------------------------------------
   public void parseFile( Rig rig, File f ) throws TfxError, IOException
   {
      m_in = new FileInputStream( f );
      parseFileHeader( rig );
      parseBody( rig );
      m_in.close();
   }

   //-------------------------------------------------------------
   public String retrieveName( byte[] msg ) throws TfxError
   {
      m_in = new ByteArrayInputStream( msg );
      Rig r = new Rig();
      parseMsgHeader( r );
      r.getName();
      return r.getName();
   }
   // ------------------------------------------------------------
   public void parseMsg( Rig rig, byte[] msg) throws TfxError
   {
      m_in = new ByteArrayInputStream( msg );
      parseMsgHeader( rig );
      parseBody( rig );
   }

   // ------------------------------------------------------------
   public byte[] extractBody( File f ) throws IOException
   {
      m_in = new FileInputStream( f );
      m_in.skip(56);
      byte[] res = new byte[ m_in.available()];
      m_in.read(res);
      return res;
   }
   
   // ------------------------------------------------------------
   Section parseSection() throws TfxError
   {
      try {
         if( m_in.available() < 12  ) return null;
      }
      catch( IOException e1 ) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
         return null;
      }
      return new Section( this );
   }
   //-------------------------------------------------------------
   public boolean checkCompleteMessage( byte[] msg )
   {
      //TODO: optimize by checking just bloc sizes.
      try{
      Rig r = new Rig();
      parseMsg( r, msg );
      }
      catch( TfxError e ) {
         return false;
      }
      return true;
   }
   // ------------------------------------------------------------
   byte[] readBytes( int nb )
   {
      byte[] buf = new byte[ nb ];
      int numRead = 0;
      try {
         numRead = m_in.read( buf );
      }
      catch( IOException e ) {
      }
      if( numRead == nb ) return buf;
      return null;

   }

   // ------------------------------------------------------------
   byte[] readQuadlet()
   {
      return readBytes( 4 );
   }

   // ------------------------------------------------------------
   String readString( int n )
   {
      byte buf[] = readBytes( n );
      StringBuilder sb = new StringBuilder();
      for ( byte b : buf )
         sb.append( (char) b );
      return sb.toString();
   }

   // ------------------------------------------------------------
   public byte[] getVersion()
   {
      return m_version;
   }

   // ------------------------------------------------------------
   public byte[] getHeaderCode()
   {
      return m_headerCode;
   }

   // ------------------------------------------------------------
   public String getRigName()
   {
      return m_rigName;
   }
}
