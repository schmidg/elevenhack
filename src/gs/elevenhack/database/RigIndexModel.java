/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import javax.swing.table.AbstractTableModel;

public class RigIndexModel extends AbstractTableModel
{
   private QueryModel m_qm;
   
   RigIndexModel( QueryModel qm )
   {
      m_qm = qm;
   }
   @Override
   public int getColumnCount()
   {
      return 2;
   }

   @Override
   public int getRowCount()
   {
      return m_qm.getSize();
   }

   @Override
   public Object getValueAt( int rowIndex, int columnIndex )
   {
      if( columnIndex == 0) return m_qm.getElementAt( rowIndex );
      else return m_qm.getPath( rowIndex );
   }

   @Override
   public String getColumnName(int col) {
      if( col == 0 ) return "Name";
      else return "Path";
  }
   
   QueryModel getQueryModel() { return m_qm; }

}
