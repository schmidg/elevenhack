/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.ConfigStrings;
import gs.elevenhack.Term;
import gs.elevenhack.utils.rigloader.CustomListSelectionModel;
import gs.elevenhack.utils.rigloader.RigLoaderGui;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class RigIndexView extends JTable
{
   private CustomListSelectionModel m_selector;
   private ListSelectionListener    m_selectListener;
   private RigDatabase              m_db;
   private RigSelectedCallback m_rigSelCallback;
   // -----------------------------------------------------------------------
   public RigIndexView( RigDatabase db )
   {
      m_db = db;
      setSelectionMode( ListSelectionModel.SINGLE_SELECTION );

      // Drag & Drop setup.
      
      setDragEnabled(true);
      setTransferHandler(new TransferHandler() {
         @Override
         protected Transferable createTransferable(JComponent c) {
            String rigPath = RigLoaderGui.getPrefs().get( ConfigStrings.PREF_RIG_COLLECTION_PATH, "" )
                  + "/" + ((RigIndexModel) getModel() ).getQueryModel().getPath( getSelectedRow() );
           Term.println( "Dragging: " + rigPath );
            return new StringSelection( rigPath );
         }

         @Override
         public int getSourceActions(JComponent c) {
            return COPY;
            //COPY MOVE COPY_OR_MOVE same problem :)
         }
      });
      addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
               if( m_rigSelCallback != null ) {
                  String rigPath = RigLoaderGui.getPrefs().get( ConfigStrings.PREF_RIG_COLLECTION_PATH, "" )
                        + "/" + ((RigIndexModel) getModel() ).getQueryModel().getPath( getSelectedRow() );
                  m_rigSelCallback.onRigDoubleClicked( rigPath );
               }
            }
         }
      });
   }

   // -----------------------------------------------------------------------
   public void setRequest( String req ) throws SQLException
   {
      if( m_db == null || !m_db.isOpen() ) return;
      ResultSet rs =  m_db.query( req );
      Term.println( "Setting requeston list view  with nb fields: " + rs.getMetaData().getColumnCount() );
      assert rs.getMetaData().getColumnCount() == 3 
            : "Bzerk programming error: The view query model should have 3 fields.";
      setModel( new RigIndexModel( new QueryModel( rs ) ));
      rs.close();
   }

   public void setRigSelectionCallback( RigSelectedCallback cb )
   {
      m_rigSelCallback = cb;
   }


}
