/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.Term;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;


public class QueryModel implements ComboBoxModel< String >

{
   private ArrayList<ResultItem> m_values = new ArrayList<ResultItem> ();
   private int m_nbRows = 0;
   private LinkedList< ListDataListener > m_listeners = new LinkedList< ListDataListener >();
   private Object m_selectedObject;
   
   //--------------------------------------------------
   QueryModel( ResultSet rs ) throws SQLException
   {
      m_nbRows = 0;
      
      while(rs.next() ) {
         if( rs.getMetaData().getColumnCount() == 3) {
            m_values.add( new ResultItem( rs.getInt(1), rs.getString( 2 ), rs.getString( 3 ) ) );
         }
         else {
            m_values.add( new ResultItem( rs.getInt(1), rs.getString( 2 ), null ));
         }
         ++m_nbRows;
      }
   }

   //--------------------------------------------------
   void prepend(int id, String name, String path)
   {
      m_values.add( 0, new ResultItem( id, name, path) );
      ++m_nbRows;
   }
   
   //--------------------------------------------------
   @Override
   public String getElementAt( int rowNum )
   {
      return m_values.get( rowNum ).label;
   }
  
   //--------------------------------------------------
   public String getPath( int rowNum )
   {
      return m_values.get(  rowNum ).path;
   }
   
   //--------------------------------------------------
   Integer getRowId(int rowNum)
   {
      return m_values.get( rowNum ).id;
   }
   //--------------------------------------------------
   @Override
   public void addListDataListener( ListDataListener l )
   {
      m_listeners.add( l );
   }
   
   //--------------------------------------------------
   @Override
   public int getSize()
   {
      return m_nbRows;
   }
   
   //--------------------------------------------------
   @Override
   public void removeListDataListener( ListDataListener l )
   {
      m_listeners.remove( l );      
   }
   
   //--------------------------------------------------
   @Override
   public Object getSelectedItem()
   {
      return m_selectedObject;
   }
   
   //--------------------------------------------------
   @Override
   public void setSelectedItem( Object anItem )
   {
      m_selectedObject = anItem;
   }
   
   //--------------------------------------------------
   public void printToTerm()
   {
      for( ResultItem i : m_values) {
         Term.println( "Model Item: " + i.id + " : " + i.label );
      }
   }

}
