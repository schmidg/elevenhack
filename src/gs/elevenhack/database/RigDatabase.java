/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.ERError;
import gs.elevenhack.SelectOption;
import gs.elevenhack.Term;
import gs.elevenhack.ER.Effect;
import gs.elevenhack.ER.EffectAmpCab;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.RigRenderer;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class RigDatabase implements RigRenderer
{



   private Connection          m_conn;
   private final static String DBNAME            = "rigdatabase";
   private int                 m_currEffectType  = 0;
   private int                 m_currEffectClass = 0;
   private boolean             m_currBypass;
   private int                 m_dbRigId         = -1;
   private PreparedStatement   m_stLastId;
   
   //@formatter:off
   private HashMap<Integer, Integer> m_ampDoubles = new HashMap< Integer, Integer>(){{
      put( 0,-2147483648 );
      put( 1, -1861152495);
      put( 2, -1574821342);
      put( 3, -1288490189);
      put( 4, -1002159036);
      put( 5, -715827883);
      put( 6, -429496730);
      put( 7, -143165577);
      put( 8, 143165576);
      put( 9, 429496729);
      put( 10, 715827882);
      put( 11, 1002159035);
      put( 12, 1288490188);
      put( 13, 1574821341);
      put( 14, 1861152494);
      put( 15, 2147483647);
                                                     
   }};
   //@formatter:on

   public RigDatabase()
   {
   }

   // -------------------------------------------------------------
   public void open( String path ) throws SQLException, ERError, IOException
   {
      File dir = new File( path );
      if( !dir.exists() ) {
         throw new ERError( "cannot open directory " + path
                            + " to store rig database." );
      }
      if( !dir.isDirectory() ) {
         throw new ERError(
                            "path "
                                  + path
                                  + " is not a valid directory to store rig database." );
      }
      try {
         Class.forName( "org.hsqldb.jdbcDriver" );
      }
      catch( Exception e ) {
         throw new ERError( "cannot load HSQL driver:" + e.getMessage() );
      }
      m_conn = DriverManager.getConnection( "jdbc:hsqldb:file:" + path + "/"
                                            + DBNAME, "sa", "" );
      m_conn.setAutoCommit( true );
      mCheckCreateDB();

      m_stLastId = m_conn.prepareStatement( "call identity()" );
   }

   // -------------------------------------------------------------
   void mCheckCreateDB() throws IOException, SQLException
   {
      try {
         query( "select * from rigs" );
      }
      catch( SQLException e ) {
         Term.println( "Cannot select from rigs: " + e.getMessage() );
         m_conn.setAutoCommit( false );
         // @formatter:off
         update( "CREATE TABLE rigs ("
                 + "id identity NOT NULL PRIMARY KEY,"
                 + "hashkey integer not null,"
                 + "name CHARACTER(16) not null,"
                 + "exp_version integer not null,"
                 + "path VARCHAR(1024) not null,"
                 + " );" );
         
         update("CREATE TABLE effect_class (" +
         		"id identity NOT NULL PRIMARY KEY," +
                "label VARCHAR(32) NOT NULL," +
                "used BOOLEAN NOT NULL" + // Internal, some values are not used.
         		");");
                  
         update("CREATE TABLE effect_type (" +
         		"id identity NOT NULL PRIMARY KEY," +
                "main_sibling_id INTEGER NOT NULL," + 
         		"label VARCHAR(32) NOT NULL" +
         		")");
         
         update("CREATE TABLE amp_type (" +
                "id INTEGER UNIQUE NOT NULL," +
                "label VARCHAR(32) NOT NULL" +
                ")");
        
         update("CREATE TABLE effect (" +
         		"id identity NOT NULL PRIMARY KEY," +
                "bypass BOOLEAN NOT NULL," +
         		"rig_id INTEGER NOT NULL," +
         		"slot INTEGER DEFAULT -1," + // Will probably be used in the future...
         		"class_id INTEGER NOT NULL," +
         		"type_id INTEGER NOT NULL," +
                "FOREIGN KEY( rig_id ) REFERENCES Rigs( id )," +
                "FOREIGN KEY( type_id ) REFERENCES Effect_type( id )," +
                "FOREIGN KEY( class_id ) REFERENCES Effect_class( id )" +
         		");");
         
         update("CREATE TABLE amp (" +
               "id identity NOT NULL PRIMARY KEY," +
               "bypass BOOLEAN NOT NULL," +
               "rig_id INTEGER NOT NULL," +
               "amp_id INTEGER NOT NULL," +
               "FOREIGN KEY( rig_id ) REFERENCES Rigs( id )," +
               "FOREIGN KEY( amp_id ) REFERENCES Amp_Type( id )" +
         		");");
         
         update("CREATE TABLE GUI_EFFECT_LOOKUP (" +
         		"id identity NOT NULL PRIMARY KEY," +
         		"class_id INTEGER NOT NULL," +
         		"main_sibling_id INTEGER NOT NULL);");
         
         update("CREATE TRIGGER trig_gui_effect_lookup AFTER INSERT ON EFFECT " +
         		"REFERENCING NEW ROW AS newrow " +
         		"FOR EACH ROW WHEN (NOT EXISTS ( select gl.main_sibling_id, gl.class_id from gui_effect_lookup gl, effect_type et " +
         		"                                where gl.class_id = newrow.class_id and " +
         		"                                gl.main_sibling_id = et.main_sibling_id and" +
         		"                                et.id = newrow.type_id ) ) " +
         		"   BEGIN ATOMIC" +
         		"     INSERT INTO gui_effect_lookup (class_id, main_sibling_id) " +
         		"            select newrow.class_id, main_sibling_id from effect_type WHERE " +
                "                                id = newrow.type_id;" +
         		"   END;");
         
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 0, 'Amp + Cab', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 1, 'FX Loop', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 2, 'Volume', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 3, 'Wah', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 4, 'Mod', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 5, 'Reverb', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 6, 'Delay', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 7, 'Disto', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 8, 'FX1', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 9, 'FX2', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 10, 'Input', TRUE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 11, '0B', FALSE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 12, '0C', FALSE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 13, '0D', FALSE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 14, '0E', FALSE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 15, '0F', FALSE );");
         update("INSERT INTO Effect_Class ( id, label, used) VALUES ( 16, 'Rig Parameter', TRUE );");
         // @formatter:on

         // Fill The effect Table automatically from effect factory.
         for ( int i = 0; i < 100; ++i ) {

            Effect ef = new Effect( 0, i );
            if( ef.isKnown() ) {

               if( i == 12 ) continue; // Amp/Cab handled differently.

               String req = "INSERT INTO Effect_Type"
                            + "(id, main_sibling_id, label) VALUES"
                            + "(?, ?, ?);";
               PreparedStatement st = m_conn.prepareStatement( req );
               st.setInt( 1, i );
               if( ef.getSiblings().isEmpty() ) st.setInt( 2, i );
               else st.setInt( 2, ef.getSiblings().get( 0 ) );
               st.setString( 3, ef.getName() );
               st.executeUpdate();
            }
         }

         // Fill the effect table with Amp names.
         for ( int i : EffectAmpCab.getAmpMap().keySet() ) {

            if( !m_ampDoubles.containsKey( i ) ) {
               String req = "INSERT INTO Amp_Type" + "(id, label) VALUES"
                     + "(?,?);";
               PreparedStatement st = m_conn.prepareStatement( req );
               st.setInt( 1, i );
               st.setString( 2, EffectAmpCab.getAmpMap().get( i ) );
               st.executeUpdate();
            }
         }

         m_conn.commit();
         m_conn.setAutoCommit( true );

         Term.println( "Created Database." );
      }
   }

   // -------------------------------------------------------------
   public void resetDB() throws SQLException, IOException
   {
      m_conn.setAutoCommit( false );
      update( "drop table amp;" );
      update( "drop table effect;" );
      update( "drop table rigs;" );
      update( "drop table effect_class;" );
      update( "drop table effect_type;" );
      update( "drop table amp_type;" );
      update( "drop table gui_effect_lookup;" );
      m_conn.commit();
      m_conn.setAutoCommit( true );
      mCheckCreateDB();
   }
   
   // -------------------------------------------------------------
   public void shutdown() throws SQLException
   {
      if( ! isOpen() ) return;
      
      Statement st = m_conn.createStatement();

      // db writes out to files and performs clean shuts down
      // otherwise there will be an unclean shutdown
      // when program ends
      st.execute( "SHUTDOWN" );
      m_conn.close(); // if there are no other open connection
      Term.println( "Performed Database Shutdown" );
   }

   // -------------------------------------------------------------
   public synchronized ResultSet query( String expression ) throws SQLException
   {
      Statement st = m_conn.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                             ResultSet.CLOSE_CURSORS_AT_COMMIT );
      return st.executeQuery( expression ); // run the query
   }

   // --------------------------------------------------------------
   int mSignatureToInt( byte[] sig )
   {
      return ( (int) sig[0] << 24 ) + ( (int) sig[1] << 16 )
             + ( (int) sig[2] << 8 ) + (int) sig[3];
   }

   // --------------------------------------------------------------
   public void insertRig( String relPath, byte[] msg ) throws SQLException, TfxError
   {
      Rig rig = new Rig();
      TfxParser p = new TfxParser();
      p.parseMsg( rig, msg );
      Term.println( "Exp: " + rig.getExpansionPackVersion() );
      String req = "INSERT INTO Rigs"
                   + "(hashkey, name, exp_version, path) VALUES" + "(?,?,?,?)";
      PreparedStatement st = m_conn.prepareStatement( req );
      st.setInt( 1, mSignatureToInt( rig.getSignature() ) );
      st.setString( 2, rig.getName() );
      st.setInt( 3, rig.getExpansionPackVersion() );
      st.setString( 4, relPath );
      st.executeUpdate();
      Term.println( "Retrieving rig identity." );
      m_dbRigId = getIdentity();
      Term.println( "Rendering rig " + rig.getName() );
      rig.render( this );
   }

   // --------------------------------------------------------------
   int getIdentity() throws SQLException
   {

      ResultSet rs = m_stLastId.executeQuery();

      rs.next();
      int id = rs.getInt( 1 );
      rs.close();
      return id;
   }

   // -------------------------------------------------------------
   // use for SQL commands CREATE, DROP, INSERT and UPDATE
   public synchronized void update( String expression ) throws SQLException
   {
      Statement st = m_conn.createStatement(); // statements
      int i = st.executeUpdate( expression ); // run the query

      if( i == -1 ) {
         System.out.println( "db error : " + expression );
      }
      Term.println( "Executed statement:" + expression );
      st.close();
   }

   // ----------------------------------------------------------------
   // Rig Renderer Interface.
   // ----------------------------------------------------------------
   @Override
   public void rigName( String n )
   {

   }

   @Override
   public void rigVersion( int v )
   {

   }

   @Override
   public void newEffectName( int effectType, int effectClass, String name )
   {
      m_currEffectType = effectType;
      m_currEffectClass = effectClass;
      // Wait bypass param to store effect.
   }

   @Override
   public void knobParam( String name, int Value )
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void selectorParam( String name, SelectOption[] options, int val )
   {
      try {
         if( name.equals( "Amp type" ) ) {
            Term.println( "Storing Amp" );

            // Convert old Amp id to new ones for database dedupe.
            if( m_ampDoubles.containsKey( val ) ) {
               val = m_ampDoubles.get( val );

            }

            String req = "INSERT INTO Amp"
                         + "(bypass, rig_id, amp_id) VALUES ( ?, ?, ? )";
            PreparedStatement st = m_conn.prepareStatement( req );
            st.setBoolean( 1, m_currBypass );
            st.setInt( 2, m_dbRigId );
            st.setInt( 3, val );
            st.executeUpdate();
         }
      }
      catch( Exception e ) {
         Term.println( "error while storing effect in database: "
                       + e.getMessage() );
      }
   }

   @Override
   public void switchParam( String name, int val )
   {
      if( m_currEffectClass >= 9 ) return;
      try {
         if( name.equals( "bypa" ) ) {
            m_currBypass = !( val == 0 || val == -2147483648 );

            if( m_currEffectClass != Effect.EF_TYPE_AMPCAB ) { // AmpCab

               String req = "INSERT INTO Effect"
                            + "(bypass, rig_id, class_id, type_id) VALUES"
                            + "(?, ?, ?, ?)";
               PreparedStatement st = m_conn.prepareStatement( req );
               st.setBoolean( 1, m_currBypass );
               st.setInt( 2, m_dbRigId );
               st.setInt( 3, m_currEffectClass );
               st.setInt( 4, m_currEffectType );
               st.executeUpdate();
            }
         }
      }
      catch( Exception e ) {
         Term.println( "error while storing effect in database: "
                       + e.getMessage() );
      }
   }

   @Override
   public void commitEffect()
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void renderUnset( String m_deco )
   {
      // TODO Auto-generated method stub

   }

   public boolean isOpen()
   {
      if( m_conn == null ) return false;
      try {
         return !m_conn.isClosed();
      }
      catch( SQLException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         return false;
      }
   }
}
