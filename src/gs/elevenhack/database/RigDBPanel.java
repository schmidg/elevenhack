/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.utils.rigloader.ErrorBox;
import gs.elevenhack.utils.rigloader.ThemeLoader;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import java.awt.event.ActionListener;

public class RigDBPanel extends JPanel implements RigFilterCallback
{
   private RigFiltersPanel m_filterPanel;
   private RigDatabase     m_db;
   private RigIndexView    m_rigListView;
   private JLabel          m_nbRigsLabel;

   
   public RigDBPanel( RigDatabase db )
   {
      m_db = db;
      setLayout( new MigLayout( "", "grow" ) );

      m_filterPanel = new RigFiltersPanel( m_db );
      add( m_filterPanel , "cell 0 0");
      m_filterPanel.setVisible( true );

      m_nbRigsLabel = new JLabel( "Found 0 Rigs" );
      m_nbRigsLabel.setBackground( Color.ORANGE );
      add(m_nbRigsLabel, "cell 0 1, center");
      
      JScrollPane scrollPane = new JScrollPane();
      m_rigListView = new RigIndexView( m_db );
      scrollPane.setViewportView( m_rigListView );
      add( scrollPane, "cell 0 2, grow" );

      handleNewFilter( "select id, name, path  from rigs order by name" );

      m_filterPanel.setFilterCallback( this );
      setVisible( true );
   }

   public void setRigSelectionCallback( RigSelectedCallback cb )
   {
      m_rigListView.setRigSelectionCallback (cb);
   }
   
   // ----------------------------------------------------------
   public void updateGui()
   {
      m_filterPanel.updateGui();
   }
// ----------------------------------------------------------
   public void setSearchButtonCallback( ActionListener al )
   {
      m_filterPanel.setSearchButtonCallback( al );
   }

   // ----------------------------------------------------------
   @Override
   public void handleNewFilter( String query )
   {
      try {
         m_rigListView.setRequest( query );
         m_nbRigsLabel.setText( "Found " + m_rigListView.getModel().getRowCount() +" Rigs" );
      }
      catch( SQLException e ) {
         Term.println( "Query error: " + query );
         e.printStackTrace();
      }
   }

   // ----------------------------------------------------------
   // Stand Alone Database Viewer
   // ----------------------------------------------------------
   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      ThemeLoader.loadTheme();
      
      Term.activated = false;
      String dbPath = new String();
      String sourcePath = new String();

      int i = 0;
      String arg;

      while( i < args.length && args[i].startsWith( "-" ) ) {
         arg = args[i++];
         Term.println( "param: [" + arg + "]" );

         // use this type of check for "wordy" arguments
         if( arg.equals( "-verbose" ) ) {
            Term.println( "verbose mode on" );
            Term.activated = true;
         }

         else if( arg.equals( "-db" ) ) {
            if( i < args.length ) {
               dbPath = args[i++];
               Term.println( "using database path " + dbPath );
            }
            else System.err.println( "-db requires a database path" );
         }

         // use this type of check for a series of flag arguments
         else {
            for ( int j = 1; j < arg.length(); j++ ) {
               char flag = arg.charAt( j );
               switch ( flag ) {
                  case 'v':
                     Term.activated = true;
                     break;

                  default:
                     System.err.println( "ParseCmdLine: illegal option " + flag );
                     break;
               }
            }
         }
      }
      if( dbPath.isEmpty() ) {
         System.err.println( "error, no database path specified." );
         mPrintUsage();
         System.exit( -1 );
      }
      final RigDatabase db = new RigDatabase();

      try {
         db.open( dbPath );

         Runtime.getRuntime().addShutdownHook( new Thread( new Runnable() {
            public void run()
            {

               try {
                  db.shutdown();
                  Term.println( "Database shutdown properly." );

               }
               catch( SQLException e ) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
            }
         }, "Shutdown-thread" ) );

         RigDBPanel rp = new RigDBPanel( db );
         Term.println( "Disaplay filter panel" );
         rp.setVisible( true );
         JFrame frame = new JFrame( "ListDemo" );
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

         frame.setContentPane( rp );
         // Display the window.
         frame.pack();
         frame.setVisible( true );
      }
      catch( SQLException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( ERError e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( IOException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   // ---------------------------------------------------------
   static void mPrintUsage()
   {
      System.out.println( "use:\n RigDBPanel [-v] -db path_to_database" );
   }

}
