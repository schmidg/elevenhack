/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;
import gs.elevenhack.utils.rigloader.ErrorBox;
import gs.elevenhack.utils.rigloader.IndexProgressReport;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.util.LinkedList;

public class RigIndexer extends SimpleFileVisitor< Path >
{
   private RigDatabase  m_db;
   private Path m_rootPath;
   private TfxParser    m_parser;
   private static LinkedList< String > m_failedFiles;
   private IndexProgressReport m_ipr;
   private int m_countFilesOk = 0;
   
   // -----------------------------------------------------
   public RigIndexer( RigDatabase db, IndexProgressReport ipr )
   {
      m_db = db;
      m_ipr = ipr;
      m_parser = new TfxParser();
      m_failedFiles = new LinkedList< String >();
   }

   // -----------------------------------------------------
   public void indexDir( Path dir ) throws IOException
   {
      m_rootPath = dir; 
      m_failedFiles.clear();
      report( "Rig files path: " + dir.toString() + "\n" );
      Files.walkFileTree( dir, this );
      if( !m_failedFiles.isEmpty() ) {
         StringBuilder sb = new StringBuilder();
         for ( String s : m_failedFiles ) {
            sb.append( s + "\n" );
         }
         report( "========================\n" +
               " PARSING FAILURES:\n"
               + sb.toString() +"\n");
      }
      else {
         report( "No errors (cool!).\n");
      }
      report("Indexed successfully " + m_countFilesOk + "tfx files.\n");
   }
   
   // -----------------------------------------------------
   void indexFile( Path path ) throws TfxError, IOException, SQLException
   {
      File f = path.toFile();
      byte[] msg = m_parser.extractBody( f );
      String relPath = m_rootPath.relativize( path ).toString();
      report( "Indexing " + relPath + "\n" );
      m_db.insertRig( relPath, msg );
   }

   void report( String s )
   {
      if( m_ipr != null  ) m_ipr.print( s );
   }
   // -----------------------------------------------------
   // DIRECTORY WALKER
   // -----------------------------------------------------
   @Override
   public FileVisitResult visitFile( Path file, BasicFileAttributes attr )
   {
      if( attr.isRegularFile() ) {

         try {
            String fname = file.toString();
            if( fname.endsWith( ".tfx" ) || fname.endsWith( ".TFX" ) ) {
               indexFile( file );
               ++ m_countFilesOk;
            }
         }
         catch( Exception e ) {
            Term.println( e.getMessage());
            m_failedFiles.add( file.toString() + ": " + e.getMessage() );
         }

      }
      return FileVisitResult.CONTINUE;
   }

   // ----------------------------------------------------------
   // Stand Alone indexer
   // ----------------------------------------------------------
   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      Term.activated = false;
      String dbPath = new String();
      String sourcePath = new String();
      
      int i = 0;
      String arg;
      
      while (i < args.length && args[i].startsWith( "-" )) {
         arg = args[i++];
         Term.println( "param: [" + arg + "]" );
         
         // use this type of check for "wordy" arguments
         if (arg.equals( "-verbose" )) {
             Term.println( "verbose mode on" );
             Term.activated= true;
         }

         else if (arg.equals( "-db" )) {
             if (i < args.length) {
                dbPath = args[i++];
                Term.println( "using database path " + dbPath );
             }
             else System.err.println( "-db requires a database path" );
         }

         // use this type of check for a series of flag arguments
         else {
             for (int j = 1; j < arg.length(); j++) {
                 char flag = arg.charAt( j );
                 switch (flag) {
                 case 'v':
                    Term.activated=true;
                     break;

                 default:
                     System.err.println( "ParseCmdLine: illegal option "
                             + flag );
                     break;
                 }
             }
         }
         if( args.length == i+1 ) {
             sourcePath = args[i];
         }
     }
      if( sourcePath.isEmpty() ) {
            System.err.println("error, no source directory specified.");
            mPrintUsage();
            System.exit( -1 );
      }
      if( dbPath.isEmpty() ) {
         System.err.println("error, no database path specified.");
         mPrintUsage();
         System.exit( -1 );
      }
      RigDatabase db = null;
      try {
         db = new RigDatabase();
         db.open( dbPath );
         RigIndexer ri = new RigIndexer( db, null );
         Path p = Paths.get( sourcePath);
         ri.indexDir( p );
         if( !m_failedFiles.isEmpty() ) {
            StringBuilder sb = new StringBuilder();
            for( String s : m_failedFiles ) sb.append( s + "\n" );
            Term.println( "Rig parsing failed on files: " + sb.toString() );
         }
      }
      catch( SQLException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( ERError e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( IOException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      finally{
         if( db != null ) try {
            db.shutdown();
         }
         catch( SQLException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         Term.println( "Database shutdown properly." );
      }
      Term.println( "Done." );
   }
   
   //---------------------------------------------------------
   static void mPrintUsage()
   {
      System.out.println("use:\n RigIndexer [-v] -db=path_to_database path_to_tfx_directory");
   }
}
