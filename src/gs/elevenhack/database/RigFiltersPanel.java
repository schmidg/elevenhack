/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.database;

import gs.elevenhack.Term;
import gs.elevenhack.ER.Effect;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class RigFiltersPanel extends JPanel implements ActionListener
{
   private JTextField        m_nameField;
   private RigDatabase       m_db;
   private RigFilterCallback m_filterCallback;

   private class Combo
   {
      QueryModel          qm;
      String query;
      JComboBox< String > cbox;
      ButtonGroup bgroup;
      JComboBox<String> onOffBox;
   }

   private Combo m_ampCB;
   private Combo m_distoCB;
   private Combo m_wahCB;
   private Combo m_delayCB;
   private Combo m_modFxCB;

   private JButton m_btnSearch;
   
   public RigFiltersPanel( RigDatabase db )
   {
      m_db = db;
      setLayout( new MigLayout( "", "[][grow]", "[][][][][][]" ) );

      JLabel lblName = new JLabel( "Name" );
      add( lblName, "cell 0 0" );

      m_nameField = new JTextField();
      add( m_nameField, "cell 1 0,growx" );
      m_nameField.setColumns( 10 );
      m_nameField.getDocument().addDocumentListener(new DocumentListener() {
         public void changedUpdate(DocumentEvent e) {
             actionPerformed( null );
          }
         @Override
         public void insertUpdate( DocumentEvent arg0 ) {actionPerformed( null );}
         @Override
         public void removeUpdate( DocumentEvent arg0 ) {actionPerformed( null );}
      });
      int row = 1;
      // Special request as there are multiple possible IDs for a given amp, we must select at most one
      // to avoid dupes in the combo.
      m_ampCB = pAddFilterCBox( "Amp", row,
                                "select id, label, label from AMP_TYPE order by label;" );
      m_ampCB.onOffBox.setVisible( false ); 
      ++row;

      m_distoCB = pAddFilterCBox( "Disto", row,
                                  "select distinct et.id, et.label from effect_type et, gui_effect_lookup gl WHERE "
                                        + "et.id = gl.main_sibling_id "
                                        + "and gl.class_id = "
                                        + Effect.EF_TYPE_DISTO
                                        + " order by label;" );
      ++row;

      m_wahCB = pAddFilterCBox( "Wah", row,
                                "select distinct et.id, et.label from effect_type et, gui_effect_lookup gl WHERE "
                                      + "et.id = gl.main_sibling_id "
                                      + "and gl.class_id = "
                                      + Effect.EF_TYPE_WAH + " order by label;" );
      ++row;

      m_delayCB = pAddFilterCBox( "Delay", row,
                                  "select distinct et.id, et.label from effect_type et, gui_effect_lookup gl WHERE "
                                        + "et.id = gl.main_sibling_id "
                                        + "and gl.class_id = "
                                        + Effect.EF_TYPE_DELAY
                                        + " order by label;" );
      ++row;

      m_modFxCB = pAddFilterCBox( "Mod/FX", row,
                                  "select distinct et.id, et.label from effect_type et, gui_effect_lookup gl WHERE "
                                        + "et.id = gl.main_sibling_id "
                                        + "and ( gl.class_id = "
                                        + Effect.EF_TYPE_FX1 + " or "
                                        + "gl.class_id = " + Effect.EF_TYPE_FX2
                                        + " or " + "gl.class_id = "
                                        + Effect.EF_TYPE_MOD
                                        + ") order by label;" );
      ++row;

      JButton btnClear = new JButton( "Clear" );
      add( btnClear, "flowx,cell 1 " + row );
      btnClear.addActionListener( new ActionListener() {
         @Override
         public void actionPerformed( ActionEvent arg0 )
         {
            m_nameField.setText("");
            m_ampCB.cbox.setSelectedIndex( 0 );
            m_ampCB.onOffBox.setSelectedIndex( 0 );
            m_distoCB.cbox.setSelectedIndex( 0 );
            m_distoCB.onOffBox.setSelectedIndex( 0 );
            m_wahCB.cbox.setSelectedIndex( 0 );
            m_wahCB.onOffBox.setSelectedIndex( 0 );
            m_delayCB.cbox.setSelectedIndex( 0 );
            m_delayCB.onOffBox.setSelectedIndex( 0 );
            m_modFxCB.cbox.setSelectedIndex( 0 );
            m_modFxCB.onOffBox.setSelectedIndex( 0 );
         }
      });

      m_btnSearch = new JButton( "Re-Index" );
      add( m_btnSearch, "cell 1 " + row + ",alignx right" );
   }

   void setFilterCallback( RigFilterCallback cb )
   {
      m_filterCallback = cb;
   }
   
   // ------------------------------------------------------------------
   void setSearchButtonCallback( ActionListener al )
   {
      m_btnSearch.addActionListener( al );
   }
   
   // ------------------------------------------------------------------
   Combo pAddFilterCBox( String label, int guiRow, String query )
   {
      JLabel lblAmp = new JLabel( label );
      add( lblAmp, "cell 0 " + guiRow + ",alignx trailing" );

      Combo cb = new Combo();
      cb.cbox = new JComboBox< String >();
      cb.query = query;
      add( cb.cbox, "cell 1 " + guiRow + ",growx" );
      cb.onOffBox = new JComboBox<String>();
      cb.onOffBox.addItem("On&Off");
      cb.onOffBox.addItem("On");
      cb.onOffBox.addItem("Off");
      add( cb.onOffBox, "cell 1 " + guiRow  );
      
      mUpdateCombo( cb );
      return cb;
   }

   // ------------------------------------------------------------------
   void mUpdateCombo( Combo cb )
   {
      if( m_db == null || !m_db.isOpen() ) return;
      
      try {
         cb.cbox.removeAll();
         cb.qm = new QueryModel( m_db.query( cb.query ) );
         cb.qm.prepend( 0, "All", "" );
         cb.qm.printToTerm();
         cb.cbox.setModel( cb.qm );
         cb.cbox.setSelectedIndex( 0 );
         cb.cbox.removeActionListener( this );
         cb.cbox.addActionListener( this );
         cb.onOffBox.setSelectedIndex(0);
         cb.onOffBox.removeActionListener( this );
         cb.onOffBox.addActionListener( this );
      }
      catch( SQLException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
   
   // ------------------------------------------------------------------
   void updateGui()
   {
      mUpdateCombo(m_ampCB);
      Term.println( "updates combo Amp" );
      mUpdateCombo(m_distoCB);
      Term.println( "updates combo Disto" );
      mUpdateCombo(m_wahCB);
      Term.println( "updates combo Wah" );
      mUpdateCombo(m_delayCB);
      Term.println( "updates combo Delay" );
      mUpdateCombo(m_modFxCB);
      Term.println( "updates combo Mod" );
      actionPerformed( null );
   }
   
   // ------------------------------------------------------------------
   @Override
   public void actionPerformed( ActionEvent arg0 )
   {
      StringBuilder sbWhere = new StringBuilder();
      StringBuilder sbFrom = new StringBuilder();

      // Full Text Search.
      String filter = m_nameField.getText().trim().toLowerCase();
      if( !filter.isEmpty() ) {
         String[] filters = filter.split( " " );
         for( String s : filters ) {
            Term.println( "s=" +s );
            s = "'%" + s.replaceAll("'", "''") + "%'";
            Term.println( "s=" +s );
            sbWhere.append(" AND lower(r.name) LIKE " +  s );
         }
      }

      int selIdx = m_ampCB.cbox.getSelectedIndex();
      if( selIdx != -1 && selIdx != 0 ) {
         sbFrom.append( ", amp a " );
         sbWhere.append( " AND a.rig_id = r.id AND a.amp_id = "
                         + m_ampCB.qm.getRowId( selIdx ) );
         int bypass = m_ampCB.onOffBox.getSelectedIndex();
         if( bypass > 0 ) {
            sbWhere.append( " and a.bypass = " +  (bypass == 1? "false " : "true "));
         }
      }

      selIdx = m_distoCB.cbox.getSelectedIndex();
      if( selIdx != -1 && selIdx != 0 ) {
         sbFrom.append( ", effect ef2, effect_type et2 " );
         sbWhere.append( " AND ef2.rig_id = r.id AND ef2.type_id = et2.id and et2.main_sibling_id = "
                         + m_distoCB.qm.getRowId( selIdx ) );
         int bypass = m_distoCB.onOffBox.getSelectedIndex();
         if( bypass > 0 ) {
            sbWhere.append( " and ef2.bypass = " +  (bypass == 1? "false " : "true "));
         }
      }

      selIdx = m_wahCB.cbox.getSelectedIndex();
      if( selIdx != -1 && selIdx != 0 ) {
         sbFrom.append( ", effect ef3, effect_type et3 " );
         sbWhere.append( " and ef3.rig_id = r.id and ef3.type_id = et3.id and et3.main_sibling_id = "
                         + m_wahCB.qm.getRowId( selIdx ) );
         int bypass = m_wahCB.onOffBox.getSelectedIndex();
         if( bypass > 0 ) {
            sbWhere.append( " and ef3.bypass = " +  (bypass == 1? "false " : "true "));
         }

      }

      selIdx = m_delayCB.cbox.getSelectedIndex();
      if( selIdx != -1 && selIdx != 0 ) {
         sbFrom.append( ", effect ef4, effect_type et4 " );
         sbWhere.append( " and ef4.rig_id = r.id and ef4.type_id = et4.id and et4.main_sibling_id = "
                         + m_delayCB.qm.getRowId( selIdx ) );
         int bypass = m_delayCB.onOffBox.getSelectedIndex();
         if( bypass > 0 ) {
            sbWhere.append( " and ef4.bypass = " +  (bypass == 1? "false " : "true "));
         }

      }

      selIdx = m_modFxCB.cbox.getSelectedIndex();
      if( selIdx != -1 && selIdx != 0 ) {
         sbFrom.append( ", effect ef4, effect_type et4 " );
         sbWhere.append( " and ef4.rig_id = r.id and ef4.type_id = et4.id and et4.main_sibling_id = "
                         + m_modFxCB.qm.getRowId( selIdx ) );
         int bypass = m_modFxCB.onOffBox.getSelectedIndex();
         if( bypass > 0 ) {
            sbWhere.append( " and ef4.bypass = " +  (bypass == 1? "false " : "true "));
         }

      }

      
      if( m_filterCallback != null ) {
         String req = "select id, name, path from rigs r "
               + sbFrom.toString() + " WHERE r.id >= 0 "
               + sbWhere.toString() + " order by name;" ;
         Term.println( req );
         m_filterCallback.handleNewFilter( req );
      }
   }
}
