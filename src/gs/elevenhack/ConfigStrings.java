/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack;

public class ConfigStrings
{
   public static final String PREF_INPORT  = "inport";
   public static final String PREF_OUTPORT = "outport";
   public static final String PREF_DISPLAY_COLLECTION = "pref_display_collection";

   public static final String PREF_RIG_COLLECTION_PATH = "Rig_Collection_Path";
   public static final String PREF_RIG_INDEX_PATH = "Rig_Index_Path";
}
