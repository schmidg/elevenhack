package gs.elevenhack.utils.rigloader;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenListener;
import gs.elevenhack.ER.ElevenRack;

import javax.swing.JToggleButton;

@SuppressWarnings( "serial" )
public class ButtonTuner extends JToggleButton implements ItemListener
{
   private ElevenRack m_rack;
   private boolean    m_callbackActivated;

   // ----------------------------------------------------------
   ButtonTuner( ElevenRack rack )
   {
      super( "Tuner", false );
      m_rack = rack;
      m_callbackActivated = true;
      addItemListener( this );
   }

   // ----------------------------------------------------------
   void silentSet( boolean v )
   {
      m_callbackActivated = false;
      setSelected( v );
      m_callbackActivated = true;
   }

   // ----------------------------------------------------------
   @Override
   public void itemStateChanged( ItemEvent arg0 )
   {
      try {
         Term.println( "Item state" );
         if( !m_callbackActivated || !m_rack.isConnected()) return;

         Term.println( "Tuner " + isSelected() );
         m_rack.activateTuner( isSelected() );
      }
      catch( ERError e ) {
         ErrorBox.error( "Communication Error", "Could not switch the Tuner." );
         Term.print( e.getMessage() );
      }
   }
}
