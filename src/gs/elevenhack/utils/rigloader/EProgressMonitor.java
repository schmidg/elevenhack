package gs.elevenhack.utils.rigloader;


import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import net.miginfocom.swing.MigLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;


public class EProgressMonitor extends JDialog
{
   private JLabel m_lblText;
   private JProgressBar m_progress;

   public EProgressMonitor( Component parent) 
   {
      getContentPane().setBackground(Color.BLACK);

      setSize(new Dimension(615, 340));
      getContentPane().setSize(new Dimension(804, 360));
      setAlwaysOnTop(true);
      setVisible(true);

      getContentPane().setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][][]"));
      
      JLabel lblImage = new JLabel("");
      lblImage.setBackground(Color.BLACK);
      lblImage.setSize(new Dimension(800, 335));
      getContentPane().add(lblImage, "cell 0 0,alignx center");
      
      m_lblText = new JLabel("");
      m_lblText.setForeground(Color.ORANGE);
      getContentPane().add(m_lblText, "cell 0 1,alignx center");
      
      m_progress = new JProgressBar();
      getContentPane().add(m_progress, "cell 0 2,alignx center");
      
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream input = classLoader.getResourceAsStream("11HV2.png");
      
      try {
         Image logo = ImageIO.read(input);
         ImageIcon icon = new ImageIcon( logo );
         lblImage.setIcon( icon );
         }
      catch( IOException e ) {
         e.printStackTrace();
      }
      
      setVisible(true);
      setLocationRelativeTo( parent );
   }

   public void setMinimum( int i )
   {
      m_progress.setMinimum( i );
   }

   public void setMaximum( int i )
   {
      m_progress.setMaximum( i );  
   }

   public void setProgress( int i )
   {
      m_progress.setValue( i );
   }

   public void setNote( String text )
   {
     m_lblText.setText( text );
   }
}
