package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Dimension;

@SuppressWarnings( "serial" )
public class DialogRigRename extends JDialog
{
   private ElevenRack m_rack;
   private JTextField m_textField;

   public DialogRigRename( ElevenRack rack )
   {
      setMinimumSize( new Dimension( 400, 100 ) );
      m_rack = rack;
      setTitle( "Rig Rename" );
      GridBagLayout gridBagLayout = new GridBagLayout();
      gridBagLayout.columnWidths = new int[ ] { 100, 100, 100 };
      gridBagLayout.rowHeights = new int[ ] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      gridBagLayout.columnWeights = new double[ ] { 1.0, 0.0, 0.0 };
      gridBagLayout.rowWeights = new double[ ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, Double.MIN_VALUE };
      getContentPane().setLayout( gridBagLayout );

      JLabel lblRenameCurrentRig = new JLabel( "Rename Current Rig" );
      GridBagConstraints gbc_lblRenameCurrentRig = new GridBagConstraints();
      gbc_lblRenameCurrentRig.gridwidth = 3;
      gbc_lblRenameCurrentRig.insets = new Insets( 0, 0, 5, 0 );
      gbc_lblRenameCurrentRig.gridx = 0;
      gbc_lblRenameCurrentRig.gridy = 1;
      getContentPane().add( lblRenameCurrentRig, gbc_lblRenameCurrentRig );

      m_textField = new JTextField();
      GridBagConstraints gbc_textField = new GridBagConstraints();
      gbc_textField.insets = new Insets( 0, 0, 5, 0 );
      gbc_textField.gridwidth = 3;
      gbc_textField.fill = GridBagConstraints.HORIZONTAL;
      gbc_textField.gridx = 0;
      gbc_textField.gridy = 3;
      getContentPane().add( m_textField, gbc_textField );
      m_textField.setColumns( 10 );

      JButton btnOk = new JButton();
      GridBagConstraints gbc_btnOk = new GridBagConstraints();
      gbc_btnOk.insets = new Insets( 0, 0, 5, 5 );
      gbc_btnOk.gridx = 1;
      gbc_btnOk.gridy = 5;
      Term.println( "Textfield: " + m_textField.getText() );
      btnOk.setAction( new ActionRigRename( this, m_rack ) );
      btnOk.setText( "Ok" );
      getContentPane().add( btnOk, gbc_btnOk );

      JButton btnCancel = new JButton( "Cancel" );
      GridBagConstraints gbc_btnCancel = new GridBagConstraints();
      gbc_btnCancel.insets = new Insets( 0, 0, 5, 0 );
      gbc_btnCancel.gridx = 2;
      gbc_btnCancel.gridy = 5;
      btnCancel.addActionListener( new ActionListener() {
         public void actionPerformed( ActionEvent e )
         {
            dispose();
         }
      } );

      getContentPane().add( btnCancel, gbc_btnCancel );
   }

   private String getText()
   {
      return m_textField.getText();
   }

   // ------------------------------------------------------
   private class ActionRigRename extends AbstractAction
   {
      private ElevenRack      m_rack;
      private DialogRigRename m_dialog;

      public ActionRigRename( DialogRigRename dialog, ElevenRack rack )
      {
         m_rack = rack;
         m_dialog = dialog;
      }

      public void actionPerformed( ActionEvent e )
      {
         try {
            m_rack.renameRig( m_dialog.getText() );
            m_dialog.setVisible( false );
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            ErrorBox.error( "USB MIDI transmission error", e1.getMessage() );
         }
      }
   }
}
