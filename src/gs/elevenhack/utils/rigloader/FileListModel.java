package gs.elevenhack.utils.rigloader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractListModel;


@SuppressWarnings( "serial" )
public class FileListModel extends AbstractListModel< String >
{
   private ArrayList<File> m_files = new ArrayList<File>();
   
   public void setFileList( File[] files )
   {
      m_files.addAll( Arrays.asList(files) );
      fireContentsChanged( this, 0, m_files.size() );
   }
   
   public void addFiles( File[] files )
   {
      int offset = m_files.size();
      m_files.addAll( Arrays.asList(files) );
      fireContentsChanged( this, offset, files.length );
   }
   
   public void removefile( int n )
   {
      m_files.remove( n );
      fireContentsChanged( this, n, m_files.size() );
   }
   
   public void clear()
   {
      m_files.clear();
      fireContentsChanged( this, 0, m_files.size() );
   }
   
   @Override
   public String getElementAt( int n )
   {
      return m_files.get( n ).getName();
   }

   @Override
   public int getSize()
   {
      return m_files.size();
   }

   public File getFile( int idx )
   {
      return m_files.get( idx );
   }
}
