package gs.elevenhack.utils.rigloader;

import gs.elevenhack.Term;

import java.io.InputStream;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

public class ElevenHack
{
   public static final String version = "2.0";

   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      Term.activated = false;
      EventQueue.invokeLater( new Runnable() {
         public void run()
         {
            try {
               RigLoaderGui window = new RigLoaderGui();
               window.m_frame.setVisible( true );

            }
            catch( Exception e ) {
               e.printStackTrace();
            }
         }
      } );
   }
   
   /**
    * Custom font Management.
    */
   
   // Prepare a static "cache" mapping font names to Font objects.
   private static String[] names = {  }; // List to preload.

   private static Map<String, Font> cache = new HashMap<String, Font>(names.length);
   static {
     for (String name : names) {
       cache.put(name, getFont(name, 20f));
     }
   }

   public static Font getFont(String name, float size) {
     Font font = null;
     if (cache != null) {
       if ((font = cache.get(name)) != null) {
         return font;
       }
     }
     
     String fName = "fonts/" + name;
     
     try {
        Term.println( "Loading font " + fName );
       InputStream is = ElevenHack.class.getResourceAsStream(fName);
       font = Font.createFont(Font.TRUETYPE_FONT, is);
       font = font.deriveFont( size );
     } catch (Exception ex) {
       ex.printStackTrace();
       System.err.println(fName + " not loaded.  Using serif font.");
       font = new Font("serif", Font.PLAIN, 20);
     }
     return font;
   }
   

}
