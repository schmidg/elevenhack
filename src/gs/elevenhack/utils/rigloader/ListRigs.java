package gs.elevenhack.utils.rigloader;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;

import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.ERError;
import gs.elevenhack.Term;

import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.AbstractAction;

public class ListRigs extends javax.swing.JList<String>
{
   private CustomListSelectionModel m_selector;
   private ElevenRack m_rack;
   private ListSelectionListener m_selectListener;
   
   //-----------------------------------------------------------------------
   ListRigs( ElevenRack er)
   {
	   m_rack = er;
      setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
      m_selector = new CustomListSelectionModel();
      setSelectionModel( m_selector );
      
      m_selectListener = new ListSelectionListener(){
 
			@Override
            public void valueChanged(ListSelectionEvent e)
            {
				if( e.getValueIsAdjusting()) return;
				int firstIndex = getSelectedIndex();
				Term.println("Send select " + firstIndex );
				try {
	                m_rack.getTransmitter().selectCurrentRig(0, firstIndex);
                }
                catch (ERError e1) {
	                // TODO Auto-generated catch block
	                e1.printStackTrace();
                }
            }			
		};

      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_DOWN, 0), "nextrig" );
      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_UP, 0), "prevrig" );
      getActionMap().put( "nextrig", new ActionNextRig() );
      getActionMap().put( "prevrig", new ActionPrevRig() );
         
   }
   
   //-----------------------------------------------------------------------
   void setSelectedSilent( int id )
   {
      disableGui();
      setSelectedIndex( id );
      enableGui();
   } 
   
   //-----------------------------------------------------------------------
   private class ActionNextRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            m_rack.selectNextItem();
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }
   
   //-----------------------------------------------------------------------
   private class ActionPrevRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            m_rack.selectPrevItem();
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }
   
   //-----------------------------------------------------------------------
   public void enableGui()
   {
      addListSelectionListener( m_selectListener );
   }
   //-----------------------------------------------------------------------
   public void disableGui()
   {
      removeListSelectionListener( m_selectListener );
   }
}
