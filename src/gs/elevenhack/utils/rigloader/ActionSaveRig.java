package gs.elevenhack.utils.rigloader;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.TextRender;
import gs.elevenhack.midi.ReceiverCallback;
import gs.elevenhack.midi.SysEx;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxFileIO;
import gs.elevenhack.tfx.TfxParser;

@SuppressWarnings( "serial" )
public class ActionSaveRig extends AbstractAction
{

   private ElevenRack          m_rack;
   private final static String PREF_DEFAULT_SAVE_RIG_PATH = "DefaultSaveRigPath";

   // ------------------------------------------------------------------
   private class SaveFileCallback extends ReceiverCallback
   {
      SaveFileCallback( byte[] expectedSeq )
      {
         super( expectedSeq );
      }

      @Override
      public void onExpectedSeq( byte[] msg )
      {
         Term.println( "Save raw msg: " + SysEx.byteMsgToString( msg ) );
         int bulkoffset = 6;

         byte[] tmp = Arrays.copyOfRange( msg, bulkoffset, msg.length );
         byte[] bulk = SysEx.extractFrom7bits( tmp, 0, tmp.length );
         Term.println( "Save Bulk: " + SysEx.byteMsgToString( bulk ) );
         saveFile( bulk );
      }
   }

   // ------------------------------------------------------------------
   public ActionSaveRig( ElevenRack er )
   {
      m_rack = er;
   }

   // ------------------------------------------------------------------
   public void actionPerformed( ActionEvent e )
   {
      byte[] expectedSeq = { SysEx.RESPOND, SysEx.CMD_GET_BULK_TFX };

      m_rack.getReceiver().setCallbackOnce( new SaveFileCallback( expectedSeq ) );
      try {
         m_rack.getTransmitter().requestBulkRig();
      }
      catch( ERError e1 ) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
         ErrorBox.error( "Cannot send request to 11R.", e1.getMessage() );
      }
   }

   // ------------------------------------------------------------------
   static public void saveFile( byte[] msg )
   {
      try {

         EHFileChooser chooser = new EHFileChooser(PREF_DEFAULT_SAVE_RIG_PATH);

         // Default File name.
         TfxParser p = new TfxParser();
         String rigName = p.retrieveName( msg );

         File suggestedFile = new File( rigName + ".tfx" );
         chooser.setSelectedFile( suggestedFile );

         int returnVal = chooser.showSaveDialog( null );
         if( returnVal == JFileChooser.APPROVE_OPTION ) {
            File f = chooser.getSelectedFile();
            chooser.storeDefaultPath( f.toPath().getParent().toString() );
            TfxFileIO.writeTfx( f.getCanonicalPath(), msg );
         }
      }
      catch( IOException e ) {
         e.printStackTrace();
         ErrorBox.error( "Error while saving: ", e.getMessage() );
      }
      catch( TfxError e ) {
         e.printStackTrace();
         ErrorBox.error( "TFX format error.", e.getMessage() );
      }
   }
}
