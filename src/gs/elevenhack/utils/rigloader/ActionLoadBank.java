package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.midi.ReceiverCallback;
import gs.elevenhack.tfx.TfxError;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;

public class ActionLoadBank extends AbstractAction
{

   private ElevenRack                        m_rack;
   private final static String               PREF_DEFAULT_SAVE_BANK_PATH = "DefaultSaveBankPath";
   private ProgressMonitor                   m_pMon;
   private int                               m_rigProgressCount          = 0;
   private ZipFile                           m_zif;
   private Enumeration< ? extends ZipEntry > m_entryList;
   private RigLoaderGui                      m_gui;
   private int                               m_orgSelectedRig;
   private ZipEntry                          m_ze;
   private String                            m_basename;
   private int                               m_loc;

   // ------------------------------------------------------------------
   public ActionLoadBank( ElevenRack er, RigLoaderGui gui )
   {
      m_rack = er;
      m_gui = gui;
   }

   // ------------------------------------------------------------------
   public void actionPerformed( ActionEvent e )
   {
      try {

         EHFileChooser chooser = new EHFileChooser( PREF_DEFAULT_SAVE_BANK_PATH );
         chooser.setDialogTitle( "Select a bank file." );
         // File suggestedFile = new File( "NewRigBank.ehb" );
         // chooser.setSelectedFile( suggestedFile );

         int returnVal = chooser.showSaveDialog( null );
         if( returnVal == JFileChooser.APPROVE_OPTION ) {
            File f = chooser.getSelectedFile();
            chooser.storeDefaultPath( f.getParent() );

            byte[] acqSeq = { (byte) 0x12, (byte) 0x02 }; // Acq on select rig.

            m_rack.getReceiver().setCallback( new ReceiverCallback( acqSeq ) {
               @Override
               public void onExpectedSeq( byte[] msg )
               {
                  loadNextRig( null );
               }
            } );

            mLoadBank( f );
         }
      }
      catch( Exception ex ) {
         ex.printStackTrace();
         ErrorBox.error( "Error while saving bank: ", ex.getMessage() );
      }
   }

   // ------------------------------------------------------------------
   public void mLoadBank( File file ) throws ERError, TfxError
   {
      try {
         m_pMon = new ProgressMonitor( m_gui.getFrame(), "Loading Rig Bank.",
                                       "Reading rigs from bank file...", 0, 1 );
         m_pMon.setMinimum( 0 );
         m_pMon.setMaximum( 103 );
         m_pMon.setProgress( 0 );
         m_pMon.setMillisToPopup(0);
         m_pMon.setMillisToDecideToPopup( 0 );

         m_gui.disableGui();
         m_rigProgressCount = 0;
         m_zif = new ZipFile( file );
         m_entryList = m_zif.entries();
         m_orgSelectedRig = m_rack.getCurrentRigNum();
         m_loc = 0;
         // Just request name because of some comm freeze.

         selectNextRigLocation();

      }
      catch( FileNotFoundException e ) {
         mError( e, "Cannot open selected Rig Bank file." );
      }
      catch( IOException e ) {
         mError( e, "Error while reading Rig Bank file." );
      }
   }

   // ------------------------------------------------------------------
   public void selectNextRigLocation()
   {
      try {
         if( m_entryList.hasMoreElements() ) {
            m_ze = m_entryList.nextElement();
            
            boolean foundRig = false;

            while( !foundRig && m_ze != null ) {
               String filename = m_ze.getName();
               m_basename = new File( filename ).getName();
               String locName = m_basename.substring( 0, 2 );
               m_loc = ElevenRack.NameToLoc( locName );

               if( m_loc != -1 ) foundRig = true;
               else m_ze = m_entryList.nextElement();
            }
            if( m_ze != null ) {

               if( m_pMon != null ) {
                  m_pMon.setProgress( m_rigProgressCount );
                  m_pMon.setNote( "Loading rig " + m_basename + "." );
               }

               m_rack.getTransmitter().selectCurrentRig( 0, m_loc );
               m_rack.getTransmitter().requestCurrentRigNumber();
            }
         }
         else {
            mCleanup();
            m_rack.getTransmitter().selectCurrentRig( 0, m_orgSelectedRig );
         }
      }
      catch( ERError e ) {
         mError( e, "Error while sending Rig data to device." );
      }
   }

   // ------------------------------------------------------------------
   public void loadNextRig( byte[] msg )
   {
      if( m_pMon != null && m_pMon.isCanceled() ) {
         mCleanup();
         return;
      }
      try {
         Term.println( "Loading rig " + m_basename + " to location " + m_loc );
         m_rack.loadRigStream( m_zif.getInputStream( m_ze ), true );
         Term.println( "Requesting Rig Name." );
         m_rack.getTransmitter().requestRigName( 0, m_loc );
         ++m_rigProgressCount;
         selectNextRigLocation();
      }
      catch( FileNotFoundException e ) {
         mError( e, "Cannot open selected Rig Bank file." );
      }
      catch( IOException e ) {
         mError( e, "Error while reading Rig Bank file." );
      }
      catch( ERError e ) {
         mError( e, "Error while sending Rig data to device." );
      }
      catch( TfxError e ) {
         mError( e, "Bad rig format in Rig Bank file." );
      }
   }

   private void mError( Exception e, String errmsg )
   {
      Term.println( e.getMessage() );
      ErrorBox.error( "Rig Bank Load Error", errmsg );
      mCleanup();
   }

   private void mCleanup()
   {
      if( m_pMon == null ) {
         m_pMon.close();
      }
      m_rack.getReceiver().clearCallback();
      if( m_zif != null ) try {
         m_zif.close();
      }
      catch( IOException e1 ) {
      }
      m_gui.enableGui();
      try {
         m_rack.getTransmitter().selectCurrentRig( 0, m_orgSelectedRig );
      }
      catch( ERError e1 ) {
         Term.println( e1.getMessage() );
      }

   }
}