package gs.elevenhack.utils.rigloader;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;

@SuppressWarnings( "serial" )
public class AboutBox extends JDialog
{
   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      try {
         AboutBox dialog = new AboutBox();
         dialog.setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );
         dialog.setVisible( true );
      }
      catch( Exception e ) {
         e.printStackTrace();
      }
   }

   /**
    * Create the dialog.
    */
   public AboutBox()
   {
      setModal( true );
      setTitle( "About ElevenHack" );
      setBounds( 100, 100, 610, 700 );
      getContentPane().setLayout( new BorderLayout() );
      setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );
      {
         JPanel buttonPane = new JPanel();
         buttonPane.setLayout( new FlowLayout( FlowLayout.RIGHT ) );
         getContentPane().add( buttonPane, BorderLayout.SOUTH );
         {
            JButton okButton = new JButton( "OK" );
            okButton.setActionCommand( "OK" );
            okButton.addActionListener( new ActionListener() {
               public void actionPerformed( ActionEvent e )
               {
                  dispose();
               }
            } );
            buttonPane.add( okButton );
            getRootPane().setDefaultButton( okButton );
         }
      }
      {
         JScrollPane scrollPane = new JScrollPane();
         getContentPane().add( scrollPane, BorderLayout.CENTER );
         JTextPane jtext = new JTextPane();
         jtext.setContentType( "text/html" );
         jtext.setEditable( false );
         jtext.setText( "<center><H2>Eleven Hack "
                        + ElevenHack.version
                        + "</H2></center>"
                        + "<center>CopyRight (c) 2012,2013 Guillaume Schmid </center>"
                        + "<center><br>Thank you for using Eleven Hack!</center><br>"
                        + "<center>Please visit our website and check for news:</center><br>"
                        + "<center><a href=\"http://sites.google.com/site/elevenhack\">"
                        + "http://sites.google.com/site/elevenhack</a></center>"
                        + "<br>Thanks to:" 
                        + "<br> - Hellbat for the cool splash screen!"
                        + "<br> - Str@man, misfit, Seemöwe and roachone for testing."
                        + "<hr>"
                        + "<br>THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,"
                        + " EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF "
                        + "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT."
                        + " IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY "
                        + "CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, "
                        + "TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE "
                        + "OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
                        + "<hr>"
                        + "This software uses the libRtMidi (http://www.music.mcgill.ca/~gary/rtmidi/)"
                        + "with the following licence:"
                        + "<br>RtMidi: realtime MIDI i/o C++ classes"
                        + "<br>Copyright (c) 2003-2012 Gary P. Scavone"
                        + "<br>Permission is hereby granted, free of charge, to any person obtaining "
                        + "a copy of this software and associated documentation files (the \"Software\"), "
                        + "to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:"
                        + "The above copyright notice and this permission notice shall be included in all "
                        + "copies or substantial portions of the Software."
                        + "Any person wishing to distribute modifications to the Software is asked to send the "
                        + "modifications to the original developer so that they can be incorporated into the "
                        + "canonical version. This is, however, not a binding provision of this "
                        + "license."
                        + "<br>THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, "
                        + "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES "
                        + "OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT."
                        + " IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY"
                        + " CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, "
                        + "TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE "
                        + "SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE." );
         scrollPane.setViewportView( jtext );
         scrollPane.getVerticalScrollBar().setValue(0);
         {
            JLabel splashLabel = new JLabel( "" );
            ClassLoader classLoader = Thread.currentThread()
                                            .getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream( "11HV2.png" );

            try {
               Image logo = ImageIO.read( input );
               ImageIcon icon = new ImageIcon( logo );
               splashLabel.setIcon( icon );
            }
            catch( IOException e ) {
               e.printStackTrace();
            }

            scrollPane.setColumnHeaderView( splashLabel );
         }
      }
   }
}
