package gs.elevenhack.utils.rigloader;

import java.util.EventListener;

import gs.elevenhack.ERError;
import gs.elevenhack.ER.ElevenListener;
import gs.elevenhack.ER.ElevenRack;

import javax.swing.table.AbstractTableModel;

public class RigTableModel extends AbstractTableModel
implements ElevenListener
{
   private static final int NUMROWS = 26;
   private static final int NUMCOLS = 4;
   
   private TableRigs m_tableRigs;
   private ElevenRack m_rack;
   private final String[] m_columnNames = { "1", "2", "3", "4" };
   
   RigTableModel( TableRigs tableRigs )
   {
      m_tableRigs = tableRigs;
   }
   @Override
   public void attachRack(ElevenRack er)
   {
       m_rack = er;
   }
   public int getColumnCount() { return NUMCOLS; }
   public int getRowCount() { return NUMROWS;}
   public Object getValueAt(int row, int col)
   {
      return m_rack.getRigName( 0, row * NUMCOLS + col );
   }
   
   @Override
   public String getColumnName(int col) {
      return m_columnNames[ col % NUMCOLS ];
  }
   
   @Override
   public void detachRack()
   {
      m_rack = null;
      
   }
   @Override
   public void onEffectCount( int n ) throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   @Override
   public void onEffectDescription( int id ) throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   
   //---------------------------------------------------------------
   @Override
   public void onUpdateRigList( int bank, int id ) throws ERError
   {
      int row = id / NUMCOLS;
      int col = id % NUMCOLS;
      if (bank == 0) fireTableCellUpdated( row,col );      
   }
   
   // --------------------------------------------------------------------
   @Override
   public void onRigSelected(int bank, final int num) throws ERError
   {
       if (bank == 0) {
          int prevCol = m_tableRigs.getSelectedColumn();
          int prevRow = m_tableRigs.getSelectedRow();
          int prev = prevCol * NUMCOLS + prevRow;
           if (prev != num) {
               m_tableRigs.setSelectedRig( num );
           }
       }
   }
   @Override
   public void onRigDescChanged() throws ERError
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void onStartInitDevice() throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   @Override
   public void onDeviceInitialized() throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   @Override
   public void onTunerSwitched( boolean b ) throws ERError
   {
      // TODO Auto-generated method stub
      
   }
   @Override
   public void onMainVolumeSet( int val ) throws ERError
   {
      // TODO Auto-generated method stub
      
   }
}
