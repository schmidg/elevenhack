package gs.elevenhack.utils.rigloader;

import javax.swing.JSlider;
import javax.swing.event.ChangeListener;

public class RigSlider extends JSlider
{
   private boolean m_silent = false;
   
   public void setSilentValue( int val )
   {

      setValue( val );
      m_silent = false;
   }
}
