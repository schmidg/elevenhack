package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.tfx.TfxError;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;

// -----------------------------------------------------------------
@SuppressWarnings( "serial" )
class ActionLoadRig extends AbstractAction
{

	private ElevenRack m_rack;
	private boolean m_saveOnLoad;
	private final static String PREF_DEFAULT_RIG_PATH = "DefaultRigPath";

	public ActionLoadRig(ElevenRack er, boolean saveOnLoad)
	{
		m_rack = er;
		m_saveOnLoad = saveOnLoad;
	}

	public void actionPerformed(ActionEvent e)
	{
		EHFileChooser chooser = new EHFileChooser( PREF_DEFAULT_RIG_PATH );
		chooser.ShowRigPreview();
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == EHFileChooser.APPROVE_OPTION) {
        	File f = chooser.getSelectedFile();
        	chooser.storeDefaultPath( f.getParent());
        	try {
        		int numRig = m_rack.getCurrentRigNum();
	            m_rack.loadRigFile( f.getCanonicalPath(), m_saveOnLoad );
	            m_rack.getTransmitter().requestRigName( 0, numRig );
            }
            catch (ERError e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
            }
            catch (IOException e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
            }
            catch (TfxError e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
            }
        }
	}
}
