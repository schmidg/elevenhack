package gs.elevenhack.utils.rigloader;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import net.miginfocom.swing.MigLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;
import javax.swing.JButton;


public class IndexProgressReport extends JDialog
implements ActionListener
{
   private JLabel m_lblText;
   private JTextArea textArea;
   private JButton btnClose;

   public IndexProgressReport( JFrame parent) 
   {
      //setModal( true );
      getContentPane().setBackground(Color.BLACK);

      setSize(new Dimension(620, 600));
      getContentPane().setSize(new Dimension(450, 250));
      setAlwaysOnTop(true);
      setVisible(true);

      getContentPane().setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][][grow][]"));

      JLabel lblImage = new JLabel("");
      lblImage.setBackground(Color.BLACK);
      lblImage.setSize(new Dimension(450, 211));
      getContentPane().add(lblImage, "cell 0 0,alignx center");

      m_lblText = new JLabel("");
      m_lblText.setForeground(Color.ORANGE);
      getContentPane().add(m_lblText, "cell 0 1,alignx center");

      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream input = classLoader.getResourceAsStream("11HV2.png");

      try {
         Image logo = ImageIO.read(input);
         ImageIcon icon = new ImageIcon( logo );
         lblImage.setIcon( icon );
         
      }
      catch( IOException e ) {
         e.printStackTrace();
      }

      JScrollPane scrollPane = new JScrollPane();
      getContentPane().add(scrollPane, "cell 0 2,grow");
      textArea = new JTextArea();
      scrollPane.setViewportView( textArea );
      btnClose = new JButton("Close");
      getContentPane().add(btnClose, "cell 0 3");
      btnClose.addActionListener( this );

      btnClose.setVisible( false );
      setVisible(true);
      setLocationRelativeTo( parent );
   }

   public void print( String text )
   {
      textArea.append( text );
   }
   
   public void setNote( String text )
   {
      m_lblText.setText( text );
   }
   
   public void showClose()
   {
      btnClose.setVisible( true);
   }

   //----------------------------------------------
   @Override
   public void actionPerformed( ActionEvent arg0 )
   {
     dispose();
   }
   
   
}
