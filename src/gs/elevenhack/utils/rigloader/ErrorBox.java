package gs.elevenhack.utils.rigloader;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ErrorBox
{
   public static void error( String title, String msg)
   {
      JOptionPane.showMessageDialog( new JFrame(), msg,
                                     title,
                                     JOptionPane.ERROR_MESSAGE );
   }
}
