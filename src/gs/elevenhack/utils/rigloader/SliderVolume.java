package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderVolume extends JSlider implements ChangeListener
{
   private ElevenRack m_rack;
   private boolean    m_callbackActivated;

   // ----------------------------------------------------------
   SliderVolume( ElevenRack rack )
   {
      m_rack = rack;
      m_callbackActivated = true;
      addChangeListener( this );
      setMinimum( -127 );
      setMaximum( 127 );
      setPaintLabels( true );
      setPaintTicks( true );
      setSnapToTicks( true );

   }

   // ----------------------------------------------------------
   void silentSet( int v )
   {
      m_callbackActivated = false;
      setValue( v );
      m_callbackActivated = true;
   }

   @Override
   public void stateChanged( ChangeEvent arg0 )
   {
      try {
         if( !m_callbackActivated || !m_rack.isConnected()) return;

         Term.println( "Volume " + getValue() );
         m_rack.getTransmitter().setMainVolume( getValue() );
      }
      catch( ERError e ) {
         ErrorBox.error( "Communication Error", "Could not switch the Tuner." );
         Term.print( e.getMessage() );
      }
   }
}
