package gs.elevenhack.utils.rigloader;

import javax.swing.AbstractListModel;
import gs.elevenhack.ERError;
import gs.elevenhack.ER.*;

public class RigListModel extends AbstractListModel<String>
implements ElevenListener
{

	private static final long serialVersionUID = 1L;
	private ElevenRack m_rack;
	private ListRigs m_rlist;
	private String[] m_rigIndices = { "A1", "A2", "A3", "A4", "B1", "B2", "B3",
	        "B4", "C1", "C2", "C3", "C4", "D1", "D2", "D3", "D4", "E1", "E2",
	        "E3", "E4", "F1", "F2", "F3", "F4", "G1", "G2", "G3", "G4", "H1",
	        "H2", "H3", "H4", "I1", "I2", "I3", "I4", "J1", "J2", "J3", "J4",
	        "K1", "K2", "K3", "K4", "L1", "L2", "L3", "L4", "M1", "M2", "M3",
	        "M4", "N1", "N2", "N3", "N4", "O1", "O2", "O3", "O4", "P1", "P2",
	        "P3", "P4", "Q1", "Q2", "Q3", "Q4", "R1", "R2", "R3", "R4", "S1",
	        "S2", "S3", "S4", "T1", "T2", "T3", "T4", "U1", "U2", "U3", "U4",
	        "V1", "V2", "V3", "V4", "W1", "W2", "W3", "W4", "X1", "X2", "X3",
	        "X4", "Y1", "Y2", "Y3", "Y4", "Z1", "Z2", "Z3", "Z4" };

	// --------------------------------------------------------------------
	RigListModel(ListRigs jl)
	{
		m_rlist = jl;
	}

	@Override
	public void onEffectCount(int n) throws ERError
	{
	}

	@Override
	public void onEffectDescription(int id) throws ERError
	{
	}

	// --------------------------------------------------------------------
	@Override
	public void onUpdateRigList(int bank, final int id) throws ERError
	{
		if (bank == 0) fireContentsChanged( this, id, id );
	}

	// --------------------------------------------------------------------
	@Override
	public void onRigDescChanged() throws ERError
	{
	}

	// --------------------------------------------------------------------
	@Override
	public void onRigSelected(int bank, final int num) throws ERError
	{
		if (bank == 0) {
			int prev = m_rlist.getSelectedIndex();
			if (prev != num) {
				m_rlist.setSelectedSilent( num );
				m_rlist.ensureIndexIsVisible( num );
			}
		}
	}

	// --------------------------------------------------------------------
	@Override
	public String getElementAt(int n)
	{
		return m_rigIndices[n] + " : " + m_rack.getRigName( 0, n );
	}

	// --------------------------------------------------------------------
	@Override
	public int getSize()
	{
		if (m_rack == null) return 0; // For designer.
		return m_rack.getNbRigsInBank( 0 );
	}

	@Override
	public void onStartInitDevice() throws ERError
	{
	}

	@Override
	public void onDeviceInitialized() throws ERError
	{
	}

	@Override
	public void attachRack(ElevenRack er)
	{
		m_rack = er;
	}

	@Override
	public void detachRack()
	{
		m_rack = null;
	}

   @Override
   public void onTunerSwitched( boolean b ) throws ERError {}

   @Override
   public void onMainVolumeSet( int val ) throws ERError {}

}
