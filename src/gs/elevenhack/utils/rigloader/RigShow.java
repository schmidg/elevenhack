package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

import java.awt.Dimension;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.SpinnerModel;
import javax.swing.Timer;

import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import java.awt.FlowLayout;
import javax.swing.SpinnerNumberModel;
import java.awt.Color;

@SuppressWarnings( "serial" )
public class RigShow extends JFrame
{
   private ElevenRack      m_rack;
   private String          PREF_RIGSHOW_OPEN_PATH = "PrefRigShowOpenPath";
   private String          PREF_DELAY             = "RigShowDelay";
   private String          PREF_LOOP              = "RigShowLoop";
   private int             DEFAULT_DELAY_SEC      = 10;
   private FileListModel   m_flist                = new FileListModel();
   private JList< String > m_rigList;
   private Timer           m_timer;
   private RigOverview     m_rigView;
   private JCheckBox       m_checkLoop;
   private JSpinner        m_spinDelay;
   private JToggleButton   m_btnPlay;

   // ----------------------------------------------------------------------
   RigShow( ElevenRack er )
   {
      super( "Rig Show!" );
      setAlwaysOnTop( true );
      m_rack = er;

      setModalExclusionType( ModalExclusionType.APPLICATION_EXCLUDE );

      JPanel panel = new JPanel();

      getContentPane().setLayout( new MigLayout( "",
                                                 "[grow][][grow][][][][][]",
                                                 "[][grow][]" ) );

      m_rigView = new RigOverview();
      m_rigView.setMinimumSize( new Dimension( 320, 50 ) );

      m_rigView.setForeground( Color.YELLOW );
      m_rigView.setOpaque( true );
      m_rigView.setBackground( new Color( 204, 102, 51 ) );
      m_rigView.setFont( ElevenHack.getFont( "ygrid.ttf", 26.0f ) );

      getContentPane().add( m_rigView,
                            "cell 2 0 5 1,alignx center,aligny center" );

      JScrollPane scrollPane = new JScrollPane();
      getContentPane().add( scrollPane, "cell 0 0 1 2,grow" );

      m_rigList = new JList< String >();
      scrollPane.setViewportView( m_rigList );
      m_rigList.setModel( m_flist );
      m_rigList.addListSelectionListener( new ListSelectionListener() {
         @Override
         public void valueChanged( ListSelectionEvent e )
         {
            m_rigList.ensureIndexIsVisible( m_rigList.getSelectedIndex() );
            playCurrRig();
         }
      } );

      JPanel panel_2 = new JPanel();
      getContentPane().add( panel_2, "cell 2 1 5 1,grow" );
      panel_2.setLayout( new FlowLayout( FlowLayout.CENTER, 5, 5 ) );

      JButton button = new JButton();
      button.setAction( new ActionFirstRig() );
      button.setText( "|<" );
      button.setToolTipText( "Select first" );
      panel_2.add( button );

      JButton button_1 = new JButton();
      button_1.setAction( new ActionPrevRig() );
      button_1.setText( "<" );
      button_1.setToolTipText( "Select previous rig" );
      panel_2.add( button_1 );

      m_btnPlay = new JToggleButton( "Play" );
      m_btnPlay.setToolTipText( "Play/Pause" );
      m_btnPlay.addChangeListener( new ChangeListener() {

         @Override
         public void stateChanged( ChangeEvent arg0 )
         {
            if( m_btnPlay.isSelected() ){
               playCurrRig();
               m_timer.start();
            }
            else m_timer.stop();
         }
      } );
      panel_2.add( m_btnPlay );

      JButton button_2 = new JButton();
      button_2.setAction( new ActionNextRig() );
      button_2.setText( ">" );
      button_2.setToolTipText( "Next rig" );
      panel_2.add( button_2 );

      getContentPane().add( panel, "cell 7 1" );

      JButton btnAddRig = new JButton();
      getContentPane().add( btnAddRig, "flowx,cell 0 2" );
      btnAddRig.setAction( new ActionAddRig( this ) );
      btnAddRig.setText( "Add" );

      JButton btnDelRig = new JButton();
      getContentPane().add( btnDelRig, "cell 0 2" );
      btnDelRig.setAction( new ActionDelRig() );
      btnDelRig.setText( "Del" );

      JButton btnClear = new JButton();
      getContentPane().add( btnClear, "cell 0 2" );
      btnClear.setAction( new ActionClear() );
      btnClear.setText( "Clear" );

      JPanel panel_1 = new JPanel();
      getContentPane().add( panel_1, "cell 2 2 5 1,grow" );
      panel_1.setLayout( new FlowLayout( FlowLayout.CENTER, 5, 5 ) );

      JLabel lblDelay = new JLabel( "Delay:" );
      panel_1.add( lblDelay );

      m_spinDelay = new JSpinner();
      m_spinDelay.setModel( new SpinnerNumberModel( 10, 2, 100, 1 ) );
      panel_1.add( m_spinDelay );
      m_spinDelay.addChangeListener( new ChangeListener() {

         @Override
         public void stateChanged( ChangeEvent e )
         {
            SpinnerModel model = m_spinDelay.getModel();
            if( model instanceof SpinnerNumberModel ) {
               int delay = 1000 * ( (SpinnerNumberModel) model ).getNumber()
                     .intValue();
               m_timer.setDelay( delay );
               m_timer.setInitialDelay( delay );
            }
         }
      } );

      m_checkLoop = new JCheckBox( "Loop" );
      panel_1.add( m_checkLoop );

      setSize( new Dimension( 640, 480 ) );
      pack();

      m_timer = new Timer( DEFAULT_DELAY_SEC * 1000, new ActionListener() {
         @Override
         public void actionPerformed( ActionEvent arg0 )
         {
            new ActionNextRig().actionPerformed( null );
         }
      } );

      addWindowListener( new WindowAdapter() {
         public void windowClosing( WindowEvent e )
         {
            if( m_timer != null ) m_timer.stop();
            mSaveConfig();
         }
      } );

      mRestoreConfig();
   }

   // -----------------------------------------------------------------
   public void playCurrRig()
   {
      if( m_flist.getSize() == 0 ) return;

      int idx = m_rigList.getSelectedIndex();
      if( idx == -1 ) {
         m_rigList.setSelectedIndex( 0 );
         idx = 0;
      }

      Rig rig;
      try {
         rig = new Rig();
         TfxParser p = new TfxParser();
         p.parseFile( rig, m_flist.getFile( idx ) );
         m_rigView.update( rig );
         if( m_rack.isConnected() ) {
            m_rack.loadRigFile( m_flist.getFile( idx ).getCanonicalPath(), false );
         }
      }
      catch( TfxError e ) {
          new ActionNextRig().actionPerformed( null );
          e.printStackTrace();
       }
      catch( IOException e) {
          new ActionNextRig().actionPerformed( null );
          e.printStackTrace();
       }
      catch( ERError e ) {
          new ActionNextRig().actionPerformed( null );
          e.printStackTrace();
       }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionFirstRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         if( m_flist.getSize() == 0 ) return;
         if( m_rigList.getSelectedIndex() == 0 ) return;

         m_rigList.setSelectedIndex( 0 );
      }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionNextRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         if( m_flist.getSize() == 0 ) return;

         int idx = m_rigList.getSelectedIndex();
         if( idx == -1 ) m_rigList.setSelectedIndex( 0 );
         else {
            Term.println( "idx = " + idx + " checkLoop: " + m_checkLoop.isSelected());
            if( idx >= m_flist.getSize() - 1 ) {
               if( m_checkLoop.isSelected() ) m_rigList.setSelectedIndex( 0 );
               else {
                  m_btnPlay.setSelected( false );
                  m_timer.stop();
               }
            }
            else m_rigList.setSelectedIndex( idx + 1 );
         }
      }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionPrevRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         if( m_flist.getSize() == 0 ) return;

         int idx = m_rigList.getSelectedIndex();
         if( idx < 1 ) m_rigList.setSelectedIndex( 0 );
         else {
            --idx;
            m_rigList.setSelectedIndex( idx );
         }
      }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionAddRig extends AbstractAction
   {
      private RigShow m_parent;

      ActionAddRig( RigShow parent )
      {
         m_parent = parent;
      }

      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         EHFileChooser fc = new EHFileChooser( "PREF_RIGSHOW_OPEN_PATH" );
         fc.setMultiSelectionEnabled( true );
         FileFilter filter = new FileNameExtensionFilter("TFX rig file", "tfx", "TFX");
         fc.addChoosableFileFilter((javax.swing.filechooser.FileFilter) filter);
         fc.setFileFilter( filter );
         int returnVal = fc.showOpenDialog( m_parent );
         if( returnVal == EHFileChooser.APPROVE_OPTION ) {
            try {
               fc.storeSelectedPath();
            }
            catch( IOException e ) {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
            m_flist.addFiles( fc.getSelectedFiles() );
         }
      }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionDelRig extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         int n = m_rigList.getSelectedIndex();
         if( n == -1 ) return;
         m_flist.removefile( n );
      }
   }

   // -----------------------------------------------------------------
   @SuppressWarnings( "serial" )
   private class ActionClear extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         m_flist.clear();
      }
   }

   // -----------------------------------------------------------------
   private void mSaveConfig()
   {
      Preferences pref = RigLoaderGui.getPrefs();
      pref.putInt( PREF_DELAY,
                   Integer.parseInt( m_spinDelay.getValue().toString() ) );
      pref.putBoolean( PREF_LOOP, m_checkLoop.isSelected() );
   }

   // -----------------------------------------------------------------
   private void mRestoreConfig()
   {
      Preferences pref = RigLoaderGui.getPrefs();
      int delay = pref.getInt( PREF_DELAY, DEFAULT_DELAY_SEC );
      boolean loop = pref.getBoolean( PREF_LOOP, false );

      m_spinDelay.setValue( delay );
      m_timer.setDelay( delay * 1000 );
      m_timer.setInitialDelay( delay * 1000 );
      m_checkLoop.setSelected( loop );
   }
}
