package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.midi.MidiCom;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class DialogMidiSelector extends JDialog
{

	private static final long serialVersionUID = 1L;
	private final Action actionInitMidi;
	private final RigLoaderGui m_parent;
	private final JComboBox<String> m_comboOut;
	private final JComboBox<String> m_comboIn;

	/**
	 * Create the dialog.
	 */
	public DialogMidiSelector(RigLoaderGui parent)
	{
	   setMinimumSize(new Dimension(500, 200));
		setSize( new Dimension(500, 200) );
		setType( Type.UTILITY );
		setModal( true );
		m_parent = parent;
		actionInitMidi = new ActionInitMidi( this );
        String[] devicesIn = {"No MIDI IN device detected."};
        String[] devicesOut = {"No MIDI OUT device detected."};
		try {
			devicesIn = MidiCom.listMidiInDevices();
	        devicesOut = MidiCom.listMidiOutDevices();
		} catch (ERError e1) {
			// TODO Auto-generated catch block
			ErrorBox.error("Midi connection error", "cannot retrieve the list of MIDI ports: " + e1.getMessage() );
		}
		setTitle( "Midi Channel Selection" );
		setBounds( 100, 100, 373, 219 );
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{60, 125, 81, 0};
		gridBagLayout.rowHeights = new int[]{15, 37, 24, 24, 37, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		{
			JLabel lblSelectMidiInput = new JLabel(
			        "Select Midi Input and Output Channels" );
			GridBagConstraints gbc_lblSelectMidiInput = new GridBagConstraints();
			gbc_lblSelectMidiInput.fill = GridBagConstraints.BOTH;
			gbc_lblSelectMidiInput.insets = new Insets(0, 0, 5, 0);
			gbc_lblSelectMidiInput.gridwidth = 4;
			gbc_lblSelectMidiInput.gridx = 0;
			gbc_lblSelectMidiInput.gridy = 1;
			getContentPane().add( lblSelectMidiInput, gbc_lblSelectMidiInput );
		}
		{
			JLabel lblInput = new JLabel( "Input:" );
			GridBagConstraints gbc_lblInput = new GridBagConstraints();
			gbc_lblInput.anchor = GridBagConstraints.WEST;
			gbc_lblInput.fill = GridBagConstraints.VERTICAL;
			gbc_lblInput.insets = new Insets(0, 0, 5, 5);
			gbc_lblInput.gridx = 0;
			gbc_lblInput.gridy = 2;
			getContentPane().add( lblInput, gbc_lblInput );
		}
		m_comboIn = new JComboBox<String>();
		m_comboIn.setSize(new Dimension(320, 32));
		m_comboIn.setToolTipText( "Select Eleven Rack Midi Input Channel" );
		GridBagConstraints gbc_m_comboIn = new GridBagConstraints();
		gbc_m_comboIn.fill = GridBagConstraints.BOTH;
		gbc_m_comboIn.insets = new Insets(0, 0, 5, 0);
		gbc_m_comboIn.gridwidth = 3;
		gbc_m_comboIn.gridx = 1;
		gbc_m_comboIn.gridy = 2;
		getContentPane().add( m_comboIn, gbc_m_comboIn );
		{
			JLabel lblOutput = new JLabel( "Output:" );
			GridBagConstraints gbc_lblOutput = new GridBagConstraints();
			gbc_lblOutput.anchor = GridBagConstraints.WEST;
			gbc_lblOutput.fill = GridBagConstraints.VERTICAL;
			gbc_lblOutput.insets = new Insets(0, 0, 5, 5);
			gbc_lblOutput.gridwidth = 3;
			gbc_lblOutput.gridx = 0;
			gbc_lblOutput.gridy = 3;
			getContentPane()
			        .add( lblOutput, gbc_lblOutput );
		}
		m_comboOut = new JComboBox<String>();
		//m_comboOut.setMaximumSize(new Dimension(300, 32));
		m_comboOut.setToolTipText( "Select Eleven Rack Output Channel." );
		GridBagConstraints gbc_m_comboOut = new GridBagConstraints();
		gbc_m_comboOut.fill = GridBagConstraints.BOTH;
		gbc_m_comboOut.insets = new Insets(0, 0, 5, 0);
		gbc_m_comboOut.gridwidth = 3;
		gbc_m_comboOut.gridx = 1;
		gbc_m_comboOut.gridy = 3;
		getContentPane().add( m_comboOut, gbc_m_comboOut );
		{
			int itemCount = 0;
			for (String s : devicesIn) {
				m_comboIn.addItem( "" + itemCount + " - " + s );
				++itemCount;
			}
			m_comboIn.setSelectedIndex( m_parent.getInPort());
		}
		{
			int itemCount = 0;
			for (String s : devicesOut) {
				m_comboOut.addItem( "" + itemCount + " - " + s );
				++itemCount;
			}
            m_comboOut.setSelectedIndex( m_parent.getOutPort());
		}
		{
			JButton okButton = new JButton( "OK" );
			okButton.setAction( actionInitMidi );
			GridBagConstraints gbc_okButton = new GridBagConstraints();
			gbc_okButton.anchor = GridBagConstraints.EAST;
			gbc_okButton.fill = GridBagConstraints.VERTICAL;
			gbc_okButton.insets = new Insets(0, 0, 5, 5);
			gbc_okButton.gridx = 2;
			gbc_okButton.gridy = 4;
			getContentPane()
			        .add( okButton, gbc_okButton );
			getRootPane().setDefaultButton( okButton );
			okButton.setAction( actionInitMidi );
		}
		{
			JButton cancelButton = new JButton( "Cancel" );
			GridBagConstraints gbc_cancelButton = new GridBagConstraints();
			gbc_cancelButton.insets = new Insets(0, 0, 5, 0);
			gbc_cancelButton.anchor = GridBagConstraints.EAST;
			gbc_cancelButton.fill = GridBagConstraints.VERTICAL;
			gbc_cancelButton.gridx = 3;
			gbc_cancelButton.gridy = 4;
			getContentPane().add( cancelButton, gbc_cancelButton );
			cancelButton.setActionCommand( "Cancel" );
			cancelButton.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    dispose();	                    
                }} );
		}
	}

	private class ActionInitMidi extends AbstractAction
	{

		private DialogMidiSelector m_dialog;
		private static final long serialVersionUID = 1L;

		public ActionInitMidi(DialogMidiSelector d)
		{
			m_dialog = d;
			putValue( NAME, "Ok" );
			putValue( SHORT_DESCRIPTION, "Initialize Midi" );
		}

		public void actionPerformed(ActionEvent e)
		{
			try {
				int inPort = m_comboIn.getSelectedIndex();
				int outPort = m_comboOut.getSelectedIndex();
				Term.println( "in = " + inPort + " out = " + outPort );

/*				if (inPort == outPort) {
				   ErrorBox.error("Midi Port Selection Error",
					                "Error, inport and outport are the same.\nPlease select different ports." );
					return;
				}
*/				m_dialog.m_parent.initRack( inPort, outPort );
				m_dialog.setVisible( false );
			}
			catch (ERError e1) {
               ErrorBox.error("Midi Port Selection Error",
                     "Cannot connect to device." );
			}
		}
	}
}
