package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ConfigStrings;
import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.midi.MidiCom;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;
import java.nio.file.Path;
import javax.swing.JTextField;

public class DialogCollectionConfig extends JDialog
{
   private static final long serialVersionUID = 1L;
   private final Action m_actionIndexDatabase;
   private final RigLoaderGui m_parent;
   private Path m_rigPath;
   private Path m_indexPath;
   private JTextField rigPathField;
   private JTextField indexPathField;
   private final int RIGPATH_TAG = 0;
   private final int INDEXPATH_TAG = 1;
   
   /**
    * Create the dialog.
    */
   public DialogCollectionConfig(RigLoaderGui parent)
   {
      setMinimumSize(new Dimension(500, 200));
      setSize( new Dimension(500, 200) );
      setType( Type.UTILITY );
      setModal( true );
      m_parent = parent;
      m_actionIndexDatabase = new ActionIndexDatabase( this );
      setTitle( "Collection Configuration" );
      setBounds( 100, 100, 373, 219 );
      GridBagLayout gridBagLayout = new GridBagLayout();
      gridBagLayout.columnWidths = new int[]{60, 125, 81, 0};
      gridBagLayout.rowHeights = new int[]{15, 37, 24, 24, 37, 0, 0};
      gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0};
      gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
      getContentPane().setLayout(gridBagLayout);
      {
         JLabel lblSelectMidiInput = new JLabel(
               "Select Collection Paths" );
         GridBagConstraints gbc_lblSelectMidiInput = new GridBagConstraints();
         gbc_lblSelectMidiInput.fill = GridBagConstraints.BOTH;
         gbc_lblSelectMidiInput.insets = new Insets(0, 0, 5, 0);
         gbc_lblSelectMidiInput.gridwidth = 4;
         gbc_lblSelectMidiInput.gridx = 0;
         gbc_lblSelectMidiInput.gridy = 1;
         getContentPane().add( lblSelectMidiInput, gbc_lblSelectMidiInput );
      }
      {
         JLabel lblInput = new JLabel( "Rigs directory" );
         lblInput.setToolTipText("");
         GridBagConstraints gbc_lblInput = new GridBagConstraints();
         gbc_lblInput.anchor = GridBagConstraints.EAST;
         gbc_lblInput.fill = GridBagConstraints.VERTICAL;
         gbc_lblInput.insets = new Insets(0, 0, 5, 5);
         gbc_lblInput.gridx = 0;
         gbc_lblInput.gridy = 2;
         getContentPane().add( lblInput, gbc_lblInput );
      }
      {
         rigPathField = new JTextField();
         GridBagConstraints gbc_rigPathField = new GridBagConstraints();
         gbc_rigPathField.gridwidth = 2;
         gbc_rigPathField.insets = new Insets(0, 0, 5, 5);
         gbc_rigPathField.fill = GridBagConstraints.HORIZONTAL;
         gbc_rigPathField.gridx = 1;
         gbc_rigPathField.gridy = 2;
         getContentPane().add(rigPathField, gbc_rigPathField);
         rigPathField.setColumns(10);

         String dir = RigLoaderGui.getPrefs().get( ConfigStrings.PREF_RIG_COLLECTION_PATH, null );
            if( dir != null ) { rigPathField.setText( dir );
               m_rigPath = new File( dir ).toPath();
            }
        
      }
      JButton btnSelectRigPath = new JButton("Select");
      GridBagConstraints gbc_btnSelectRigPath = new GridBagConstraints();
      gbc_btnSelectRigPath.insets = new Insets(0, 0, 5, 0);
      gbc_btnSelectRigPath.gridx = 3;
      gbc_btnSelectRigPath.gridy = 2;
      getContentPane().add(btnSelectRigPath, gbc_btnSelectRigPath);
      btnSelectRigPath.addActionListener( new ActionSelectPath( RIGPATH_TAG, rigPathField, 
                                                                  ConfigStrings.PREF_RIG_COLLECTION_PATH) );
      {
         JLabel lblOutput = new JLabel( "Collection index directory" );
         GridBagConstraints gbc_lblOutput = new GridBagConstraints();
         gbc_lblOutput.anchor = GridBagConstraints.EAST;
         gbc_lblOutput.fill = GridBagConstraints.VERTICAL;
         gbc_lblOutput.insets = new Insets(0, 0, 5, 5);
         gbc_lblOutput.gridx = 0;
         gbc_lblOutput.gridy = 3;
         getContentPane()
         .add( lblOutput, gbc_lblOutput );
      }
      {
         JButton btnSelectIndexPath = new JButton("Select");
         GridBagConstraints gbc_btnSelectIndexPath = new GridBagConstraints();
         gbc_btnSelectIndexPath.insets = new Insets(0, 0, 5, 0);
         gbc_btnSelectIndexPath.gridx = 3;
         gbc_btnSelectIndexPath.gridy = 3;
         {
            indexPathField = new JTextField();
            GridBagConstraints gbc_indexPathField = new GridBagConstraints();
            gbc_indexPathField.gridwidth = 2;
            gbc_indexPathField.insets = new Insets(0, 0, 5, 5);
            gbc_indexPathField.fill = GridBagConstraints.HORIZONTAL;
            gbc_indexPathField.gridx = 1;
            gbc_indexPathField.gridy = 3;
            getContentPane().add(indexPathField, gbc_indexPathField);
            indexPathField.setColumns(10);
            String dir = RigLoaderGui.getPrefs().get( ConfigStrings.PREF_RIG_INDEX_PATH, null );
            if( dir != null ) {
               indexPathField.setText( dir );
               m_indexPath = new File( dir ).toPath();
            }
         }
         getContentPane().add(btnSelectIndexPath, gbc_btnSelectIndexPath);
         btnSelectIndexPath.addActionListener( new ActionSelectPath( INDEXPATH_TAG, indexPathField, 
                                                                       ConfigStrings.PREF_RIG_INDEX_PATH) );

      }
      {
         JButton okButton = new JButton( "Index" );
         okButton.setAction( m_actionIndexDatabase );
         GridBagConstraints gbc_okButton = new GridBagConstraints();
         gbc_okButton.anchor = GridBagConstraints.EAST;
         gbc_okButton.fill = GridBagConstraints.VERTICAL;
         gbc_okButton.insets = new Insets(0, 0, 5, 5);
         gbc_okButton.gridx = 1;
         gbc_okButton.gridy = 4;
         getContentPane()
         .add( okButton, gbc_okButton );
         getRootPane().setDefaultButton( okButton );
         okButton.setAction( m_actionIndexDatabase );
      }
      {
         JButton cancelButton = new JButton( "Cancel" );
         cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               dispose();
            }
         });
         GridBagConstraints gbc_cancelButton = new GridBagConstraints();
         gbc_cancelButton.insets = new Insets(0, 0, 5, 5);
         gbc_cancelButton.anchor = GridBagConstraints.EAST;
         gbc_cancelButton.fill = GridBagConstraints.VERTICAL;
         gbc_cancelButton.gridx = 2;
         gbc_cancelButton.gridy = 4;
         getContentPane().add( cancelButton, gbc_cancelButton );
        // cancelButton.setActionCommand( "Cancel" );
         //cancelButton.setAction( new ActionCancel( this ) );
      }
   }
   
   private void mLaunchIndex()
   {
      Term.println( "Launchin index on " + m_rigPath.toString() + " and " + m_indexPath.toString() );
      m_parent.launchDBIndex( m_rigPath, m_indexPath );
   }

   //------------------------------------------------------------------
   private class ActionIndexDatabase extends AbstractAction
   {
      private DialogCollectionConfig m_dialog;

      public ActionIndexDatabase(DialogCollectionConfig d)
      {
         m_dialog = d;
         putValue( NAME, "Index" );
         putValue( SHORT_DESCRIPTION, "Index Collection" );
      }

      public void actionPerformed(ActionEvent e)
      {
         dispose();
         m_dialog.mLaunchIndex();
         
      }
   }

   //------------------------------------------------------------------
   class ActionSelectPath extends AbstractAction
   {
      private JTextField m_textField;
      private String m_prefTag;
      private int m_pathTag;
      
      ActionSelectPath( int pathTag, JTextField textField, String prefTag)
      {
         m_textField = textField;
         m_pathTag = pathTag;
         m_prefTag = prefTag;
      }

      //---------------------------------------------------------------
      @Override
      public void actionPerformed( ActionEvent e )
      {
         EHFileChooser chooser = new EHFileChooser( m_prefTag );
         chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );

         int returnVal = chooser.showOpenDialog( null );
         if(returnVal == EHFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            chooser.storeDefaultPath( f.getPath() );
            if( m_pathTag == INDEXPATH_TAG ) m_indexPath = f.toPath();
            else m_rigPath = f.toPath();
            
            Term.println( "paths: " + m_indexPath + " & " + m_rigPath );
            m_textField.setText( f.toString() );	               
         }
      }
   }	   

   class ActionCancel extends AbstractAction
   {
      JDialog m_dialog;
      public ActionCancel( JDialog d ) { m_dialog = d; }
      public void actionPerformed(ActionEvent e)
      {
         m_dialog.dispose();                      
      }
   }
}

