package gs.elevenhack.utils.rigloader;

import java.awt.Color;

public class ThemeColors
{
   public final static Color selectedBackgroundColor = new Color(230, 130, 0, 255 );
   public final static Color evenLineColor = new Color( 200, 200, 255, 255 );
   public final static Color oddLineColor = new Color( 240, 240, 255, 255 );
   public final static Color headerBackground = new Color( 100, 100, 100, 255 );
}
