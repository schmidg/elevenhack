package gs.elevenhack.utils.rigloader;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.List;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.tfx.TfxError;

import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;


public class TableRigs extends JTable
{
   private CustomListSelectionModel m_selector;
   private ElevenRack m_rack;
   private ListSelectionListener m_selectListener;
   
   //-----------------------------------------------------------------------
   TableRigs( ElevenRack er)
   {
      m_rack = er;

      setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
      setCellSelectionEnabled(true);
      // Headers.
      JTableHeader header = getTableHeader();

      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_DOWN, 0), "nextrow" );
      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_UP, 0), "prevrow" );
      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_DOWN, 0), "nextcol" );
      getInputMap().put( KeyStroke.getKeyStroke( KeyEvent.VK_UP, 0), "prevcol" );
      getActionMap().put( "nextrow", new ActionNextRow() );
      getActionMap().put( "prevrow", new ActionPrevRow() );
      getActionMap().put( "nextcol", new ActionNextCol() );
      getActionMap().put( "prevcol", new ActionPrevCol() );

      // Drag & Drop config.

      setDragEnabled(true);  
      setDropMode(DropMode.ON);

      setTransferHandler( new CustomTransferHandler(m_rack) );

      // Mouse Interraction Config.

      addMouseListener( new MouseAdapter()
      {
         public void mousePressed( MouseEvent e )
         {
            // Left mouse click
            if ( SwingUtilities.isLeftMouseButton( e ) )
            {
               Point p = e.getPoint();

               int row = rowAtPoint( p );
               int col = columnAtPoint( p );
               int index = row * 4 + col;
               Term.println("Send select " + index );
               try {
                  m_rack.getTransmitter().selectCurrentRig(0, index);
               }
               catch (ERError e1) {
                  // TODO Auto-generated catch block
                  e1.printStackTrace();
               }
            }
            else if ( SwingUtilities.isRightMouseButton( e ) )
            {
               /// Handle Right Click here (might be useful somedays).
            }
         }
      });

     // Cosmetic line background color.
      setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
      {
         @Override
         public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                        boolean hasFocus, int row, int column)
         {
            final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if( isSelected ) {
               c.setBackground( ThemeColors.selectedBackgroundColor);
            }
            else {
               c.setBackground(row % 2 == 0 ? ThemeColors.evenLineColor : ThemeColors.oddLineColor);
            }
            return c;
         }
      });

   }

   //-----------------------------------------------------------------------
   void setSelectedSilent( int row, int col )
   {
      //disableGui();
      changeSelection( row, col, false, false);
      //enableGui();
   } 

   //-----------------------------------------------------------------------
   public void setSelectedRig( int num )
   {
      changeSelection( num / 4, num % 4, false, false);      
   }

   //-----------------------------------------------------------------------
   private class ActionNextRow extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            int selItem = m_rack.getCurrentRigNum();
            selItem += 4;
            m_rack.selectItem( selItem );
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }

   //-----------------------------------------------------------------------
   private class ActionPrevRow extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            int selItem = m_rack.getCurrentRigNum();
            selItem -= 4;
            m_rack.selectItem( selItem );
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }
   //-----------------------------------------------------------------------
   private class ActionNextCol extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            m_rack.selectNextItem();
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }

   //-----------------------------------------------------------------------
   private class ActionPrevCol extends AbstractAction
   {
      @Override
      public void actionPerformed( ActionEvent e )
      {
         try {
            m_rack.selectPrevItem();
         }
         catch( ERError e1 ) {
            // TODO Auto-generated catch block
            Term.println( e1.getMessage() );
            ErrorBox.error("Transmission error", "Error while selecting rig on device." );
         }
      }      
   }

   /*
   //-----------------------------------------------------------------------
   public void enableGui()
   {
      addSelectionListener( m_selectListener );
   }
   //-----------------------------------------------------------------------
   public void disableGui()
   {
      removeListSelectionListener( m_selectListener );
   }
    */

   class CustomTransferHandler extends TransferHandler
   {
      private ElevenRack m_rack;

      CustomTransferHandler( ElevenRack rack)
      {
         m_rack = rack;
      }

      @Override
      public boolean canImport(TransferHandler.TransferSupport info) {

         return info.isDataFlavorSupported( DataFlavor.stringFlavor)
                || info.isDataFlavorSupported(DataFlavor.javaFileListFlavor);
      }
      
      //---------------------------------------------------------------
      String extractFilePath(TransferHandler.TransferSupport info ) throws UnsupportedFlavorException, IOException
      {
         Term.println( "AAAAAAA" );
         String filePath = (String)info.getTransferable().getTransferData(DataFlavor.stringFlavor);
         if( filePath.startsWith( "file://" )) {
            filePath = filePath.substring( 7 );
         }
         return filePath;
      }
      
      //---------------------------------------------------------------
      @Override
      public boolean importData(TransferHandler.TransferSupport info) {
         String fileName = null;
         File fileData = null;
         
         try {
            if(info.isDataFlavorSupported( DataFlavor.javaFileListFlavor )){

               List< File > ls = (List<File>) info.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
               fileData = ls.get(0);
            }
            else if( info.isDataFlavorSupported( DataFlavor.stringFlavor) ){
               fileName = extractFilePath( info );
            }
         } 
         catch (UnsupportedFlavorException ufe) {
            Term.println("importData: unsupported data flavor");
            return false;
         } 
         catch (IOException ioe) {
            Term.println("importData: I/O exception");
            return false;
         }
         // fetch the drop location
         JTable.DropLocation dl = (JTable.DropLocation) info.getDropLocation();

         int row = dl.getRow();
         int col = dl.getColumn();
         Term.println( "dropping data to row " + row + " col:" +col+ ": " + fileName );

         int numRig = row * 4 + col;
         if (numRig < 0) numRig = 0;
         if(numRig > 103 ) numRig = 103;
         
         try {
            m_rack.selectItem( numRig );
            if( fileName != null ) {
               Term.println("loading from file name String ");
               m_rack.loadRigFile( fileName, numRig, true );
            }
            else {
               Term.println( "Loading from File item" );
               m_rack.loadRigFile( fileData, numRig, true );
            }
            m_rack.getTransmitter().requestRigName( 0, numRig );
         }
         catch( Exception e ) {
            ErrorBox.error("Rig Loading Error", "There was an error while loading rig "
                           + fileName + "\n" + e.getMessage());
         }
         
         return true;
      }
   }
   //------------------------------------------------------------
   //  Row List static factory.
   //------------------------------------------------------------
   public static JList< String > buildRowHeader(final JTable table)
   {
      final int MIN_ROW_HEIGHT = 12;

      final Vector<String> headers = new Vector<String>();
      for (int i = 0; i < table.getRowCount(); i++) {
         headers.add(String.valueOf((char) (i + 65)).toUpperCase());
      }
      ListModel< String > lm = new AbstractListModel< String >() {

         public int getSize() {
            return headers.size();
         }

         public String getElementAt(int index) {
            return headers.get(index);
         }
      };

      final JList< String > rowHeader = new JList< String >(lm);
      rowHeader.setOpaque(false);
      rowHeader.setFixedCellWidth(25);
      rowHeader.setBackground( ThemeColors.headerBackground );
      
      MouseInputAdapter mouseAdapter = new MouseInputAdapter() {
         Cursor oldCursor;
         Cursor RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
         int index = -1;
         int oldY = -1;

         @Override
         public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
         }

         @Override
         public void mouseMoved(MouseEvent e) {
            super.mouseMoved(e);
            int previ = getLocationToIndex(new Point(e.getX(), e.getY() - 3));
            int nexti = getLocationToIndex(new Point(e.getX(), e.getY() + 3));
            if (previ != -1 && previ != nexti) {
               if (!isResizeCursor()) {
                  oldCursor = rowHeader.getCursor();
                  rowHeader.setCursor(RESIZE_CURSOR);
                  index = previ;
               }
            } else if (isResizeCursor()) {
               rowHeader.setCursor(oldCursor);
            }
         }

         private int getLocationToIndex(Point point) {
            int i = rowHeader.locationToIndex(point);
            if (!rowHeader.getCellBounds(i, i).contains(point)) {
               i = -1;
            }
            return i;
         }

         @Override
         public void mouseReleased(MouseEvent e) {
            super.mouseReleased(e);
            if (isResizeCursor()) {
               rowHeader.setCursor(oldCursor);
               index = -1;
               oldY = -1;
            }
         }

         @Override
         public void mouseDragged(MouseEvent e) {
            super.mouseDragged(e);
            if (isResizeCursor() && index != -1) {
               int y = e.getY();
               if (oldY != -1) {
                  int inc = y - oldY;
                  int oldRowHeight = table.getRowHeight(index);
                  if (oldRowHeight > 12 || inc > 0) {
                     int rowHeight = Math.max(MIN_ROW_HEIGHT, oldRowHeight + inc);
                     table.setRowHeight(index, rowHeight);
                     if (rowHeader.getModel().getSize() > index + 1) {
                        int rowHeight1 = table.getRowHeight(index + 1) - inc;
                        rowHeight1 = Math.max(12, rowHeight1);
                        table.setRowHeight(index + 1, rowHeight1);
                     }
                  }
               }
               oldY = y;
            }
         }

         private boolean isResizeCursor() {
            return rowHeader.getCursor() == RESIZE_CURSOR;
         }
      };
      //rowHeader.addMouseListener(mouseAdapter);
      //rowHeader.addMouseMotionListener(mouseAdapter);
      //rowHeader.addMouseWheelListener(mouseAdapter);

      rowHeader.setCellRenderer(new RowHeaderRenderer(table));
      rowHeader.setBackground(table.getBackground());
      rowHeader.setForeground(table.getForeground());
      return rowHeader;
   }

   static class RowHeaderRenderer extends JLabel implements ListCellRenderer< Object > {

      private JTable table;

      RowHeaderRenderer(JTable table) {
         this.table = table;
         JTableHeader header = this.table.getTableHeader();
         setOpaque(true);
         setBorder(UIManager.getBorder("TableHeader.cellBorder"));
         setHorizontalAlignment(CENTER);
         setForeground(header.getForeground());
         setBackground(header.getBackground());
         setFont(header.getFont());
         setDoubleBuffered(true);
      }

      public Component getListCellRendererComponent(JList< ? > list, Object value,
                                                    int index, boolean isSelected, boolean cellHasFocus) {
         setText((value == null) ? "" : value.toString());
         setPreferredSize(null);
         setPreferredSize(new Dimension((int) getPreferredSize().getWidth(), table.getRowHeight(index)));
         //trick to force repaint on JList (set updateLayoutStateNeeded = true) on BasicListUI
         list.firePropertyChange("cellRenderer", 0, 1);
         return this;
      }
   }
}
