package gs.elevenhack.utils.rigloader;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

public class ThemeLoader
{
   public static void loadTheme()
   {
      // Set System L&F

      try {
         for ( LookAndFeelInfo info : UIManager.getInstalledLookAndFeels() ) {
            if( "Nimbus".equals( info.getName() ) ) {
               UIManager.setLookAndFeel( info.getClassName() );
               break;
            }
         }
      }
      catch( Exception a ) {
         try {
            UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
         }
         catch( ClassNotFoundException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch( InstantiationException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch( IllegalAccessException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch( UnsupportedLookAndFeelException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }

   }
}
