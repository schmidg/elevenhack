package gs.elevenhack.utils.rigloader;

import gs.elevenhack.Term;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;

@SuppressWarnings( "serial" )
public class EHFileChooser extends JFileChooser
{
   private String      m_defaultPathTag;
   static final String PREF_WIDTH  = "EHFileChooser_Width";
   static final String PREF_HEIGHT = "EHFileChooser_Height";
   private Preferences m_pref;

   EHFileChooser( String prefDefaultPathTag )
   {
      m_defaultPathTag = prefDefaultPathTag;
      // Select detailview.
      AbstractButton button = SwingUtils.getDescendantOfType( AbstractButton.class,
                                                              this,
                                                              "Icon",
                                                              UIManager.getIcon( "FileChooser.detailsViewIcon" ) );
      button.doClick();

      m_pref = RigLoaderGui.getPrefs();
      String defaultPath = m_pref.get( prefDefaultPathTag, null );
      if( defaultPath != null ) {
         File df = new File( defaultPath );
         if( df.exists() && df.isDirectory() ) {
            setCurrentDirectory( new File( defaultPath ) );
         }
      }
      int width = m_pref.getInt( PREF_WIDTH, -1 );
      int height = m_pref.getInt( PREF_HEIGHT, -1 );

      if( width != -1 && height != -1 ) {
         setPreferredSize( new Dimension( width, height ) );
      }
   }

   public void ShowRigPreview()
   {
      RigPreview rp = new RigPreview();
      setAccessory( rp );
      addPropertyChangeListener( rp );
   }

   @Override
   protected void processComponentEvent( ComponentEvent e )
   {
      if( e.getID() == ComponentEvent.COMPONENT_RESIZED ) {
         m_pref.putInt( PREF_WIDTH, getWidth() );
         m_pref.putInt( PREF_HEIGHT, getHeight() );
      }
      super.processComponentEvent( e );
   }

   void storeDefaultPath( String path )
   {
      Preferences pref = RigLoaderGui.getPrefs();
      pref.put( m_defaultPathTag, path );
      try {
         pref.flush();
      }
      catch( BackingStoreException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   public void storeSelectedPath() throws IOException
   {
      storeDefaultPath( getCurrentDirectory().getCanonicalPath() );
   }
   
   //------------------------------------------------------------------
   // TEST.
   //------------------------------------------------------------------
   public static void main( String[] args)
   {
      Term.activated = true;
      ThemeLoader.loadTheme();
      EHFileChooser chooser = new EHFileChooser( "~/Dropbox/ElevenRack" );
      chooser.ShowRigPreview();
      chooser.showOpenDialog(null);
   }
}
