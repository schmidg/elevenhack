package gs.elevenhack.utils.rigloader;

import gs.elevenhack.Term;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.tfx.TfxParser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.JFileChooser;

@SuppressWarnings( "serial" )
public class RigPreview extends RigOverview implements PropertyChangeListener
{
   private File m_file;

   @Override
   public void propertyChange( PropertyChangeEvent e )
   {
      Term.println( "propertyChange." );
      String prop = e.getPropertyName();

      // If the directory changed, don't show an image.
      if( JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals( prop ) ) {
         m_file = null;
         clear();

         // If a file became selected, find out which one.
      }
      else if( JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals( prop ) ) {
         m_file = (File) e.getNewValue();
         Rig rig;
         try {
            rig = new Rig();
            TfxParser p = new TfxParser();
            p.parseFile( rig, m_file );
            update( rig );
         }
         catch( Exception ex ) {
            Term.println( "Parsing error:" + ex.getMessage() );
            clear();
         }
      }
   }
}
