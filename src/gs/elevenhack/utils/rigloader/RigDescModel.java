package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ERError;
import gs.elevenhack.ER.ElevenListener;
import gs.elevenhack.ER.ElevenRack;

import javax.swing.AbstractListModel;

public class RigDescModel extends AbstractListModel<String>
implements ElevenListener
{
   private ElevenRack m_rack;
   int m_bank;
   int m_rigNum;
   
   //----------------------------------------------------------------------
   RigDescModel( ElevenRack er)
   {
      m_rack = er;
      m_bank = -1;
      m_rigNum = -1;
   }
   
   //----------------------------------------------------------------------
   @Override
   public void onRigSelected( int bank, int num ) throws ERError
   {
      if( bank == 0 && m_rigNum != num ) this.fireContentsChanged( this, num, num );
   }
   
   //----------------------------------------------------------------------
   @Override public void onEffectDescription( int id ) throws ERError
   {
      
   }
   
   //----------------------------------------------------------------------
   @Override
   public String getElementAt( int pos )
   {
      StringBuilder sb = new StringBuilder();
      sb.append( m_rack.getRigEffectType( pos ) );
      return sb.toString();
   }
   
   //----------------------------------------------------------------------
   @Override
   public int getSize()
   {
      return m_rack.getNbEffects();
   }
   
   //----------------------------------------------------------------------
   @Override public void attachRack( ElevenRack er ) { m_rack = er; }
   @Override public void detachRack() {}
   @Override public void onEffectCount( int n ) throws ERError {}
   @Override public void onUpdateRigList( int bank, int id ) throws ERError {}
   @Override public void onRigDescChanged() throws ERError {}
   @Override public void onStartInitDevice() throws ERError {}
   @Override public void onDeviceInitialized() throws ERError {}
   @Override public void onTunerSwitched( boolean b ) throws ERError {}
   @Override public void onMainVolumeSet( int v ) throws ERError {} 

}
