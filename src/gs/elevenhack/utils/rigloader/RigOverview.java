package gs.elevenhack.utils.rigloader;

import gs.elevenhack.SelectOption;
import gs.elevenhack.Term;
import gs.elevenhack.ER.Effect;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.RigRenderer;
import gs.elevenhack.ER.TextRender;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import java.awt.Dimension;
import java.util.ArrayList;

@SuppressWarnings( "serial" )
public class RigOverview extends JPanel implements RigRenderer
{
   private JLabel              m_lblName;
   private int                 m_currEffectNum;
   private ArrayList< JLabel > m_lblTypes;
   private ArrayList< JPanel > m_panTypes;
   private ArrayList< JLabel > m_lblEffects;
   private Font                m_nameFont      = ElevenHack.getFont( "ygrid.ttf",
                                                                     26 );
   private Font                m_itemFont      = ElevenHack.getFont( "stan0755.ttf",
                                                                     16 );
   private Color               m_colBackground = new Color( 204, 102, 51 );
   private Color               m_colForeground = Color.YELLOW;
   private int                 m_currType      = 0;

   RigOverview()
   {
      m_currEffectNum = -1;
      setLayout( new MigLayout( "center", "40[shrink][shrink]",
                                "[]2[]2[]2[]2[]2[]2[]2[]2[]2[]" ) );

      m_lblName = new JLabel( "Rig Name" );
      m_lblName.setOpaque( true );
      m_lblName.setMinimumSize( new Dimension( 340, 50 ) );
      m_lblName.setHorizontalAlignment( SwingConstants.CENTER );
      m_lblName.setForeground( m_colForeground );
      m_lblName.setFont( m_nameFont );
      m_lblName.setBackground( m_colBackground );
      setBackground( m_colBackground );
      add( m_lblName, "cell 0 0 2 2,alignx center" );

      m_lblTypes = new ArrayList< JLabel >();
      m_lblEffects = new ArrayList< JLabel >();
      m_panTypes = new ArrayList<JPanel>();

      for ( int i = 0; i < 10; ++i ) {
         JLabel lblType = new JLabel();
         lblType.setFont( m_itemFont );
         m_lblTypes.add( lblType );
         lblType.setForeground( m_colForeground );
         JPanel pan = new JPanel();
         pan.setBackground( m_colBackground );
         pan.add( lblType );
         m_panTypes.add( pan );
         add( pan, "cell 0 " + ( i + 1 ) + ",grow" );

         JLabel lblEffect = new JLabel();
         lblEffect.setFont( m_itemFont );
         lblEffect.setForeground( m_colForeground );
         m_lblEffects.add( lblEffect );
         add( lblEffect, "cell 1 " + ( i + 1 ) );
      }
      clear();
   }

   // ------------------------------------------------
   public void update( Rig rig )
   {
      // should clear...
      m_currEffectNum = -1;

      rig.render( this );
   }

   // -----------------------------------------------
   public void clear()
   {
      m_lblName.setText( "Rig Name" );
      for ( JLabel l : m_lblEffects ) {
         l.setText( "Effect name" );
      }
      for ( JLabel l : m_lblTypes ) {
         l.setText( "Type" );
         l.setForeground( m_colBackground );
      }
      for ( JPanel p : m_panTypes ) {
         p.setBackground( m_colForeground );
      }
   }

   @Override
   public void rigName( String name )
   {
      m_lblName.setText( name );
   }

   @Override
   public void rigVersion( int v )
   {
   }

   @Override
   public void newEffectName( int type, int effectClass, String name )
   {
      m_currType = type;
      if( effectClass > 9 ) return;
      ++m_currEffectNum;
      m_lblTypes.get( m_currEffectNum )
                .setText( Effect.getClassShortName( effectClass ) );
      m_lblEffects.get( m_currEffectNum ).setText( name );
   }

   @Override
   public void knobParam( String name, int Value )
   {
   }

   @Override
   public void selectorParam( String name, SelectOption[] options, int val )
   {
   }

   @Override
   public void switchParam( String name, int val )
   {
      if( m_currType >= 9 ) return;
      if( name.equals( "bypa" ) ) {
         JLabel l = m_lblTypes.get( m_currEffectNum );
         if( val == 0 || val ==  -2147483648 ) {
            l.setForeground( m_colBackground );
            m_panTypes.get( m_currEffectNum ).setBackground( m_colForeground );            
         }
         else {
            l.setForeground( m_colForeground );
            m_panTypes.get( m_currEffectNum ).setBackground( m_colBackground );
         }
      }

   }

   @Override
   public void commitEffect()
   {
   }

   @Override
   public void renderUnset( String m_deco )
   {
   }
}
