package gs.elevenhack.utils.rigloader;

import gs.elevenhack.ConfigStrings;
import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenDumper;
import gs.elevenhack.ER.ElevenListener;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.database.RigDBPanel;
import gs.elevenhack.database.RigDatabase;
import gs.elevenhack.database.RigIndexer;
import gs.elevenhack.database.RigSelectedCallback;
import gs.elevenhack.tfx.TfxError;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ProgressMonitor;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import java.awt.Dimension;

public class RigLoaderGui implements ElevenListener
{

   JFrame                      m_frame;
   private ElevenRack          m_rack;
   private TableRigs           m_rigTable;
   private ButtonTuner         m_btnTune;
   private SliderVolume        m_sliderVol;
   private EProgressMonitor    m_pMon;
   private RigTableModel       m_rigTableModel;
   private RigDBPanel          m_indexPanel;
   private RigDatabase         m_db;
   
   static public Preferences getPrefs()
   {
      return Preferences.userNodeForPackage( RigLoaderGui.class );
   }

   /**
    * Create the application.
    */
   public RigLoaderGui()
   {
      m_rack = new ElevenRack();
      initialize();
      Preferences prefs = getPrefs();
      int inPort = prefs.getInt( ConfigStrings.PREF_INPORT, -1 );
      int outPort = prefs.getInt( ConfigStrings.PREF_OUTPORT, -1 );
      try {
         Term.println( "Trying to connect with ports " + inPort + " and "
                       + outPort + "." );
         if( inPort != -1 && outPort != -1 ) initRack( inPort, outPort );
      }
      catch( ERError e ) {
         inPort = -1;
         outPort = -1;
         m_rack.close();
      }
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      // Set System L&F
      ThemeLoader.loadTheme();

      m_frame = new JFrame();
      m_frame.setTitle( "Eleven Hack! v" + ElevenHack.version );
      m_frame.setBounds( 100, 100, 279, 510 );
      m_frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

      JMenuBar menuBar = new JMenuBar();
      m_frame.setJMenuBar( menuBar );

      JMenu mnFile = new JMenu( "File" );
      menuBar.add( mnFile );

      JMenuItem mntmLoad = new JMenuItem();
      mntmLoad.setAction( new ActionLoadRig( m_rack, true ) );
      mntmLoad.setText( "Load TFX" );
      mnFile.add( mntmLoad );

      JMenuItem mntmTry = new JMenuItem();
      mntmTry.setAction( new ActionLoadRig( m_rack, false ) );
      mntmTry.setText( "Try TFX" );
      mnFile.add( mntmTry );

      JMenuItem mntmSave = new JMenuItem();
      mntmSave.setAction( new ActionSaveRig( m_rack ) );
      mntmSave.setText( "Save TFX" );
      mnFile.add( mntmSave );

      JMenuItem mntmSaveBank = new JMenuItem();
      mntmSaveBank.setAction( new ActionSaveBank( m_rack, ActionSaveBank.USER_BANK,
                                                  new ProgressMonitor(m_frame, "Saving User Rig Bank.","Writing rigs...", 0, 1) ) );
   
      mntmSaveBank.setText( "Save User Presets Bank" );
      mnFile.add( mntmSaveBank );

      JMenuItem mntmSaveDemoBank = new JMenuItem();
      mntmSaveDemoBank.setAction( new ActionSaveBank( m_rack,ActionSaveBank.DEMO_BANK,
                                                  new ProgressMonitor(m_frame, "Saving Demo Rig Bank.","Writing rigs...", 0, 1) ) );
      mntmSaveDemoBank.setText( "Save Factory Presets Bank" );
      mnFile.add( mntmSaveDemoBank );

      JMenuItem mntmLoadBank = new JMenuItem();
      mntmLoadBank.setAction( new ActionLoadBank( m_rack, this));                                             
      mntmLoadBank.setText( "Load User Presets Bank" );
      mnFile.add( mntmLoadBank );

      JMenu mnDevice = new JMenu( "Device" );
      menuBar.add( mnDevice );

      JMenuItem mntmSelectMidi = new JMenuItem( "Select Midi" );
      mntmSelectMidi.setAction( new ActionOpenMidiDialog( this ) );
      mnDevice.add( mntmSelectMidi );

      JMenu mnRig = new JMenu( "Rig" );
      menuBar.add( mnRig );

      JMenuItem mntmRename = new JMenuItem();
      mntmRename.setAction( new ActionRigRename( m_rack ) );
      mntmRename.setText( "Rename Rig" );
      mnRig.add( mntmRename );

      JMenuItem mntmRigShow = new JMenuItem();
      mntmRigShow.setAction( new ActionRigShow( m_rack ) );
      mntmRigShow.setText( "Rig Show" );
      mnRig.add( mntmRigShow );

      mnRig.addSeparator();
      
      // Collection Menu.
      
      final JCheckBoxMenuItem displayCollection = new JCheckBoxMenuItem("Show Collection");
      displayCollection.addItemListener( new ItemListener() {
         @Override
         public void itemStateChanged(ItemEvent e) {
            boolean b = displayCollection.getState();
            showCollectionWidget( b);
            getPrefs().putBoolean( ConfigStrings.PREF_DISPLAY_COLLECTION, b );
        }
      });
      mnRig.add(displayCollection);
      
      JMenuItem mntmIndex = new JMenuItem( "Index Rig Collection" );
      mntmIndex.setAction( new ActionIndexDB( this ) );
      mntmIndex.setText( "Index Rig Collection"  );
      mnRig.add( mntmIndex );
      
      // Help Menu
      
      menuBar.add(Box.createHorizontalGlue()); // Right Align help.
      JMenu mnHelp = new JMenu( "Help" );
      menuBar.add( mnHelp );

      JMenuItem mntmAbout = new JMenuItem();
      mntmAbout.setAction( new ActionAbout() );
      mntmAbout.setText( "About" );
      mnHelp.add( mntmAbout );
      
      // Rig Table.
      
      m_frame.getContentPane().setLayout( new BorderLayout( 0, 0 ) );
      JScrollPane scrollPane = new JScrollPane();    
      m_frame.getContentPane().add( scrollPane );
      m_rigTable = new TableRigs( m_rack );
      m_rigTable.setPreferredScrollableViewportSize(new Dimension(450, 420));
      scrollPane.setViewportView( m_rigTable );
      m_rigTableModel = new RigTableModel( m_rigTable );
      m_rigTable.setModel( m_rigTableModel );
      scrollPane.setRowHeaderView(TableRigs.buildRowHeader( m_rigTable));

      // Index Panel
      
      mInitDb();
      m_indexPanel = new RigDBPanel( m_db );
      m_frame.getContentPane().add( m_indexPanel, BorderLayout.WEST );
      m_indexPanel.setVisible( true );
      m_indexPanel.setRigSelectionCallback( new RigSelectedCallback() {

         @Override
         public void onRigDoubleClicked( String rigPath )
         {
            try {
               m_rack.loadRigFile( rigPath, false );
            }
            catch( Exception e ) {
               ErrorBox.error( "Cannot load Tfx file", "There was an errir while loading TFX file:\n" + rigPath );
               e.printStackTrace();
            }
         }
      
      });
      // Side buttons
      
      JPanel panel = new JPanel();
      m_frame.getContentPane().add( panel, BorderLayout.EAST );
      panel.setLayout( new MigLayout( "", "[64px]" ) );

      m_btnTune = new ButtonTuner( m_rack );
      panel.add( m_btnTune, "wrap" );

      JLabel lblVolume = new JLabel( "Volume:" );
      panel.add( lblVolume, "wrap" );

      m_sliderVol = new SliderVolume( m_rack );
      m_sliderVol.setOrientation( SwingConstants.VERTICAL );
      m_sliderVol.setName( "Volume" );
      panel.add( m_sliderVol, "alignx center,growy" );
      final RigLoaderGui locThis = this;
      m_indexPanel.setSearchButtonCallback( new ActionListener() {

         @Override
         public void actionPerformed( ActionEvent arg0 )
         {
            String locRigPath = getPrefs().get( ConfigStrings.PREF_RIG_COLLECTION_PATH, null);
            String locIndexPath = getPrefs().get( ConfigStrings.PREF_RIG_INDEX_PATH, null );
            if( locRigPath == null || locIndexPath == null )  {
               ActionIndexDB a = new ActionIndexDB( locThis );
               a.actionPerformed( null );
               return;
            }
            locThis.launchDBIndex( Paths.get(locRigPath), Paths.get(locIndexPath) );
   
         }        
      });

      // Restore user parameters.
   
      boolean displayColSelected = getPrefs().getBoolean(ConfigStrings.PREF_DISPLAY_COLLECTION, true);
      displayCollection.setSelected( displayColSelected );
      
      enableGui();
   }

   // -----------------------------------------------------------------
   public void disableGui()
   {
      m_rack.removeListener( this );
      m_rack.removeListener( m_rigTableModel );
      //m_rigTable.disableGui();
   }

   // -----------------------------------------------------------------
   public void enableGui()
   {
      m_rack.addListener( this );
      m_rack.addListener( m_rigTableModel );
      //m_rigTable.enableGui();
   }

   // -----------------------------------------------------------------
   private class ActionOpenMidiDialog extends AbstractAction
   {
      private RigLoaderGui      m_parent;

      public ActionOpenMidiDialog( RigLoaderGui parent )
      {
         m_parent = parent;
         putValue( NAME, "Select Midi" );
         putValue( SHORT_DESCRIPTION,
                   "Select Midi Channels for the Eleven Rack." );
      }

      public void actionPerformed( ActionEvent e )
      {
         DialogMidiSelector dialog = new DialogMidiSelector( m_parent );
         dialog.setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );
         dialog.setVisible( true );
      }
   }

   // -----------------------------------------------------------------
   private class ActionAbout extends AbstractAction
   {
      public void actionPerformed( ActionEvent arg0 )
      {
         new AboutBox().setVisible( true );
      }
   }

   // -----------------------------------------------------------------
   private class ActionRigRename extends AbstractAction
   {
      private ElevenRack m_rack;

      public ActionRigRename( ElevenRack rack )
      {
         m_rack = rack;
      }

      public void actionPerformed( ActionEvent arg0 )
      {
         new DialogRigRename( m_rack ).setVisible( true );
      }
   }

   // -----------------------------------------------------------------
   private class ActionRigShow extends AbstractAction
   {
      private ElevenRack m_rack;

      public ActionRigShow( ElevenRack rack )
      {
         m_rack = rack;
      }

      public void actionPerformed( ActionEvent arg0 )
      {
         new RigShow( m_rack ).setVisible( true );
      }
   }

   // -----------------------------------------------------------------
   private class ActionIndexDB extends AbstractAction
   {
      RigLoaderGui m_gui;
      
      ActionIndexDB( RigLoaderGui gui )  { m_gui = gui; }
      
      @Override
      public void actionPerformed( ActionEvent arg0 )
      {
         DialogCollectionConfig d = new DialogCollectionConfig( m_gui );
         d.setVisible( true );
      }   
   }
   
   // -----------------------------------------------------------------
   public void initRack( int inPort, int outPort ) throws ERError
   {
      m_rack.close();
      m_rack.open( inPort, outPort );
      ElevenDumper dump = new ElevenDumper();
      m_rack.addListener( dump );
      Term.println( "Starting init..." );
      m_pMon = new EProgressMonitor( m_frame );
      m_rack.startInitDevice( m_pMon );
      Preferences prefs = getPrefs();
      prefs.putInt( ConfigStrings.PREF_INPORT, inPort );
      prefs.putInt( ConfigStrings.PREF_OUTPORT, outPort );
   }

   // ------------------------------------------------------------------
   private void mInitDb()
   {
      m_db = new RigDatabase();

      String dbPath = getPrefs().get( ConfigStrings.PREF_RIG_INDEX_PATH, null );
      if( dbPath != null ) {
         try {
            m_db.open( dbPath );

            Runtime.getRuntime().addShutdownHook( new Thread( new Runnable() {
               public void run()
               {

                  try {
                     m_db.shutdown();
                     Term.println( "Database shutdown properly." );

                  }
                  catch( SQLException e ) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
                  }
               }
            }, "Shutdown-thread" ) );

         }
         catch( SQLException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch( ERError e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch( IOException e1 ) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
      }

   }
   // ------------------------------------------------------------------
   // Eleven Listener Implementation
   // ------------------------------------------------------------------
   @Override
   public void attachRack( ElevenRack er )
   {
   }

   @Override
   public void detachRack()
   {
   }

   @Override
   public void onEffectCount( int n ) throws ERError
   {
   }

   @Override
   public void onEffectDescription( int id ) throws ERError
   {
   }

   @Override
   public void onUpdateRigList( int bank, int id ) throws ERError
   {
   }

   @Override
   public void onRigDescChanged() throws ERError
   {
   }

   @Override
   public void onRigSelected( int bank, int num ) throws ERError
   {
   }

   @Override
   public void onStartInitDevice() throws ERError
   {
   }

   @Override
   public void onDeviceInitialized() throws ERError
   {
      Term.println( "Intitialization complete." );
      if( m_pMon != null ) {
         m_pMon.dispose();
         m_pMon = null;
      }
   }

   @Override
   public void onTunerSwitched( boolean b ) throws ERError
   {
      m_btnTune.silentSet( b );
   }

   @Override
   public void onMainVolumeSet( int val ) throws ERError
   {
      m_sliderVol.silentSet( val );
   }

   public int getInPort()
   {
      return m_rack.getInPort();
   }

   public int getOutPort()
   {
      return m_rack.getOutPort();
   }

   public Component getFrame()
   {
      return m_frame;
   }
   
   //---------------------------------------------------------
   public void showCollectionWidget( boolean b )
   {
      m_indexPanel.setVisible( b );
      m_frame.pack();
   }

   //---------------------------------------------------------   
   public void launchDBIndex( Path rigPath, Path dbPath)
   {
      int option = JOptionPane.showConfirmDialog (null, "Indexing Database will erase" +
      		" current database and re-create-it. Continue?", "Please Confirm!", JOptionPane.YES_NO_OPTION);
      if( option == JOptionPane.NO_OPTION) return;
      
      try {
         m_db.shutdown();
      }
      catch( SQLException e1 ) {
         e1.printStackTrace();
      }

      try {
         m_db.open( dbPath.toString() );
      }
      catch( Exception e ) {
         ErrorBox.error( "Rig collection indexing error.",
                         "Cannot open or create collection database in path " +
                               dbPath.toString() + ":\n"
                               + e.getMessage() );
         return;
      }
      
      try {
         m_db.resetDB();
      }
      catch( Exception e ) {
         ErrorBox.error( "Rig collection indexing error.",
                         "could not index rig collection, there was an error" +
                         " while resetting database:\n"
                               + e.getMessage() );
         return;
      }
      IndexProgressReport ipr = new IndexProgressReport(m_frame);
      RigIndexer ri = new RigIndexer( m_db, ipr );
      
      try {
         ri.indexDir( rigPath );
      }
      catch( IOException e ) {
         e.printStackTrace();
         ErrorBox.error( "Rig collection indexing error.",
                         "Error wile indexing rigs:" 
                               + e.getMessage() );
         return;     
      }
      ipr.showClose();
      m_indexPanel.updateGui();
   }
}
