package gs.elevenhack.utils.rigloader;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;
import javax.swing.UIManager;

import gs.elevenhack.ERError;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ArchiveBuilder;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.TextRender;
import gs.elevenhack.midi.ReceiverCallback;
import gs.elevenhack.midi.SysEx;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxFileIO;
import gs.elevenhack.tfx.TfxParser;

public class ActionSaveBank extends AbstractAction
{

   private ElevenRack          m_rack;
   private final static String PREF_DEFAULT_SAVE_BANK_PATH = "DefaultSaveBankPath";
   private ProgressMonitor m_pMon;
   private int m_bank;
   public final static int USER_BANK = 0;
   public final static int DEMO_BANK = 1;
   
   // ------------------------------------------------------------------
   public ActionSaveBank( ElevenRack er, int bank, ProgressMonitor pm )
   {
      m_rack = er;
      m_bank = bank;
      m_pMon = pm;
   }

   // ------------------------------------------------------------------
   public void actionPerformed( ActionEvent e )
   {
      try {

         EHFileChooser chooser = new EHFileChooser(PREF_DEFAULT_SAVE_BANK_PATH);
         chooser.setDialogTitle("Select a bank file name.");
         File suggestedFile = new File( "NewRigBank.ehb" );
         chooser.setSelectedFile( suggestedFile );

         int returnVal = chooser.showSaveDialog( null );
         if( returnVal == JFileChooser.APPROVE_OPTION ) {
            File f = chooser.getSelectedFile();
            chooser.storeDefaultPath( f.getParent() );
            ArchiveBuilder ab = new ArchiveBuilder( m_rack, m_bank, m_pMon );
            ab.buildArchive(f.toPath().toString() );
         }
      }
      catch( Exception ex ) {
         ex.printStackTrace();
         ErrorBox.error( "Error while saving bank: ", ex.getMessage() );
      }
   }
}
