package gs.elevenhack.utils.rigloader;

import javax.swing.DefaultListSelectionModel;

@SuppressWarnings( "serial" )
public class CustomListSelectionModel extends DefaultListSelectionModel
{
   private boolean m_isActive = false;

   public void setSelectionSilent(int firstIndex){
       m_isActive = true;
       setSelectionInterval(firstIndex, firstIndex);
       m_isActive = false;
   }

   @Override
   public void setSelectionInterval( int first, int last )
   {
	   super.setSelectionInterval( first, last );
   }
   
   //-------------------------------------------------------------
   protected void fireValueChanged( int firstIndex, int lastIndex,
		   							boolean isAdjusting){
       if (m_isActive || isAdjusting){
           return;
       }
       super.fireValueChanged(firstIndex, lastIndex, isAdjusting);
   }
}
