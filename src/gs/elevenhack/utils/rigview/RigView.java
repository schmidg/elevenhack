package gs.elevenhack.utils.rigview;

import java.io.IOException;

import gs.elevenhack.Term;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.TextRender;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

public class RigView
{
   public static void main(String[] args)
   {
      Term.activated = true;
       if( args.length < 1 ) {
          System.err.println("error, missing file argument.");
          System.exit( -1 );
       }

       String fname = args[0];
       
       TfxParser p = new TfxParser();
       try {
         Rig rig = new Rig();
         p.parseFile(rig, fname );
         TextRender r = new TextRender();
         rig.render( r );
         Term.println( r.getText() );
      }
      catch( Exception e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }   
   }
}
