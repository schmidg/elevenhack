package gs.elevenhack.utils.rigview.hexdump2bin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import gs.elevenhack.Term;
import gs.elevenhack.midi.SysEx;

public class HexDump2Bin
{
   public static void main( String[] args )
   {
      try {
         Term.activated = true;
         if( args.length < 2 ) {
            System.err.println( "error, missing file argument." );
            System.err.println( "Use: HexDump2Bin infile outfile." );
            System.exit( -1 );
         }

         String infile = args[0];
         String outfile = args[1];

         BufferedReader in = new BufferedReader( new FileReader( infile ) );
         FileOutputStream out = new FileOutputStream( new File( outfile ) );

         String inString = in.readLine();
         byte[] msg = SysEx.stringToSysEx( inString );
         
         out.write( msg );
         in.close();
         out.close();
      }
      catch( FileNotFoundException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( IOException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
}
