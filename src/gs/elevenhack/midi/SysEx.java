/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ParseUtils;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;

import java.text.StringCharacterIterator;

public class SysEx
{
   SysEx( ElevenRack rack )
   {
   }

   // Message structure.
   public static final byte START             = (byte) 0xF0;
   public static final byte VENDOR_ID         = (byte) 0x13;
   public static final byte DEVICE_ID         = (byte) 0x0B;
   public static final byte MODEL_ID          = (byte) 0x0F;
   public static final byte MODEL_ID2         = (byte) 0x0E; // Strange, happens
                                                             // sometimes.

   public static final byte SNDSET            = (byte) 0x00;
   public static final byte REQU              = (byte) 0x01;
   public static final byte ASYNCSET          = (byte) 0x02;
   public static final byte RESPOND           = (byte) 0x12;

   public static final byte END               = (byte) 0xF7;

   // Command Type
   public static final int  CMD_SET_BULK_TFX = 0x00;
   public static final int  CMD_GET_BULK_TFX = 0x01;
   public static final int  CMD_CURR_RIG_NUM  = 0x02;
   public static final int  CMD_SAVE_RIG      = 0x03;
   public static final int  CMD_RIG_GETNAME   = 0x04;
   public static final int  CMD_RIG_SETNAME   = 0x05;
   public static final int  CMD_DESC_EFFECT   = 0x20;
   public static final int  CMD_RIG_DESC      = 0x21;
   public static final int  CMD_COUNT_EFFECT  = 0x22;
   public static final int  CMD_MAIN_VOLUME   = 0x36;
   public static final int  CMD_TUNER         = 0x40;
   public static final int  CMD_TUNER_A       = 0x41;

   // ------------------------------------------------------------
   private static char      hexDigits[]       = { '0', '1', '2', '3', '4', '5',
         '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

   // ------------------------------------------------------------
   public static String intToHex( int i )
   {
      return "" + hexDigits[( i & 0xF0 ) >> 4] + hexDigits[i & 0x0F];
   }

   // ------------------------------------------------------------
   static int extractCommand( byte[] msg )
   {
      return msg[5];
   }

   // ------------------------------------------------------------
   static int extractParam( byte[] msg, int loc )
   {
      return msg[6 + loc];
   }

   // ------------------------------------------------------------
   static byte[] queryNbEffects()
   {
      return buildQuery1( CMD_COUNT_EFFECT );
   }

   // ------------------------------------------------------------
   static byte[] queryEffect( int num )
   {
      return buildQuery2( CMD_DESC_EFFECT, num );
   }

   // ------------------------------------------------------------
   public static byte[] queryRigName( int bank, int num )
   {
      return buildQuery3( CMD_RIG_GETNAME, bank, num );
   }

   // ------------------------------------------------------------
   public static byte[] buildQuery1( int code )
   {
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, REQU,
            (byte) ( code & 0xFF ), END };
   }

   // ------------------------------------------------------------
   public static byte[] buildSet1( int code, int param1 )
   {
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, SNDSET,
            (byte) ( code & 0xFF ), (byte) ( param1 & 0xFF ), END };
   }

   // ------------------------------------------------------------
   public static byte[] buildSet2( int code, int param1, int param2 )
   {
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, SNDSET,
            (byte) ( code & 0xFF ), (byte) ( param1 & 0xFF ),
            (byte) ( param2 & 0xFF ), END };
   }

   // ------------------------------------------------------------
   public static byte[] buildSetEncoded1( int cmd, int i )
   {
      byte[] encoded = ParseUtils.byteToEncodedInt( (byte) ( i & 0xFF ) );
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, SNDSET,
            (byte) ( cmd & 0xFF ), encoded[0], encoded[1], encoded[2],
            encoded[3], encoded[4], END };
   }

   // ------------------------------------------------------------
   // Same thatn previous on but with 0 before parameter.
   public static byte[] buildSetEncoded01( int cmd, int i )
   {
      byte[] encoded = ParseUtils.byteToEncodedInt( (byte) ( i & 0xFF ) );
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, SNDSET,
            (byte) ( cmd & 0xFF ), 0, encoded[0], encoded[1], encoded[2],
            encoded[3], encoded[4], END };
   }

   // ------------------------------------------------------------
   static byte[] buildQuery2( int code, int param )
   {
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, REQU,
            (byte) ( code & 0xFF ), (byte) ( param & 0xFF ), END };
   }

   // ------------------------------------------------------------
   static byte[] buildQuery3( int code, int param1, int param2 )
   {
      return new byte[ ] { START, VENDOR_ID, DEVICE_ID, MODEL_ID, REQU,
            (byte) ( code & 0xFF ), (byte) ( param1 & 0xFF ),
            (byte) ( param2 & 0xFF ), END };
   }

   // ------------------------------------------------------------
   public static String byteMsgToString( byte[] msg )
   {
      StringBuilder sb = new StringBuilder();
      sb.append( "\t" );
      // Hexa
      for ( int i = 0; i < msg.length; i++ ) {
         byte b = msg[i];
         sb.append( ' ' );
         sb.append( intToHex( b ) );
      }
      sb.append( "\n\t   " );
      for ( int i = 0; i < msg.length; i++ ) {
         byte b = msg[i];
         sb.append( "  " );
         if( b >= 32 && b <= 176 ) sb.append( (char) b );
         else sb.append( "." );
      }
      return sb.toString();
   }

   // ----------------------------------------------------------
   public static byte[] stringToSysEx( String s )
   {
      StringBuilder sb = new StringBuilder();
      s = s.trim();

      boolean b = false;
      int val = 0;

      String hexTab[] = s.split( " " );
      for ( String hex : hexTab ) {
         int v = Integer.parseInt( hex, 16 );
         sb.append( (char) ( v & 0xFF ) );
      }

      String outStr = sb.toString();
      byte[] buf = new byte[ outStr.length() ];
      int i = 0;
      StringCharacterIterator iter = new StringCharacterIterator( outStr );
      for ( char c = iter.first(); c != iter.DONE; c = iter.next() ) {
         buf[i] = (byte) ( c & 0xFF );
         i++;
      }
      return buf;
   }

   // ----------------------------------------------------------
   static String extractString( byte[] msg, int pos )
   {
      StringBuilder sb = new StringBuilder();
      byte c = msg[pos];

      while( c != 0 ) {
         sb.append( (char) c );
         pos++;
         c = msg[pos];
      }
      return sb.toString();
   }

   // ------------------------------------------------------------
   static String extractString( byte[] msg, int pos, int len )
   {
      StringBuilder sb = new StringBuilder();
      for ( int i = pos; i < pos + len; i++ ) {
         sb.append( (char) msg[i] );
      }
      return sb.toString();
   }

   // ------------------------------------------------------------
   public static String getHexString( byte[] aByte )
   {
      StringBuilder sb = new StringBuilder();
      sb.append( "\t<- " );
      // Hexa
      for ( int i = 0; i < aByte.length; i++ ) {
         byte b = aByte[i];
         sb.append( ' ' );
         sb.append( SysEx.intToHex( b ) );
      }
      sb.append( "\n\t   " );
      for ( int i = 0; i < aByte.length; i++ ) {
         byte b = aByte[i];
         sb.append( "  " );
         if( b >= 32 && b <= 176 ) sb.append( (char) b );
         else sb.append( "." );
      }

      return sb.toString();
   }
   // ------------------------------------------------------------
   public static byte[] stripF7( byte[] buf )
   {
      if ( buf.length < 1 ) return null;
      // Find F7 index starting from the end.
      int i;
      for( i = buf.length - 1; i > 0 && buf[i] != (byte)0xF7; --i ) {}
      if( i == 0 ) return buf;
      int offset = 0;
      if( buf[0] == (byte) 0xF7 ) {
    	  --i;
    	  offset = 1;
      }
      
      byte[] newBuf = new byte[i];
      System.arraycopy(buf, offset, newBuf, 0, i );
      return newBuf;
   }
   
   // ------------------------------------------------------------
   public static byte[] extractFrom7bits( byte[] msg, int begin, int end )
   {
      int len = end - begin;
      byte[] res = new byte[ len ];
      int savePos = begin;
      for ( int i = 0; i < len; i++ )
         res[i] = 0;

      int shift = 1;
      int i = 0;
      for ( ; begin + i < end - 1; ++i ) {
         res[i] = (byte) ( ( (byte) ( ( msg[begin + i] & 0xFF ) << shift ) ) + ( (byte) ( ( msg[begin
                                                                                                + i
                                                                                                + 1] & 0xFF ) >>> ( 7 - shift ) ) ) );
         shift++;
         if( shift == 8 ) {
            shift = 1;
            begin++;
         }
      }
      res[i] = (byte) ( ( (byte) ( ( msg[savePos + len - 1] ) & 0xFF << shift ) ) );
      byte[] trimmed = new byte[ i + 1 ];
      System.arraycopy( res, 0, trimmed, 0, i + 1 );
      return trimmed;
   }

   // ------------------------------------------------------------
   public static byte[] encodeTo7bits( byte[] msg, int start, int end )
   {
      int len = end - start;
      byte[] res = new byte[ len + (int) ( len * 0.20 + 1 ) ];
      for ( int i = 0; i < res.length; i++ )
         res[i] = 0;

      res[0] = (byte) ( ( ( msg[start] & 0xFF ) >>> 1 ) & 0x7F );
      int shift = 2;
      int resPos = 1;

      int idx = start;
      while( idx < start + len - 1 ) {

         res[resPos] += (byte) ( ( ( msg[idx] & 0xFF ) << ( 8 - shift ) ) & 0x7F );
         res[resPos] += (byte) ( ( ( msg[idx + 1] & 0xFF ) >>> shift ) & 0x7F );
         idx++;
         shift++;
         resPos++;
         if( shift == 9 ) {
            shift = 2;

            res[resPos] = (byte) ( ( msg[idx] & 0xFF ) >>> 1 );
            resPos++;
         }
      }
      res[resPos] += (byte) ( ( ( msg[idx] & 0xFF ) << ( 8 - shift ) ) & 0x7F );
      resPos++;

      byte[] trimmed = new byte[ resPos ];
      System.arraycopy( res, 0, trimmed, 0, resPos );
      return trimmed;
   }

   // ------------------------------------------------------------
   public static byte[] encodeTo7bits( byte[] msg )
   {
      return encodeTo7bits( msg, 0, msg.length );
   }
}
