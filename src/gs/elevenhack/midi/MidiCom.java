/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ERError;
import gs.elevenhack.ER.ElevenRack;


public class MidiCom
{

   ElevenRack        m_rack    = null;
   ElevenTransmitter m_toRack   = null;
   ElevenReceiver    m_fromRack = null;
   MidiDriver m_md = new MidiDriver();
   int m_inPort;
   int m_outPort;
   
 
   // ---------------------------------------------------------
   static public String listMidiDevices() throws ERError
   {
      return new MidiCom().m_md.listAllMidiPorts();
   }

   // ---------------------------------------------------------
   static public String[] listMidiInDevices() throws ERError
   {
      return new MidiCom().m_md.getMidiInList();
   }
   
   // ---------------------------------------------------------
   static public String[] listMidiOutDevices() throws ERError
   {
      return new MidiCom().m_md.getMidiOutList(); 
   }   
   
   public MidiCom( ElevenRack er, int numDevIn, int numDevOut ) throws ERError
   {
      m_rack = er;
      m_md.open( numDevIn, numDevOut );
      m_fromRack = new ElevenReceiver( m_rack );
      m_toRack = new ElevenTransmitter( m_md );
      m_md.registerCallback( m_fromRack );
      m_inPort = numDevIn;
      m_outPort = numDevOut;
   }

   // ---------------------------------------------------------
   public ElevenTransmitter getTransmitter()
   {
      return m_toRack;
   }

   // ---------------------------------------------------------
   public void sendSysex( byte[] msg ) throws ERError
   {
      m_toRack.sendSysex( msg );
   }

   // ---------------------------------------------------------
   public ElevenReceiver getReceiver()
   {
      return m_fromRack;
   }

   // ---------------------------------------------------------
   public boolean isConnected()
   {
      return m_fromRack != null && m_toRack != null;
   }

   // ---------------------------------------------------------
   public void close() throws ERError
   {
      m_md.close();
   }

   // ---------------------------------------------------------
   public int getInPort()
   {
      return m_inPort;
   }

   // ---------------------------------------------------------
   public int getOutPort()
   {
      return m_outPort;
   }
   
   private MidiCom()
   {
   }
}
