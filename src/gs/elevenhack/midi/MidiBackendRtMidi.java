/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import rtmidi.swig.ByteVector;
import rtmidi.swig.RtCallback;
import rtmidi.swig.RtError;
import rtmidi.swig.RtMidiIn;
import rtmidi.swig.RtMidiOut;


import gs.elevenhack.ERError;
import gs.elevenhack.Term;

public class MidiBackendRtMidi implements MidiBackend
{
   private RtMidiIn     m_in;
   private MidiOutJavax    m_out;
   private MidiCallbackBridge m_cbb;

   MidiBackendRtMidi()
   {
      m_in = new RtMidiIn();
      m_out = new MidiOutJavax();
   }

   @Override
   public int getNbPorts()
   {
      return (int) ( m_in.getPortCount() + m_out.getNbPorts() );
   }

   @Override
   public String[] getMidiInList() throws ERError
   {
      long n = m_in.getPortCount();
      String ret[] = new String[ (int) n ];
      for ( long i = 0; i < n; ++i ) {
         try {
			ret[(int) i] = m_in.getPortName( i );
		} catch (Exception e) {
			throw new ERError( e.getMessage() );
		}
      }
      return ret;
   }

   @Override
   public String[] getMidiOutList() throws ERError
   {
      return m_out.getMidiOutList();
   }

   @Override
   public void open( int inPort, int outPort ) throws ERError
   {
	   try {
          Term.println( "A" );
          //m_out.close();
          Term.println( "B" );
          m_out.open( outPort );
          Term.println( "C" );
		      m_in.closePort();
	          Term.println( "D" );
	          Term.println( "E" );
		      m_in.openPort( inPort );
	          Term.println( "F" );
		      m_in.ignoreTypes( false, false, false );		   
	          Term.println( "G" );
	   }
	   catch(Exception e) {
		   throw new ERError( e.getMessage() );
	   }
   }

   @Override
   public void close()
   {
	   try {
		   m_in.closePort();	
		   m_out.close();	
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
   }

   @Override
   public void sendSysex( byte[] msg ) throws ERError
   {
      m_out.sendSysex( msg );
   }

   // ----------------------------------------------------------------
   public class MidiCallbackBridge extends RtCallback
   {
      private MidiCallback m_cb;

      MidiCallbackBridge( MidiCallback cb )
      {
         m_cb = cb;
      }

      @Override
      public void receiveMessage( double t, ByteVector msg )
      {
         byte[] ret = new byte[ (int) msg.size() ];
         for ( int i = 0; i < msg.size(); ++i ) {
            ret[i] = (byte) ( 0xFF & msg.get( i ) );
         }
         Term.println( "Received :" + SysEx.byteMsgToString( ret ) );
         m_cb.handleMidiMessage( ret );
      }
   }

   @Override
   public void registerCallback( MidiCallback cb ) throws ERError
   {
	  try { 
      m_in.cancelCallback();
	  }
	  catch( Exception e ){}
	  try {
	     m_cbb = new MidiCallbackBridge( cb );
	     m_in.setCallback( m_cbb );
	  }
	  catch( RtError e) {
		  throw new ERError( e.getMessage() );
	  }
   }

}
