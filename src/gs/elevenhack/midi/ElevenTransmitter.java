/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ERError;
import gs.elevenhack.MessageBuilder;
import gs.elevenhack.ParseUtils;
import gs.elevenhack.Term;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.SysexMessage;

public class ElevenTransmitter
{
   private MidiDriver m_md;

   ElevenTransmitter( MidiDriver md )
   {
      m_md = md;
   }

   public void requestNbEffects() throws ERError
   {
      sendSysex( SysEx.queryNbEffects() );
   }

   public void requestDescEffect( int n ) throws ERError
   {
      sendSysex( SysEx.queryEffect( n ) );
   }

   public void requestRigName( int bank, int num ) throws ERError
   {
      sendSysex( SysEx.queryRigName( bank, num ) );
   }

   public void requestRigDesc() throws ERError
   {
      sendSysex( SysEx.buildQuery1( SysEx.CMD_RIG_DESC ) );
   }
   
   public void requestMainVolume() throws ERError
   {
      sendSysex( SysEx.buildQuery2( SysEx.CMD_MAIN_VOLUME, 0 ));
   }

   public void requestCurrentRigNumber() throws ERError
   {
      sendSysex( SysEx.buildQuery1( SysEx.CMD_CURR_RIG_NUM ) );
   }

   public void selectCurrentRig( int bank, int num ) throws ERError
   {
      sendSysex( SysEx.buildSet2( SysEx.CMD_CURR_RIG_NUM, bank, num ) );
   }

   public void requestBulkRig() throws ERError
   {
      sendSysex( SysEx.buildQuery1( SysEx.CMD_GET_BULK_TFX ) );
   }

   public void sendSysex( byte[] msg ) throws ERError
   {
      try {
         m_md.sendSysex( msg );
      }
      catch( ERError e ) {

         e.printStackTrace();
         throw new ERError( "Unavailable device." );
      }
   }

   // --------------------------------------------------------------
   public void saveRig( int num ) throws ERError
   {
      sendSysex( SysEx.buildSet2( SysEx.CMD_SAVE_RIG, num, 0 ) );
   }

   // --------------------------------------------------------------
   public void setMainVolume( int i ) throws ERError
   {
      sendSysex( SysEx.buildSetEncoded01( SysEx.CMD_MAIN_VOLUME, i ));
   }

   // --------------------------------------------------------------
   public void setRigName( String name ) throws ERError
   {
      Term.println( "Set Rig Name to " + name );
      MessageBuilder mb = new MessageBuilder();
      mb.append( new byte[ ] { SysEx.START, SysEx.VENDOR_ID, SysEx.DEVICE_ID,
            SysEx.MODEL_ID, SysEx.SNDSET, SysEx.CMD_RIG_SETNAME } );
      mb.append( name.getBytes() );
      mb.append( (byte) 0 );
      int s = mb.size() + 1;
      for ( int i = 0; i < s % 4; ++i )
         mb.append( (byte) 0 );
      mb.append( SysEx.END );
      sendSysex( mb.get() );
   }

   // --------------------------------------------------------------
   public void setTunerOn( boolean v ) throws ERError
   {
      sendSysex( SysEx.buildSet1( SysEx.CMD_TUNER, ( v ? 1 : 0 ) ) );
   }
}
