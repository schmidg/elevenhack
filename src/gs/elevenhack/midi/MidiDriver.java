/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ERError;

public class MidiDriver implements MidiBackend

{
   private static MidiBackend m_mbe;

   MidiDriver()
   {
      if( m_mbe == null ) {
         if( System.getProperty( "os.name" ).toLowerCase().indexOf( "mac" ) > -1 ) {
            m_mbe = new MidiBackendRtMidi();
         }
         else
            m_mbe = new MidiBackendJavax();
     }
   }

   @Override
   public int getNbPorts()
   {
      return m_mbe.getNbPorts();
   }

   @Override
   public String[] getMidiInList() throws ERError
   {
      return m_mbe.getMidiInList();
   }

   @Override
   public String[] getMidiOutList() throws ERError
   {
      return m_mbe.getMidiOutList();
   }

   @Override
   public void open( int inPort, int outPort ) throws ERError
   {
      m_mbe.open( inPort, outPort );
   }

   @Override
   public void close() throws ERError
   {
      m_mbe.close();
   }

   @Override
   public void sendSysex( byte[] msg ) throws ERError
   {
      m_mbe.sendSysex( msg );
   }

   @Override
   public void registerCallback( MidiCallback cbk ) throws ERError
   {
      m_mbe.registerCallback( cbk );
   }

   public String listAllMidiPorts() throws ERError
   {
      StringBuilder sb = new StringBuilder();
      String[] l = getMidiInList();
      for ( String s : l ) {
         sb.append( s + "\n" );
      }
      l = getMidiOutList();
      for ( String s : l ) {
         sb.append( s + "\n" );
      }
      return sb.toString();
   }
}
