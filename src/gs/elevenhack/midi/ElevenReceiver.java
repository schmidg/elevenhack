/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ERError;
import gs.elevenhack.MessageBuilder;
import gs.elevenhack.ParseUtils;
import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

import java.util.Arrays;

import javax.sound.midi.ShortMessage;
import javax.swing.SwingUtilities;

/**
 * Displays the file format information of a MIDI file.
 */
public class ElevenReceiver implements MidiCallback
{

   private ElevenRack       m_rack;
   private ReceiverCallback m_callback;
   private MessageBuilder   m_partialMsg;
   private int              m_partialBulkOffset = 0;
   private boolean m_callbackOnce = true;
   
   // --------------------------------------------------------------------------
   public ElevenReceiver( ElevenRack r )
   {
      m_rack = r;
      m_partialMsg = new MessageBuilder();
   }

   // --------------------------------------------------------------------------
   public void close()
   {
   }

   // --------------------------------------------------------------------------
   public void setCallbackOnce( ReceiverCallback cb )
   {
      m_callback = cb;
      m_callbackOnce = true;
   }

   // ---------------------------------------------------------------------------
   public void setCallback( ReceiverCallback cb )
   {
      m_callback = cb;
      m_callbackOnce = false;
   }

   // ---------------------------------------------------------------------------
   public void clearCallback()
   {
      m_callback = null;
   }

   // --------------------------------------------------------------------------
   public void handleMidiMessage( byte[] msg )
   {
      try {
         decodeMessage( msg );
      }
      catch( ERError e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   // --------------------------------------------------------------------------
   public void decodeMessage( final byte[] msg ) throws ERError
   {
      Term.println( "SYSEX type" + SysEx.intToHex( msg[0] ) );
      final byte[] abData = msg;
      Term.println( "SYSEX size: " + abData.length );
      Term.println( "Sysex start : " + SysEx.intToHex( abData[0] ) + " "
            + SysEx.intToHex( abData[1] ) + " "
            + SysEx.intToHex( abData[2] ) + " "
            + SysEx.intToHex( abData[3] ) + " "
            + SysEx.intToHex( abData[4] ) + " "
            + SysEx.intToHex( abData[5] ) + " "
            + SysEx.intToHex( abData[6] ) + " "
            + SysEx.intToHex( abData[7] ) + "..."
            );
      

      SwingUtilities.invokeLater( new Runnable() {
         public void run()
         {
            if( msg[0] == (byte) SysEx.START ) {
               try {
                  parseMessage( abData );
               }
               catch( ERError e ) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
            }
            else if( msg[0] == (byte) SysEx.END ) {
               try {
                  parseContinuedMessage( abData );
               }
               catch( TfxError e ) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
            }
            else {
               Term.println( "Got UNKNOWN type message: "
                             + SysEx.intToHex( msg[0] ) + "DATA: " + abData );
            }
         }
      } ); // Invoke Later.
   }

   // --------------------------------------------------------------------------
   public static int get14bitValue( int nLowerPart, int nHigherPart )
   {
      return ( nLowerPart & 0x7F ) | ( ( nHigherPart & 0x7F ) << 7 );
   }

   // --------------------------------------------------------------------------
   public static String getHexString( ShortMessage sm )
   {
      // bug in J2SDK 1.4.1
      // return getHexString(sm.getMessage());
      int status = sm.getStatus();
      String res = SysEx.intToHex( sm.getStatus() );
      // if one-byte message, return
      switch ( status ) {
         case 0xF6: // Tune Request
         case 0xF7: // EOX
            // System real-time messages
         case 0xF8: // Timing Clock
         case 0xF9: // Undefined
         case 0xFA: // Start
         case 0xFB: // Continue
         case 0xFC: // Stop
         case 0xFD: // Undefined
         case 0xFE: // Active Sensing
         case 0xFF:
            return res;
      }
      res += ' ' + SysEx.intToHex( sm.getData1() );
      // if 2-byte message, return
      switch ( status ) {
         case 0xF1: // MTC Quarter Frame
         case 0xF3: // Song Select
            return res;
      }
      switch ( sm.getCommand() ) {
         case 0xC0:
         case 0xD0:
            return res;
      }
      // 3-byte messages left
      res += ' ' + SysEx.intToHex( sm.getData2() );
      return res;
   }

   // --------------------------------------------------------------------------
   void parseMessage( byte[] msg ) throws ERError
   {
      // Sanity Check.
      if( msg[1] != SysEx.VENDOR_ID || msg[2] != SysEx.DEVICE_ID
          || ( msg[3] != SysEx.MODEL_ID && msg[3] != SysEx.MODEL_ID2 )
          || ( msg[4] != SysEx.ASYNCSET && msg[4] != SysEx.RESPOND ) ) {
         throw new ERError( "bad header in sysex message: "
                            + SysEx.byteMsgToString( msg ) + "." );
      }
      Term.println( "parsing message..." );
      boolean msgComplete = true;
      int cmdType = msg[4];
      int cmd = SysEx.extractCommand( msg );

      if( cmdType == SysEx.RESPOND ) {
         switch ( cmd ) {
            case SysEx.CMD_SET_BULK_TFX:
            case SysEx.CMD_GET_BULK_TFX: {
               m_partialMsg.clear();
               msgComplete = checkBulkFull( msg, 6 );
               m_partialBulkOffset = 6;
               if( !msgComplete ) m_partialMsg.appendWithoutF7( msg );
               break;
            }
            case SysEx.CMD_CURR_RIG_NUM: {
               gotCmdCurrRigNum( msg );
               break;
            }
            case SysEx.CMD_COUNT_EFFECT: {
               m_rack.callbackSetNbEffects( SysEx.extractParam( msg, 0 ) );
               break;
            }
            case SysEx.CMD_DESC_EFFECT: {
               gotCmdDescEffect( msg );
               break;
            }
            case SysEx.CMD_RIG_GETNAME: { // Name changed confirmation.
               gotCmdRigName( msg );
               break;
            }
            case SysEx.CMD_RIG_DESC: {
               int idx = 7;
               Term.println( "Got RESMOND RIGDESC" );
               break;
            }
            case SysEx.CMD_TUNER: {
               gotTunerSwitch( msg );
               break;
            }
            case SysEx.CMD_MAIN_VOLUME: {
               int volume = ParseUtils.coded7toSignedByte( msg, 7, 12 );
               m_rack.callbackMainVolumeSet( volume );
               break;
            }
            default:
               Term.println( " Got Unknown RESPOND: "
                             + SysEx.byteMsgToString( msg ) );
         }
      }
      else { // Command type == ASYNC. --------------------------------------
         switch ( cmd ) {
            case SysEx.CMD_SET_BULK_TFX:
            case SysEx.CMD_GET_BULK_TFX: {
               // int rigNum = msg[5]; INFO Interessante
               m_partialMsg.clear();
               m_partialBulkOffset = 6;
               msgComplete = checkBulkFull( msg, m_partialBulkOffset );
               if( !msgComplete ) m_partialMsg.appendWithoutF7( msg );

               break;
            }
            case SysEx.CMD_CURR_RIG_NUM: {
               gotCmdCurrRigNum( msg );
               break;
            }
            case SysEx.CMD_COUNT_EFFECT: {
               m_rack.callbackSetNbEffects( SysEx.extractParam( msg, 0 ) );
               break;
            }
            case SysEx.CMD_DESC_EFFECT: {
               gotCmdDescEffect( msg );
               break;
            }
            case SysEx.CMD_RIG_GETNAME: {
               gotCmdRigName( msg );
               break;
            }
            case SysEx.CMD_RIG_SETNAME: {
               String rigName = SysEx.extractString( msg, 6 );
               m_rack.callbackSetCurrentRigName( rigName );
               break;
            }
            case SysEx.CMD_RIG_DESC: {
               int idx = 7;
               Term.println( "Got ASYNC RIGDESC" );
               break;
            }
            case SysEx.CMD_TUNER: {
               gotTunerSwitch( msg );
               break;
            }
            case SysEx.CMD_MAIN_VOLUME: {
               int volume = ParseUtils.coded7toSignedByte( msg, 7, 12 );
               m_rack.callbackMainVolumeSet( volume );
               break;
            }
            default:
               Term.println( " Got Unknown ASYNCSET: "
                             + SysEx.byteMsgToString( msg ) );
         }
      }
      if( msgComplete ) checkUserCallback( msg );
   }

   // -----------------------------------------------------
   private void parseContinuedMessage( byte[] msg ) throws TfxError
   {
      if( m_partialMsg.isEmpty() ) {
         throw new TfxError( "Cannot handle partial message"
                             + " (no bulk message initialized)." );
      }
      m_partialMsg.appendWithoutF7( msg );
      byte[] full = m_partialMsg.get();
      if( checkBulkFull( full, m_partialBulkOffset ) ) {
    	  Term.println("Got Full RigDesc Message.");
         checkUserCallback( full );
      }
   }

   // -----------------------------------------------------
   void checkUserCallback( byte[] msg )
   {
      // / Handling Callback.
      if( m_callback != null ) {
         byte[] seq = m_callback.getExpectedSeq();
         int offset = 4;
         Term.println( "Compare "
                       + SysEx.byteMsgToString( seq )
                       + " and "
                       + SysEx.byteMsgToString( Arrays.copyOfRange( msg,
                                                                    offset,
                                                                    seq.length
                                                                          + offset ) ) );

         if( Arrays.equals( seq,
                            Arrays.copyOfRange( msg, offset, seq.length
                                                             + offset ) ) ) {
            byte[] strippedF7 = SysEx.stripF7( msg );
            m_callback.onExpectedSeq( strippedF7 );
         }
      }
      if( m_callback != null && m_callbackOnce ) m_callback = null;
   }

   // -----------------------------------------------------
   private void gotCmdRigName( byte[] msg ) throws ERError
   {
      int bank = SysEx.extractParam( msg, 0 );
      int num = SysEx.extractParam( msg, 1 );
      String rigName = SysEx.extractString( msg, 8 );
      m_rack.callbackSetRigName( bank, num, rigName );
   }

   // -----------------------------------------------------
   private void gotCmdCurrRigNum( byte[] msg ) throws ERError
   {
      int bank = SysEx.extractParam( msg, 0 );
      int numRig = SysEx.extractParam( msg, 1 );
      m_rack.callbackSwitchRig( bank, numRig );
   }

   // -----------------------------------------------------
   private void gotCmdDescEffect( byte[] msg ) throws ERError
   {
      int n = SysEx.extractParam( msg, 0 );
      String strId = SysEx.extractString( msg, 7 );
      String name = SysEx.extractString( msg, 25 );
      m_rack.callBackSetEffect( n, strId, name );
   }

   // -----------------------------------------------------
   private void gotTunerSwitch( byte[] msg ) throws ERError
   {
      int v = SysEx.extractParam( msg, 0 );
      m_rack.callbackTunerSwitch( v > 0 );
   }

   // ------------------------------------------------------
   private boolean checkBulkFull( byte[] msg, int offset )
   {
      byte[] bulk = SysEx.extractFrom7bits( msg, offset, msg.length );

      TfxParser p = new TfxParser();
      return p.checkCompleteMessage( bulk );
   }

}
