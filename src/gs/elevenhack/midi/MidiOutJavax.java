/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import gs.elevenhack.ERError;
import gs.elevenhack.MessageBuilder;
import gs.elevenhack.Term;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.SysexMessage;

public class MidiOutJavax
{
   MidiDevice m_devout  = null;
   int        m_outPort = 0;

   boolean isOpen()
   {
      return m_devout != null && m_devout.isOpen();
   }

   public int getNbPorts()
   {
      return MidiSystem.getMidiDeviceInfo().length;
   }

   public String[] getMidiOutList()
   {
      MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
      String[] ret = new String[ infos.length ];
      int count = 0;
      for ( MidiDevice.Info i : infos ) {
         ret[count] = i.getName() + " " + i.getDescription();
         count++;
      }
      return ret;
   }

   void open( int numDevOut ) throws ERError
   {
      MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();

      if( numDevOut < 0 || numDevOut >= infos.length ) {
         throw new ERError( "error, no device " + numDevOut + ". Max device: "
                            + getNbPorts() );
      }
      try {
         m_devout = MidiSystem.getMidiDevice( infos[numDevOut] );
         if( !m_devout.isOpen() ) m_devout.open();
      }
      catch( MidiUnavailableException e ) {
         if( m_devout != null && m_devout.isOpen() ) m_devout.close();
         e.printStackTrace();
         throw new ERError( "Unavailable device." + " Nb Availlable device: "
                            + this.getNbPorts() );
      }
      m_outPort = numDevOut;
   }

   void close()
   {
      m_devout.close();
   }

   public void sendSysex( byte[] msg ) throws ERError
   {
      try {
         if( !m_devout.isOpen() ) m_devout.open();

         Receiver receiver = m_devout.getReceiver();
         /*
          * Here, we prepare the MIDI messages to send.
          */
         MessageBuilder mb = new MessageBuilder();
         mb.append( msg );

         msg = mb.get();

         SysexMessage sysexMessage = new SysexMessage();
         sysexMessage.setMessage( msg, msg.length );

         /*
          * Deliver the message
          */
         receiver.send( sysexMessage, -1 );
         Term.println( "JAVAX driver Send Message of size : " + msg.length + ":"
                       + SysEx.byteMsgToString( msg ) );
      }
      catch( MidiUnavailableException e ) {

         e.printStackTrace();
         throw new ERError( "Send message: Unavailable device." );
      }
      catch( InvalidMidiDataException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         throw new ERError( "Send Message: Invalid Midi data." );
      }

   }

}
