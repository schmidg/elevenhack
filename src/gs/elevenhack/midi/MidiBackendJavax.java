/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.midi;

import javax.sound.midi.MidiDevice;

import gs.elevenhack.ERError;
import gs.elevenhack.MessageBuilder;

import gs.elevenhack.Term;
import gs.elevenhack.ER.ElevenRack;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;

public class MidiBackendJavax implements MidiBackend
{
   private MidiCallback m_cb;

   int                  m_inPort  = 0;
   int                  m_outPort = 0;
   MidiDevice           m_devin   = null;
   MidiOutJavax         m_devout = null;

   @Override
   public int getNbPorts()
   {
      return MidiSystem.getMidiDeviceInfo().length;
   }

   @Override
   public String[] getMidiInList()
   {
      MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
      String[] ret = new String[ infos.length ];
      int count = 0;
      for ( MidiDevice.Info i : infos ) {
         ret[count] = i.getName() + " " + i.getDescription();
         count++;
      }
      return ret;
   }

   @Override
   public String[] getMidiOutList()
   {
      return getMidiInList();
   }
   
   @Override
   public void open( int numDevIn, int numDevOut ) throws ERError
   {
      // TODO Auto-generated method stub
      MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
      if( numDevIn < 0 || numDevIn >= infos.length ) {
         throw new ERError( "error, no device " + numDevIn + ". Max device: "
                            + getNbPorts() );
      }

      try {
         m_devin = MidiSystem.getMidiDevice( infos[numDevIn] );
         if( !m_devin.isOpen() ) m_devin.open();
         m_devout = new MidiOutJavax();
         m_devout.open( numDevOut );
      }
      catch( MidiUnavailableException e ) {

         if( m_devin != null && m_devin.isOpen() ) m_devin.close();
         if( m_devout != null && m_devout.isOpen() ) m_devout.close();
         e.printStackTrace();
         throw new ERError( "Unavailable device." + " Nb Availlable device: "
                            + this.getNbPorts() );
      }
      m_inPort = numDevIn;
      m_outPort = numDevOut;
      Term.println( "in: " + m_inPort + " out: " + m_outPort );
   }

   @Override
   public void close()
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void sendSysex( byte[] msg ) throws ERError
   {
      m_devout.sendSysex( msg );
   }

   private class MidiCallbackBridge implements Receiver
   {
      private MidiCallback m_cb;

      MidiCallbackBridge( MidiCallback cb )
      {
         m_cb = cb;
      }

      @Override
      public void send( MidiMessage message, long timeStamp )
      {
         Term.println( "RECEIVED MIDI MESSAGE" );
         if( message instanceof ShortMessage ) {
            Term.println( "got a unhandled short message." );
         }
         else if( message instanceof SysexMessage ) {
            m_cb.handleMidiMessage( ( (SysexMessage) message ).getMessage() );
         }
         else if( message instanceof MetaMessage ) {
            Term.println( "got a unhandled meta message." );
         }
         else {
            Term.println( "unknown message type" );
         }
      }

      @Override
      public void close()
      {// NOP
      }
   }

   @Override
   public void registerCallback( MidiCallback cb )
   {
      m_cb = cb;
      try {
         m_devin.getTransmitter().setReceiver( new MidiCallbackBridge( cb ) );
      }
      catch( MidiUnavailableException e ) {
         // TODO Auto-gennerated catch block
         e.printStackTrace();
      }
   }
}
