/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack;

import gs.elevenhack.midi.SysEx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.Adler32;
import java.util.zip.CRC32;

public class CrcCalc
{
   static byte[] crcXorCalc( byte[] msg, int offset ) throws ERError
   {
      if( (msg.length - offset) % 4 != 0 ) throw new ERError(
                                                   "cannot calc crc on non multiple of 4 buffer size." );
      byte res[] = new byte[ 4 ];
      res[0] = (byte) 0xFF;
      res[1] = (byte) 0xFF;
      res[2] = (byte) 0xFF;
      res[3] = (byte) 0xFF;

      int nloop = (msg.length - offset) / 4;

      for ( int i = 0; i < nloop; ++i ) {
         int idx = i * 4;
         res[0] = (byte) ( (res[0] ^ msg[offset + idx] ) & 0x7F );
         res[1] = (byte) ( (res[1] ^ msg[offset + idx + 1] )& 0x7F );
         res[2] = (byte) ( (res[2] ^ msg[offset + idx + 2] )& 0x7F );
         res[3] = (byte) ( (res[3] ^ msg[offset + idx + 3] )& 0x7F );
      }

      return res;
   }

   static byte[] crc32Calc( byte[] msg, int offset )
   {
      CRC32 crc = new CRC32();
      crc.update( msg, offset, msg.length - offset );
      long c = crc.getValue();
      byte[] ret = new byte[ 4 ];
      ret[0] = (byte) ( c & 0x7F );
      ret[1] = (byte) ( ( c >> 8 ) & 0x7F );
      ret[2] = (byte) ( ( c >> 16 ) & 0x7F );
      ret[3] = (byte) ( ( c >> 24 ) & 0x7F );
      return ret;
   }

   static byte[] adler32Calc( byte[] msg, int offset )
   {
      Adler32 adler = new Adler32();
      adler.update( msg, offset, msg.length - offset );
      long c = adler.getValue();
      byte[] ret = new byte[ 4 ];
      ret[0] = (byte) ( c & 0x7F );
      ret[1] = (byte) ( ( c >> 8 ) & 0x7F );
      ret[2] = (byte) ( ( c >> 16 ) & 0x7F );
      ret[3] = (byte) ( ( c >> 24 ) & 0x7F );
      return ret;
   }

   public static void main( String args[] )
   {
      if( args.length < 1 ) {
         System.err.println( "Missing file parameter." );
      }
      String fname = args[0];
      Term.print( fname + ":" );
      FileInputStream in;
      try {
         in = new FileInputStream( new File( fname ) );
         byte[] msg = new byte[ in.available() ];
         in.read( msg );

         for ( int i = 0; i < 100; ++i ) {
            Term.println( " offset: " + i + " : "
                                + SysEx.getHexString( crc32Calc( msg, i ) )
                                + SysEx.getHexString( adler32Calc( msg, i ) ) );
            if( (msg.length - i ) % 4 == 0 )
               Term.println( "xor offset " +i + " : " + SysEx.getHexString( crcXorCalc( msg, i ) ));
         
         }
      }
      catch( FileNotFoundException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( IOException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( ERError e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
}
