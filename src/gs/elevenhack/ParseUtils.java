/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack;

import gs.elevenhack.midi.SysEx;

public class ParseUtils
{
   // ------------------------------------------------------------
   public static final String quadToString( byte[] quad )
   {
      StringBuilder sb = new StringBuilder();
      int a = quad[0] & 0xFF;
      int b = quad[1] & 0xFF;
      int c = quad[2] & 0xFF;
      int d = quad[3] & 0xFF;
      sb.append( "" + a + " " + b + " " + c + " " + d );
      return sb.toString();
   }

   // ------------------------------------------------------------
   public static final int quadToInt( byte[] quad )
   {
      int a = quad[0] & 0xFF;
      int b = quad[1] & 0xFF;
      int c = quad[2] & 0xFF;
      int d = quad[3] & 0xFF;
      return a + ( b << 8 ) + ( c << 16 ) + ( d << 24 );
   }

   // -------------------------------------------------------------
   public static final byte[] intToQuad( int v )
   {
      return new byte[ ] { (byte) ( v & 0xFF ), (byte) ( (v & 0xFF00) >>> 8 ),
            (byte) ( (v & 0xFF0000) >>> 16 ), (byte) ( (v & 0xFF000000) >>> 24 ) };
   }

   //-------------------------------------------------------------
   public static final byte[] byteToEncodedInt( byte v )
   {
      if( v>>>1 == (byte)0x3f ) return new byte[ ] { (byte)0x3F, (byte)0x7F,(byte)0x7F,(byte)0x7F,(byte)0x0F };
      return new byte[ ] { (byte) ( (v>>>1) & 0x7F ), 0,0,0,0 };
   }
   // ------------------------------------------------------------
   public static final String quadToKey( byte[] buf )
   {
      StringBuilder sb = new StringBuilder();
      sb.append( (char) ( buf[3] ) );
      sb.append( (char) ( buf[2] ) );
      sb.append( (char) ( buf[1] ) );
      sb.append( (char) ( buf[0] ) );
      return sb.toString();
   }

   // ------------------------------------------------------------
   public static final int coded7toSignedByte( byte[] buf, int start, int end )
   {
      byte[] dec = SysEx.extractFrom7bits( buf, start, end );
      return dec[0];
   }
}
