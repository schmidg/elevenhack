/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack;

import gs.elevenhack.midi.SysEx;

import java.util.LinkedList;

public class MessageBuilder
{
	private LinkedList< byte[]> m_bufs;
	private int m_size;
	
	//-----------------------------------------------------------
	public MessageBuilder()
	{
		m_bufs = new LinkedList<byte[]>();
		m_size = 0;
	}
	
	//-----------------------------------------------------------
	public void append( byte[] buf) {
		m_bufs.addLast( buf );
		m_size += buf.length; 
	}
	
	//-----------------------------------------------------------
	public void appendWithoutF7( byte[] buf )
	{
	   byte[] newBuf = SysEx.stripF7( buf );
	   Term.println("Adding message without F7 size :" + newBuf.length);
		m_size += newBuf.length;
		m_bufs.addLast(newBuf);
	}

	//-----------------------------------------------------------
	public void prepend( byte[] buf ) {
		m_bufs.addFirst( buf );
		m_size += buf.length;
		}
	
	//-----------------------------------------------------------
	public byte[] get() {
		byte[] res= new byte[ m_size ];
		int count = 0;
		for( byte[] buf : m_bufs) {
			System.arraycopy(  buf, 0, res, count, buf.length );
			count += buf.length;
		}
		return res;
	}
	
	//-----------------------------------------------------------
	public int size() { return m_size; }

	//-----------------------------------------------------------
	public void append(byte b)
    {
    	 append(new byte[]{b});   
    }

	public void clear() {
		m_bufs.clear();
		m_size = 0;
	}
	
   public boolean isEmpty()
   {
      return m_bufs.isEmpty();
   }
}
