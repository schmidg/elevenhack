/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import gs.elevenhack.ParseUtils;
import gs.elevenhack.Term;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.ER.TextRender;
import gs.elevenhack.midi.SysEx;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SysExTest
{

   @Test
   public void test()
   {
      Term.activated = true;
      decodeAndView( new byte[ ] { (byte) 0x40, (byte) 0x00, (byte) 0x00,
      (byte) 0x00,(byte) 0x00 } );
      decodeAndView( new byte[ ] { (byte) 0x3F, (byte) 0x7F, (byte) 0x7F,
      (byte) 0x7F,(byte) 0x0F } );
      checkEncoding( new byte[ ] { (byte) 0x80, (byte) 0x81 } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K' } );
      checkEncoding( new byte[ ] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'X', 'Y', 'Z' } );
      for ( int s = 1; s < 128; ++s ) {
         byte[] buf = new byte[ s ];
         for ( int i = 0; i < s; ++i ) {
            buf[i] = (byte) ( ( i + 128 ) & 0xFF );
         }
         checkEncoding( buf );
      }

      for ( int s = 1000; s < 2000; ++s ) {
         byte[] buf = new byte[ s ];
         for ( int i = 0; i < s; ++i ) {
            buf[i] = (byte) ( i & 0xFF );
         }
         checkEncoding( buf );
         checkPartialEncoding( buf );
      }
   }

   // ----------------------------------------------------------------------------
   @Test
   public void testBulkDump() throws TfxError
   {
      String fileName = "src/gs/elevenhack/tests/bulkdump.bin";
      try {
         Term.activated = true;
         FileInputStream in = new FileInputStream( new File( fileName ) );
         byte[] msg = new byte[ in.available() ];
         in.read( msg );
         in.close();

         int bulkoffset = 6;
         byte[] tmp = Arrays.copyOfRange( msg,  bulkoffset, msg.length );
         byte[] bulk = SysEx.extractFrom7bits( tmp, 0, tmp.length );

         TfxParser p = new TfxParser();
         Rig rig = new Rig();
         p.parseMsg( rig, bulk );
         TextRender r = new TextRender();
         rig.render( r );
         Term.println( r.getText() );

         bulk = SysEx.extractFrom7bits( msg, bulkoffset, msg.length );

         p = new TfxParser();
         rig = new Rig();
         p.parseMsg( rig, bulk );
         r = new TextRender();
         rig.render( r );
         Term.println( r.getText() );

      }
      catch( FileNotFoundException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( IOException e ) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch( TfxError e ) {
         e.printStackTrace();
         throw e;
      }

   }

   // ----------------------------------------------------------------------------
   private void checkEncoding( byte[] tab )
   {
      byte[] sevenTab = SysEx.encodeTo7bits( tab, 0, tab.length );

      byte[] eightTab = SysEx.extractFrom7bits( sevenTab, 0, sevenTab.length );
      Term.println( "Testing buffed with length: " + tab.length );
      boolean fail = false;
      for ( int i = 0; i < tab.length; ++i ) {
         if( tab[i] != eightTab[i] ) fail = true;
      }
      if( fail == true ) Term.println( "Byte comparison failed." );
      if( eightTab.length > tab.length + 2 ) fail = true;

      if( fail == true ) {
         Term.println( SysEx.byteMsgToString( tab ) );
         Term.println( "Encoded as:" );
         Term.println( SysEx.byteMsgToString( sevenTab ) );
         Term.println( "Fails Comparison with:" );
         Term.println( SysEx.byteMsgToString( eightTab ) );
         Term.println( "7bits Encoded length:" + sevenTab.length );
      }
      assertEquals( fail, false );
   }
   
   // ----------------------------------------------------------------------------
   private void decodeAndView( byte[] tab )
   {
      byte[] eightTab = SysEx.extractFrom7bits(tab, 0, tab.length );
      Term.println( "Decoding " + SysEx.byteMsgToString( tab) +  " == " +
            SysEx.byteMsgToString( eightTab )); 
      Term.println( "As int: " + ParseUtils.quadToInt( eightTab ) );
   }
   // ----------------------------------------------------------------------------
   private void checkPartialEncoding( byte[] tab )
   {
      int offset = tab.length / 2;
      byte[] sevenTab = SysEx.encodeTo7bits( tab, offset, tab.length );

      byte[] eightTab = SysEx.extractFrom7bits( sevenTab, 0, sevenTab.length );
      Term.println( "Testing buffed with length: " + tab.length );
      boolean fail = false;
      for ( int i = 0; i < tab.length - offset; ++i ) {
         if( tab[offset + i] != eightTab[i] ) fail = true;
      }
      if( fail == true ) Term.println( "Byte comparison failed." );
      if( eightTab.length > tab.length + 2 ) fail = true;

      if( fail == true ) {
         Term.println( SysEx.byteMsgToString( tab ) );
         Term.println( "Encoded as:" );
         Term.println( SysEx.byteMsgToString( sevenTab ) );
         Term.println( "Fails Comparison with:" );
         Term.println( SysEx.byteMsgToString( eightTab ) );
         Term.println( "7bits Encoded length:" + sevenTab.length );
      }
      assertEquals( fail, false );
   }
}
