/*
 * Copyright (c) 2020. Guillaume Schmid
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gs.elevenhack.tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import gs.elevenhack.Term;
import gs.elevenhack.ER.Rig;
import gs.elevenhack.tfx.TfxError;
import gs.elevenhack.tfx.TfxParser;

import java.io.File;
import java.io.IOException;

public class TfxParserTest
{

   @BeforeClass
   public static void setUpBeforeClass() throws Exception
   {
        Term.activated = true;
   }

   @Test
   public void test()
   {

      // Directory path here
      String path = "/home/guillaume/Dropbox/ElevenRack/All_ERUG_Rigs_as_of_071312";

      File folder = new File( path );
      File[] listOfFiles = folder.listFiles();

      for ( int i = 0; i < listOfFiles.length; i++ ) {

         if( listOfFiles[i].isFile() ) {
            String file;
            try {
               file = listOfFiles[i].getCanonicalPath();
               if( file.endsWith( ".tfx" ) || file.endsWith( ".TFX" ) ) {
                  try {
                     Term.println( "Processing file " + file );
                     testParse( file );
                  }
                  catch( Exception e ) {
                     fail( "file " + file + ":" + e.getMessage() );
                  }
               }
            }
            catch( IOException e1 ) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
            }
         }
      }
   }

   void testParse( String fname ) throws TfxError, IOException
   {
      Rig rig = new Rig();
      TfxParser p = new TfxParser();
      p.parseFile( rig, fname );
   }
}
